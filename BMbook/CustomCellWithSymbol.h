//
//  CustomCellWithSymbol.h
//  BiBi
//
//  Created by Shoshi V. on 2/27/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCellWithSymbol : UITableViewCell
@property (nonatomic ,weak)IBOutlet UIImageView *imageV;
@end
