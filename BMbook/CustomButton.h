//
//  CustomButton.h
//  BMbook
//
//  Created by Shoshi V. on 5/27/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WorldLocation.h"
@interface CustomButton : UIButton
@property (nonatomic, strong)WorldLocation *worldLocation;
@end
