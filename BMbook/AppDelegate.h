//
//  AppDelegate.h
//  BMbook
//
//  Created by Shoshi V. on 5/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideViewController.h"
#import "FirstViewController.h"
#import "SoundHandler.h"
#import <FacebookSDK/FacebookSDK.h>
#import "ExceptionHandler.h"
//extern NSString *const FBSessionStateChangedNotification;
@interface AppDelegate : UIResponder <UIApplicationDelegate,ProtocolSlide>

@property (strong, nonatomic) UIWindow *window;
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;
- (void)sendRequestToiOSFriends;
@end
