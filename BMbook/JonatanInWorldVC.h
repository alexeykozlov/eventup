//
//  JonatanInWorldVC.h
//  BMbook
//
//  Created by Shoshi V. on 5/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideOptionViewController.h"
#import <MapKit/MapKit.h>
#import "WorldLocation.h"
#import "CustomAnnotationView.h"
#import "CustomPlacemark.h"
#import "OneWorldLocationVC.h"
#import "CustomButton.h"
@interface JonatanInWorldVC : SlideOptionViewController<MKMapViewDelegate, MKAnnotation>
@property (nonatomic, weak) IBOutlet MKMapView *mpDisplayMap;
@property (nonatomic, strong) NSMutableArray* arrWorldLocations;
@property (nonatomic, strong) WorldLocation* selectedWorldLocation;
@property (nonatomic, strong) NSMutableArray *arrAnontations;



@end
