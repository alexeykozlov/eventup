//
//  MenagerService.m
//  BMbook
//
//  Created by Shoshi V. on 5/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "ManagerService.h"

@implementation ManagerService

- (void)getAllEventsSuccess:(void (^)(NSArray * arrEvents))successBlock
                    failure:(void (^)())failureBlock
{

//    [[ServiceManager getInstance] handleRequestwithMethod:@"POST" andPath:[kBaseAddress stringByAppendingString:kAPIPathGetAllEvents] parameters:nil successBlock:^(NSDictionary *responseDict)
//     {
//         NSLog(@"menegar server: Succssfully getAllEvents");
//         
//         // Get the response status.
//         BOOL status = [[responseDict objectForKey:@"status"] boolValue];
//         
//         if (status)
//         {
//             NSArray * arrEvents =[Event eventsFromArray:[responseDict objectForKey:@"data"]];
//             // Invoke the success block.
//             successBlock(arrEvents);
//         }
//
//    } failureBlock:^(AppError *error)
//    {
//          failureBlock();
//        [error displayErrorMessage];
//         NSLog(@"menegar server: failure getAllEvents");
//    }];
    // Create our parameters dictionary
    NSDictionary *params = @{@"Void":@""};
    
    NSString *path = [NSString stringWithFormat:@"%@%@", kBaseAddress, @"api/getAllEvents.php"];
    
    // Perform our request
    [[ServiceManager getInstance] handleRequestwithMethod:@"POST" andPath:path parameters:params successBlock:^(NSDictionary *responseDict) {
        
        NSArray * arrEvents =[Event eventsFromArray:[responseDict objectForKey:@"data"]];
        //             // Invoke the success block.
        successBlock(arrEvents);
        
    } failureBlock:^(AppError *error) {
        
        // Display the right error message to the user
        [error displayErrorMessage];
    }];
}
- (void)getWishesForEvent:(int )event
                  Success:(void (^)(NSArray * arrWishes))successBlock
                  failure:(void (^)())failureBlock
{
    
    NSDictionary *dicParms = @{kAPIKeyEvent:[NSNumber numberWithInt:event]};
    [[ServiceManager getInstance] handleRequestwithMethod:@"POST" andPath:[kAPIBaseURL stringByAppendingString:kAPIPathGetWishes] parameters:dicParms successBlock:^(NSDictionary *responseDict)
     {
         NSLog(@"menegar server: Succssfully getWishes");
         
         // Get the response status.
         BOOL status = [[responseDict objectForKey:@"status"] boolValue];
         
         if (status)
         {
             NSArray * arrWishes =[Wish wishesFromArray:[responseDict objectForKey:@"data"]];
             // Invoke the success block.
             successBlock(arrWishes);
         }
         
     } failureBlock:^(AppError *error)
     {
           failureBlock();
        [error displayErrorMessage];
         NSLog(@"menegar server: failure getWishes");
     }];

}

- (void)getWorldLocationsForEvent:(int)event
                          Success:(void (^)(NSArray * arrWorldLocations))successBlock
                          failure:(void (^)())failureBlock
{
    NSDictionary *dicParms = @{kAPIKeyEvent:[NSNumber numberWithInt:event]};
    [[ServiceManager getInstance] handleRequestwithMethod:@"POST" andPath:[kAPIBaseURL stringByAppendingString:kAPIPathGetWorldLocations] parameters:dicParms successBlock:^(NSDictionary *responseDict)
     {
         NSLog(@"menegar server: Succssfully getWorldLocations");
         
         // Get the response status.
         BOOL status = [[responseDict objectForKey:@"status"] boolValue];
         
         if (status)
         {
//             NSArray * arrWorldLocation =[WorldLocation worldLocationsFromArray:[responseDict objectForKey:@"data"]];
             // Invoke the success block.
             successBlock(responseDict[@"data"]);
         }
         
     } failureBlock:^(AppError *error)
     {
         failureBlock();
         [error displayErrorMessage];
         NSLog(@"menegar server: failure getWorldLocations");
     }];

}

- (void)addWishWithInfo:(NSDictionary *)dicInfo andFile:(NSData*)file
                Success:(void (^)())successBlock
                failure:(void (^)())failureBlock
{
    NSArray *arrFiles= nil;
    if (file)
    {
         MultiPartFile *multiPartFile = [[MultiPartFile alloc] initWithServerKey:kAPIKeyImg andData:file  andFileName:kAPIKeyImg andMimeType:MimeTypeImageJPG];
        arrFiles =[NSArray arrayWithObject:multiPartFile];
    }
    [[ServiceManager getInstance]handleMultiPartRequestWithMethod:@"POST" andPath:[kAPIBaseURL stringByAppendingString:kAPIPathAddWish] parameters:dicInfo andMultiPartFiles:arrFiles successBlock:^(NSDictionary *responseDict) {
        NSLog(@"menegar server: Succssfully addWish");
        
        // Get the response status.
        BOOL status = [[responseDict objectForKey:@"status"] boolValue];
        if (status)
        {
            
            successBlock();
        }

    } failureBlock:^(AppError *error) {
          failureBlock();
        [error displayErrorMessage];
          NSLog(@"menegar server: failure addWish");
    }];

}
@end
