//
//  SoundHandler.h
//
//  Created by Sharon Brizinov on 11/22/11.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "AudioToolbox/AudioToolbox.h"
#import <MediaPlayer/MediaPlayer.h>
//import "Utils.h"

@interface SoundHandler : NSObject<AVAudioPlayerDelegate>
{
}
+(SoundHandler*)getSharedSingleton;
+(void)playSoundWithSoundName:(NSString*)name withFormat:(NSString*)format  shouldPlayIfIpodPlaying:(BOOL)shouldPlay;
+(void)playMP3SoundWithSoundName:(NSString*)name  shouldPlayIfIpodPlaying:(BOOL)shouldPlay; 
+(BOOL) isSilentMode;
+(void)playSoundWithSoundName:(NSString*)name withFormat:(NSString*)format shouldPlayIfIpodPlaying:(BOOL)shouldPlay withNumberOfLoop:(int)loops;
+(void)stopAllMusic;
@end
