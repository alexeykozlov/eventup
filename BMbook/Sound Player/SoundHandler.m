//
//  SoundHandler.m
//
//  Created by Sharon Brizinov on 11/22/11.
//

#import "SoundHandler.h"

@implementation SoundHandler

SoundHandler * s;
AVAudioPlayer* audioPlayerActive;
BOOL wasPlayedBefore = NO;

+(SoundHandler*)getSharedSingleton
{
    if(!s)
    {
        if([[super alloc]init])
        {
            s = [[SoundHandler alloc]init];
        }
    }
    return s;
}

+(void)stopAllMusic
{
    if(audioPlayerActive && [audioPlayerActive isPlaying]) 
        [audioPlayerActive stop];
}

+(void)playSoundWithSoundName:(NSString*)name withFormat:(NSString*)format shouldPlayIfIpodPlaying:(BOOL)shouldPlay withNumberOfLoop:(int)loops
{
    if(audioPlayerActive && [audioPlayerActive isPlaying]) 
        [audioPlayerActive stop];
    
    
    // Pause the music and than
    if ([[MPMusicPlayerController iPodMusicPlayer] playbackState] == MPMusicPlaybackStatePlaying) 
    {
        // if we don't need to play if the iPod is playing we quit
        if(!shouldPlay)
            return;
        wasPlayedBefore = YES;
        [[MPMusicPlayerController iPodMusicPlayer] pause];
    }
    else 
        wasPlayedBefore = NO;
    
    
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@.%@", [[NSBundle mainBundle] resourcePath],name,format]];
	NSError *error;
	AVAudioPlayer* audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    audioPlayer.delegate = [SoundHandler getSharedSingleton];
    
	audioPlayer.numberOfLoops = loops;
    [audioPlayer prepareToPlay];
    
    audioPlayerActive = audioPlayer;
    
	if (audioPlayer == nil)
		NSLog(@"%@",[error description]);
	else
		[audioPlayer play];
}
+(void)playSoundWithSoundName:(NSString*)name withFormat:(NSString*)format shouldPlayIfIpodPlaying:(BOOL)shouldPlay
{
 
    [SoundHandler playSoundWithSoundName:name withFormat:format shouldPlayIfIpodPlaying:shouldPlay withNumberOfLoop:0];

}
+(void)playMP3SoundWithSoundName:(NSString*)name  shouldPlayIfIpodPlaying:(BOOL)shouldPlay
{
    [SoundHandler playSoundWithSoundName:name withFormat:@"mp3" shouldPlayIfIpodPlaying:shouldPlay];
}
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    // if the music was played before we will continue playing it
    if(wasPlayedBefore)
      [[MPMusicPlayerController iPodMusicPlayer] play];
    wasPlayedBefore = NO;
    
    [player release];
    audioPlayerActive = nil;
}

+ (BOOL) isSilentMode
{
    UInt32 routeSize = sizeof (CFStringRef);
    CFStringRef route;
    
    AudioSessionGetProperty (
                             kAudioSessionProperty_AudioRoute,
                             &routeSize,
                             &route
                             );
    if(route == NULL)
        NSLog(@"Silent Is active");
    
    return (route == NULL); // if no rout == silent mode is active
}     
@end
