//
//  InviteViewController.m
//  BMbook
//
//  Created by iApps Mac Mini on 9/10/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "InviteViewController.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>

@interface InviteViewController ()
{
    BOOL _isLoginFacebook;
}

@end

@implementation InviteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=@"הזמן לאירוע";
	//sets the event details
    self.name=[[NSUserDefaults standardUserDefaults]stringForKey:kAPIKeyEventName];
    self.adress=[[NSUserDefaults standardUserDefaults]stringForKey:kAdress];
    self.code=[[NSUserDefaults standardUserDefaults]stringForKey:kAPIKeyKeyword].intValue;
    //rounded corners to the image
    self.imgInvitation.layer.cornerRadius=5;
    self.imgInvitation.clipsToBounds = YES;
    //sets the invitation image
    NSURL *url=[NSURL URLWithString:[[NSUserDefaults standardUserDefaults]stringForKey:@"invitationUrl"]];
    if(url!=NULL)
    {
        [self.imgInvitation setImageWithURL:url];
        [self.lblNoInvitation setHidden:YES];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    self.screenName=@"Invite to event screen";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - button methods
- (IBAction)inviteInEmail:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc]init];
        //[mailComposer setToRecipients:[NSArray arrayWithObject:kEmailOfContactUs]];
        [mailComposer setSubject:NSLocalizedString(@"EventUp", nil)];
        
        // [mailComposer setMessageBody:@"היי!\nכדאי שתוריד/י את אפליקציית הבר מצווה של יונתן\nhttps://itunes.apple.com/us/app/bar-mitzva-book/id652071738?ls=1&mt=8" isHTML:TRUE];
        //
        // Fill out the email body text
        NSString *iTunesLink = @"https://itunes.apple.com/us/app/bar-mitzva-book/id652071738?ls=1&mt=8"; // Link to iTune App link
        NSString *content = @"הינכם מוזמנים לאירוע: ";
        NSString *dateText=@"שייתקים בתאריך: ";
        NSString *adressText=@"בכתובת: ";
        NSString *codeText=@"הורידו את האפליקציה וכנסו לאירוע שלנו בעזרת הקוד: ";
        //set the date
        NSTimeInterval epoch = [[[NSUserDefaults standardUserDefaults]stringForKey:kAPIKeyDateEvent] doubleValue];
        self.date=[NSDate dateWithTimeIntervalSince1970:epoch];
        NSDateFormatter* formatterDate = [[NSDateFormatter alloc] init];
        [formatterDate setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
        NSString* strDate = [formatterDate stringFromDate:self.date];
        
        //sets the invitation image
        NSURL *url=[NSURL URLWithString:[[NSUserDefaults standardUserDefaults]stringForKey:@"invitationUrl"]];
        NSData *imageData=[NSData dataWithContentsOfURL:url];
//        Invitation *invitation=[Invitation MR_findFirst];
//        NSData *data = invitation.image;
//        [mailComposer addAttachmentData:data mimeType:@"image/jpg" fileName:@"Picture"];
        NSString *emailBody = [NSString stringWithFormat:@"%@%@<br/>%@%@<br/>%@%@<br/>%@%d<p>\r\n<a href = '%@'>%@</a></p>  ", content,self.name,dateText,strDate,adressText,self.adress,codeText,self.code, iTunesLink,iTunesLink];
        [mailComposer addAttachmentData:imageData mimeType:@"image/png" fileName:@"invitation"];
        [mailComposer setMessageBody:emailBody isHTML:YES];
        [mailComposer setMailComposeDelegate:self];
        [self presentViewController:mailComposer animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"תקלה", nil)  message:NSLocalizedString(@"לא ניתן לשלוח מייל,אנא בדוק אם יש חשבון דואר מוגדר במכשיר.",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"סגור" ,nil)otherButtonTitles:nil];
        [alert show];
    }
}
- (IBAction)inviteInWhatsApp:(id)sender
{
    //set the date
    NSTimeInterval epoch = [[[NSUserDefaults standardUserDefaults]stringForKey:kAPIKeyDateEvent] doubleValue];
    self.date=[NSDate dateWithTimeIntervalSince1970:epoch];
    NSDateFormatter* formatterDate = [[NSDateFormatter alloc] init];
    [formatterDate setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    NSString* strDate = [formatterDate stringFromDate:self.date];
    
    NSString *iTunesLink = @"https://itunes.apple.com/us/app/bar-mitzva-book/id652071738?ls=1&mt=8"; // Link to iTune App link
    NSString *content = @"הינכם מוזמנים לאירוע: ";
    NSString *dateText=@"שייתקים בתאריך: ";
    NSString *adressText=@"בכתובת: ";
    NSString *codeText=@"הורידו את האפליקציה וכנסו לאירוע שלנו בעזרת הקוד: ";
    NSString *messege=[NSString stringWithFormat:@"%@%@ %@%@ %@%@ %@%d \n%@",content,self.name,dateText,strDate,adressText,self.adress,codeText,self.code,iTunesLink];
    NSString *whatsApp=@"whatsapp://send?text=";
    
    NSString *encodedUrl=(NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef)messege,NULL,(CFStringRef)@"!*'();:@&=+$,/?%#[]",kCFStringEncodingUTF8 ));
    
    
    
    NSString *stringUrl= [NSString stringWithFormat:@"%@%@",whatsApp,encodedUrl];
  
    
   
    
    NSURL *whatsappURL = [NSURL URLWithString:stringUrl];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication] openURL: whatsappURL];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"תקלה", nil)  message:NSLocalizedString(@"WhatsApp לא מותקן על המכשיר",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"סגור" ,nil)otherButtonTitles:nil];
        [alert show];
    }

//    if (FBSession.activeSession.state != FBSessionStateOpen)
//    {
//        _isLoginFacebook = FALSE;
//        AppDelegate *appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
//        
//        [appDelegate openSessionWithAllowLoginUI:YES];
//        [appDelegate sendRequestToiOSFriends];
//    }
//    else
//    {
//        AppDelegate *appDelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
//        [appDelegate sendRequestToiOSFriends];
//    }
}




#pragma mark MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
    
#if __IPHONE_OS_VERSION_MIN_REQUIRED < 60000
	[self dismissModalViewControllerAnimated:YES];
#else
    [self dismissViewControllerAnimated:YES completion:NULL];
#endif
}

@end
