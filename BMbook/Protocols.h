//
//  Protocols.h
//  BiBi
//
//  Created by Shoshi V. on 2/21/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ProtocolMainViewConroller <NSObject>

-(void)selectOneOption:(UIViewController *)viewController;
-(void)pushFirstViewController;
@end

 
@protocol ProtocolSlide <NSObject>
@optional
- (void)allowInteractiveSlideing:(BOOL)allow;
-(void)showMainMenu:(id)sender;
@end

@protocol ProtocolOneImageOnAlbum <NSObject>
-(void)selectOneImageWithIndex:(int)index;


@end