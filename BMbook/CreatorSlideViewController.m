//
//  CreatorSlideViewController.m
//  BMbook
//
//  Created by iApps Mac Mini on 9/10/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "CreatorSlideViewController.h"

@interface CreatorSlideViewController ()

@end

@implementation CreatorSlideViewController

-(id)init
{
    if([super init])
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle: nil];
        
        self.creatorMainVC = [mainStoryboard instantiateViewControllerWithIdentifier:kIdentifierCreatorMainVC];
        self.creatorMainVC.delegate = self;
        
        
        self.settingsVC = [mainStoryboard instantiateViewControllerWithIdentifier:kIdentifierSettingsVC];
        self.settingsVC.delegate = self;
        
        
        self.nav = [[UINavigationController alloc]  initWithNavigationBarClass:[UINavigationBar class] toolbarClass:[UIToolbar class]];
        [self.nav pushViewController:self.settingsVC animated:NO];
        
        //        // Load the initial view controller.
        //        InitialViewController *initialController = [mainStoryboard instantiateViewControllerWithIdentifier:@"InitialViewControllerID"];
        //        [self.nav pushViewController:initialController animated:NO];
        
        //sets nav bar background
        [self.nav.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bar"] forBarMetrics:UIBarMetricsDefault];
        self.staticViewController = self.creatorMainVC;
        self.slidingViewController = self.nav;
        [self btnSlidepressed:nil];
        
        //sets navigation bar color
        self.nav.navigationBar.titleTextAttributes = @{UITextAttributeTextColor : [UIColor blackColor], UITextAttributeTextShadowColor: [UIColor blackColor]};
    }
    return self;
}

- (void)playButtonsOfSlidingView:(BOOL)play
{
//    if ([self.nav.visibleViewController isKindOfClass:[JonatanInWorldVC class]])
//    {
//        [self allowInteractiveSlideing:!play];
//    }
}

-(void)btnSlidepressed:(id)sender
{
    if (isStaticViewVisible)
    {
        if (sender == nil)
        {
            [self showSlidingViewAnimated:NO];
        }
        else
        {
            [self showSlidingViewAnimated:YES];
        }
    }
    else
    {
        if (sender == nil)
        {
            [self hideSlidingViewAnimated:NO];
        }
        else
        {
            [self hideSlidingViewAnimated:YES];
        }
        
    }
}

#pragma mark - ProtocolMainViewConroller
-(void)selectOneOption:(UIViewController *)viewController
{
    ((SlideOptionViewController *)viewController).delegate = self;
    [self.nav popToRootViewControllerAnimated:NO];
    [self.nav pushViewController:viewController animated:NO];
    if (isStaticViewVisible)
    {
        [self showSlidingViewAnimated:NO];
    }
    else
    {
        [self hideSlidingViewAnimated:NO];
    }
    
}

#pragma mark - ProtocolSlide
- (void)allowInteractiveSlideing:(BOOL)allow
{
    self.allowInteractiveSlideing = allow;
    
}
-(void)showMainMenu:(id)sender
{
    [self allowInteractiveSlideing:YES];
    [self btnSlidepressed:sender];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
