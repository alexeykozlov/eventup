//
//  CustomPlacemark.h
//  BMbook
//
//  Created by Shoshi V. on 5/20/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "WorldLocation.h"
@interface CustomPlacemark : NSObject <MKAnnotation>

@property (nonatomic, strong)WorldLocation *worldLocation;
@property (nonatomic,assign )int tag;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, readonly) NSString *strTitle;
@property (nonatomic, readonly) NSString *strSubtitle;

-(id)initWithCoordinateLat:(double)dLat Long:(double)dLong Title:(NSString*)title Subtitle:(NSString*)subtitle WordLocation:(WorldLocation *)newWordLocation;
-(id)initWithCoordinate:(CLLocationCoordinate2D) c;
-(NSString *)subtitle;
-(NSString *)title;
-(id)initWithCoordinateLat:(double)dLat Long:(double)dLong WordLocation:(WorldLocation *)newWordLocatio;
@end
