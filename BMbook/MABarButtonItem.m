//
//  MABarButtonItem.m
//  MatchApp
//
//  Created by Itamar Biton on 11/29/12.
//  Copyright (c) 2012 iApps. All rights reserved.
//

#import "MABarButtonItem.h"

@implementation MABarButtonItem

- (id)initWithTitle:(NSString *)title andBackgroundImage:(UIImage *)backgroundImage
{
    // Create the custom view (button) for the bar button item.
    int buttonWidth = backgroundImage.size.width;
    int buttonHeight = backgroundImage.size.height;
    UIButton *customButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, buttonWidth, buttonHeight)];
    [customButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
    [customButton setTitle:title forState:UIControlStateNormal];
    
    // Initialize the bar button item.
    self = [super initWithCustomView:customButton];
    
    return self;
}

- (id)initWithTitle:(NSString *)title andBackgroundImage:(UIImage *)backgroundImage target:(id)target action:(SEL)action
{
    // Create the custom view (button) for the bar button item.
    int buttonWidth = backgroundImage.size.width;
    int buttonHeight = backgroundImage.size.height;
    self.customButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, buttonWidth, buttonHeight)];
    [self.customButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
    [self.customButton setTitle:title forState:UIControlStateNormal];
    [self.customButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [[self.customButton titleLabel] setFont:[UIFont boldSystemFontOfSize:12]];
    [[self.customButton titleLabel] setShadowColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5]];
    [[self.customButton titleLabel] setShadowOffset:CGSizeMake(0, -1)];
    
    // Initialize the bar button item.
    self = [super initWithCustomView:self.customButton];
    
    return self;
}

- (void)setTitle:(NSString *)title
{
    // Call the method of the superclass first.
    [super setTitle:title];
    
    // Set the title on the custom view.
    [self.customButton setTitle:title forState:UIControlStateNormal];
}

@end
