//
//  CreatorMainViewController.m
//  BMbook
//
//  Created by iApps Mac Mini on 9/10/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "CreatorMainViewController.h"
#import "ShowAlbumBlessingsVC.h"

@interface CreatorMainViewController ()

@end

@implementation CreatorMainViewController

- (BOOL) shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#define knumberOfRows 7

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //sets the table background
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"sidemenu_bg.png"]];
    //init array with titels
    _arrTitles = [NSArray arrayWithObjects:NSLocalizedString(@"הגדרות", nil),
                    NSLocalizedString(@"ניהול ברכות",nil),
                    NSLocalizedString(@"סטטיסטיקה", nil),
                    NSLocalizedString(@"הזמן לאירוע", nil),
                    NSLocalizedString(@"יציאה",nil),nil];
    //init array with icons
    _arrIcons = [NSArray arrayWithObjects:@"sidemenu_settings",@"sidemenu_send_braha",@"sidemenu_stats",@"sidemenu_invite",@"sidemenu_logout",nil];
    
    _selectedRow = 1;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return knumberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if (indexPath.row >0 && indexPath.row -1 <_arrTitles.count)
    {
        //create cell
        CustomCellMain *customcell = (CustomCellMain *)[tableView dequeueReusableCellWithIdentifier:@"CustomCellMain"];
        
        //set text to lblTitle
        customcell.lblTitle.text = [_arrTitles objectAtIndex:indexPath.row -1];
        
        
        // Highlight selected row
        if (_selectedRow == indexPath.row)
        {
            customcell.lblTitle.textColor =[UIColor colorWithRed:255/255.0 green:220/255.0 blue:90/255.0 alpha:1];
            customcell.imagVIcon.image = [UIImage imageNamed:[[_arrIcons objectAtIndex:indexPath.row -1] stringByAppendingString:@"_highlight"]];
        }
        else
        {
            customcell.lblTitle.textColor =[UIColor darkGrayColor];
            customcell.imagVIcon.image = [UIImage imageNamed:[_arrIcons objectAtIndex:indexPath.row -1]];
        }
        cell = customcell;
        
        
    }
    else if (indexPath.row == 0)//title of table
    {
        CustomCellOnlyText * customCellOnlyText =(CustomCellOnlyText *)[tableView dequeueReusableCellWithIdentifier:@"CustomCellOnlyText"];
        
        //set text to lblTitle
        customCellOnlyText.appNameImage.image=[UIImage imageNamed:@"sidemenu_title.png"];;
        cell = customCellOnlyText;
        
    }
    else//symbol
    {
        CustomCellWithSymbol *customCellWithSymbol =(CustomCellWithSymbol *)[tableView dequeueReusableCellWithIdentifier:@"CustomCellWithSymbol"];
        customCellWithSymbol.imageV.image=[UIImage imageNamed:@"sidemenu_decoration.png"];
        cell =customCellWithSymbol;
    }
    return cell;;
}

- (float)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row ==0||indexPath.row -1 <_arrTitles.count)
    {
        return 44;
    }
    //last row with image
    return self.view.frame.size.height - (44*(knumberOfRows -1));
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle: nil];
    if (indexPath >0 &&indexPath.row -1 <_arrTitles.count)
    {
        //draw old selected row
        CustomCellMain *cell = (CustomCellMain *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:_selectedRow inSection:0]];
        cell.lblTitle.textColor =[UIColor darkGrayColor];
        cell.imagVIcon.image = [UIImage imageNamed:[_arrIcons objectAtIndex:_selectedRow -1]];
    
        //draw new selected row
        _selectedRow = indexPath.row;
        cell = (CustomCellMain *)[tableView cellForRowAtIndexPath:indexPath];
        cell.lblTitle.textColor =[UIColor colorWithRed:255/255.0 green:220/255.0 blue:90/255.0 alpha:1];
        cell.imagVIcon.image = [UIImage imageNamed:[[_arrIcons objectAtIndex:indexPath.row -1] stringByAppendingString:@"_highlight"]];
    
        //defines a view to each selected row
        switch (indexPath.row)
        {
            case kOptionToSettings:
            {
                SettingsViewController *settingsviewController = [mainStoryboard instantiateViewControllerWithIdentifier:kIdentifierSettingsVC];
                [self.delegate selectOneOption:settingsviewController];
                break;
            }
                case kOptionToEditWishes:
            {
                ShowAlbumBlessingsVC *showAlbumBlessingsVC = [mainStoryboard instantiateViewControllerWithIdentifier:kIdentifierShowAlbumBlessingsVC];
                [self.delegate selectOneOption:showAlbumBlessingsVC];
                break;
            }
            case kOptionToStatistics:
            {
                StatisticsViewController *statisticsviewcontroller = [mainStoryboard instantiateViewControllerWithIdentifier:kIdentifierStatisticsVC];
                [self.delegate selectOneOption:statisticsviewcontroller];
                break;
            }
            case kOptionToInvite:
            {
                InviteViewController *inviteviewcontroller = [mainStoryboard instantiateViewControllerWithIdentifier:kIdentifierInviteVC];
                [self.delegate selectOneOption:inviteviewcontroller];
                break;
            }
            case kOptionToGoback:
            {
                UIAlertView *alert =[[UIAlertView alloc] initWithTitle:nil message:@"האם ברצונך להתנתק מהאירוע?" delegate:self cancelButtonTitle:@"בטל" otherButtonTitles:@"כן", nil];
                [alert show];
                break;
               

            }
        }
    }
}

#pragma mark - UIAlertview delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex)
    {
        //logs out with the session id
        [GeneralManager logOutWithSessionId:[[NSUserDefaults standardUserDefaults] stringForKey:kAPIKeyEventID]
                                    Success:^{
                                        //exits from creator mode
                                        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kIsCreator];
                                        
                                        
                                        [self dismissViewControllerAnimated:YES completion:nil];
                                        
                                    }
                                    failure:^{}];
    }
    
}

@end
