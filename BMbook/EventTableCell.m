//
//  EventTableCell.m
//  BMbook
//
//  Created by iApps Mac Mini on 9/30/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "EventTableCell.h"

@implementation EventTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
