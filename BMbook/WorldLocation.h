//
//  WorldLocation.h
//  BMbook
//
//  Created by Shoshi V. on 5/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WorldLocation : NSObject
@property (assign ,nonatomic) int nID;
@property (assign ,nonatomic) float fLat;
@property (assign ,nonatomic) float fLon;
@property (strong ,nonatomic) NSString * strTitle;
@property (strong ,nonatomic) NSString * strDesc;
@property (strong ,nonatomic)  NSURL *urlImage;
@property (strong ,nonatomic)  NSURL *urlSmallImage;
+ (NSArray *)worldLocationsFromArray:(NSArray *)array;
+ (WorldLocation *)worldLocationWithDictionary:(NSDictionary *)dictionary;
- (id)initWithDictioanry:(NSDictionary *)dictionary;
@end
