//
//  GeneralManager.h
//  BMbook
//
//  Created by iApps Mac Mini on 9/9/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GeneralManager : NSObject

+(void)registerEventWithMail:(NSString*)strEmail andPassword:(NSString*)strPassword andAdress:(NSString*)strAdress andAllowGifts:(BOOL)bAllowGifts andName:(NSString*)strName andCode:(int)nCode andDate: (int)dateEvent
                      Success:(void (^)())successBlock
                      failure:(void (^)())failureBlock;

+(void) logInWithEmail:(NSString*)strEmail andPassword:(NSString*)strPassword
                Success:(void (^)())successBlock
                failure:(void (^)())failureBlock;

+(void) setSessionId:(NSString*)sessionId;

+(void) logOutWithSessionId:(NSString*)sessionId
                    Success:(void (^)())successBlock
                    failure:(void (^)())failureBlock;

+(void) updateEventDetailsWithSessionId:(NSString*)sessionId andName:(NSString*)name andDate:(int)date andAdress:(NSString*)adress andAllowGifts:(BOOL)allowGifts andEmail:(NSString*)email andPassword:(NSString*)password andCode:(int)code andSong:(NSString*)song
                    Success:(void (^)())successBlock
                    failure:(void (^)())failureBlock;

@end
