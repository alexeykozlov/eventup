//
//  ShowBigPhotosViewController.m
//  BMbook
//
//  Created by Shoshi V. on 5/19/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "ShowBigPhotosViewController.h"

@interface ShowBigPhotosViewController ()
{
    int _visiblePage;
}
@end

@implementation ShowBigPhotosViewController

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self initViewPagesWithPageVisble:self.indexOfSelectedPhoto];
    [self.navigationController.navigationBar setHidden:FALSE];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //self.title = NSLocalizedString(@"אלבום הברכות", nil);
    //set locatio of right and left buttons
	if(IS_IPHONE_5 )
    {
        self.leftButton.frame = CGRectMake(self.leftButton.frame.origin.x,
                                           161,
                                           self.leftButton.frame.size.width ,
                                           self.leftButton.frame.size.height);
        self.rightButton.frame = CGRectMake(self.rightButton.frame.origin.x,
                                            161,
                                            self.rightButton.frame.size.width ,
                                            self.rightButton.frame.size.height);
        self.leftImage.frame = CGRectMake(self.leftImage.frame.origin.x,
                                          187,
                                          self.leftImage.frame.size.width ,
                                          self.leftImage.frame.size.height);
        self.rightImage.frame = CGRectMake(self.rightImage.frame.origin.x,
                                           187,
                                           self.rightImage.frame.size.width ,
                                           self.rightImage.frame.size.height);
    }
    else
    {
        self.leftButton.frame = CGRectMake(self.leftButton.frame.origin.x,
                                           141,
                                           self.leftButton.frame.size.width ,
                                           self.leftButton.frame.size.height);
        self.rightButton.frame = CGRectMake(self.rightButton.frame.origin.x,
                                            141,
                                            self.rightButton.frame.size.width ,
                                            self.rightButton.frame.size.height);
        self.leftImage.frame = CGRectMake(self.leftImage.frame.origin.x,
                                          167,
                                          self.leftImage.frame.size.width ,
                                          self.leftImage.frame.size.height);
        self.rightImage.frame = CGRectMake(self.rightImage.frame.origin.x,
                                           167,
                                           self.rightImage.frame.size.width ,
                                           self.rightImage.frame.size.height);
}
    
    //set leftBarButtonItem
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:[UIImage imageNamed:@"nav_bar_back_btn"] forState:UIControlStateNormal];
    btn.frame = CGRectMake(0, 0, 25, 27);
    [btn addTarget:self action:@selector(popController) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    //set rightBar delete button
    if([[NSUserDefaults standardUserDefaults] boolForKey:kIsCreator]){
        [self.navigationItem setRightBarButtonItem:[[MABarButtonItem alloc] initWithTitle:@"" andBackgroundImage:[UIImage imageNamed:@"pah"] target:self action:@selector(deleteAlert)]];
    }
}

-(void) viewDidAppear:(BOOL)animated
{
    self.screenName=@"Single blessing screen";
}

#pragma mark - delete methods
//deletes the wish
- (void)deleteWish
{
    Wish *selectedWish=self.arrPhotos[self.indexOfSelectedPhoto];
    [EventManager deleteWishWithID:[[NSUserDefaults standardUserDefaults] stringForKey:kAPIKeyEventID].intValue andKeyword:[[NSUserDefaults standardUserDefaults] stringForKey:kAPIKeyKeyword].intValue andWishId:selectedWish.nID
                           Success:^{
                               [self.navigationController popViewControllerAnimated:YES];
                           }
                           failure:^{
                               
                           }];
}
//shows an alert when delete was clicked
-(void)deleteAlert
{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"אזהרה"
                                                      message:@"האם ברצונך למחוק לצמיתות את הברכה?"
                                                     delegate:self
                                            cancelButtonTitle:@"לא"
                                            otherButtonTitles:@"כן", nil];
    [message show];
}
//sets the buttons of the alert
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"לא"])
    {

    }
    else if([title isEqualToString:@"כן"])
    {
        [self deleteWish];
    }
    
}

- (void)popController
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initViewPagesWithPageVisble:(int)page
{
    self.arrViewsOfPhotos = [[NSMutableArray alloc] init];
    
    NSMutableArray *arrPages = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < [self.arrPhotos count]; i++)
    {
		[arrPages addObject:[NSNull null]];
    }
    self.arrViewsOfPhotos = arrPages;
    //init scrollview
    for (UIView *view in self.scrollView.subviews)
    {
        [view removeFromSuperview];
    }
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * [self.arrPhotos count], self.scrollView.frame.size.height);
    
    Wish *wish = (Wish *)[self.arrPhotos objectAtIndex:page];
    self.title = wish.strAuthorName;
    
    [self loadScrollViewWith3Pages:page];
    _visiblePage = page;
    [self updateScrollView];
    
}
#pragma arrows functions
- (IBAction)btnArrowLeftPressed
{
    [self loadScrollViewWith3Pages:_visiblePage -1];
    if (_visiblePage -1 >= 0)
    {
        _visiblePage --;
    }
    
    Wish *wish = (Wish *)[self.arrPhotos objectAtIndex:_visiblePage];
    self.title = wish.strAuthorName;
    
    [self updateScrollView];
}
- (IBAction)btnArrowRightPressed
{
    [self loadScrollViewWith3Pages:_visiblePage +1];
    if (_visiblePage +1 < [self.arrPhotos count])
    {
        _visiblePage ++;
    }
    
    Wish *wish = (Wish *)[self.arrPhotos objectAtIndex:_visiblePage];
    self.title = wish.strAuthorName;
    
    [self updateScrollView];
}

#pragma ScrollView functions

- (void)loadScrollViewWith3Pages:(int)page
{
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    // A possible optimization would be to unload the views+controllers which are no longer visible
    
//    Wish *wish = (Wish *)[self.arrPhotos objectAtIndex:page];
//    self.title = wish.strAuthorName;
    
}
- (void)updateScrollView
{
    // update the scroll view to the appropriate page
    CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * _visiblePage;
    frame.origin.y = 0;
    [self.scrollView scrollRectToVisible:frame animated:YES];
    
}
- (void)loadScrollViewWithPage:(int)page
{
    if (page < 0)
        return;
    if (page >= [self.arrPhotos count])
        return;
    
        OneWishView * oneWishView = [self.arrViewsOfPhotos objectAtIndex:page];
    if ((NSNull *)oneWishView == [NSNull null])
    {
        oneWishView = [[OneWishView alloc] init];
        [self.arrViewsOfPhotos replaceObjectAtIndex:page withObject:oneWishView];
        
        oneWishView.tag = page + 1;
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        oneWishView.frame = frame;
        
        Wish *wish = (Wish *)[self.arrPhotos objectAtIndex:page];
        //set text
        [oneWishView.textView setCenter:self.imgTextBackground.center];
        oneWishView.textView.text = wish.strText;
        //oneWishView.lblTitle.text = wish.strAuthorName;
        
        oneWishView.lblTitle.textColor=[UIColor colorWithRed:(255/255.0) green:(220/255.0) blue:(90/255.0) alpha:1] ;
        if (oneWishView.photo.image== nil)
        {
            // Add a progress indicator on the image view.
            UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            [indicator setHidesWhenStopped:YES];
            [indicator startAnimating];
            [indicator setCenter:oneWishView.photo.center];
            [self.view insertSubview:indicator belowSubview:oneWishView.photo];
            
            
            if (wish.urlImage)
            {
                NSURL *imageURL = wish.urlImage;
               __weak NSURLRequest *imageRequest = [NSURLRequest requestWithURL:imageURL];

                [oneWishView.photo setImageWithURLRequest:imageRequest
                                         placeholderImage:nil
                                                  success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                                      
                                                      // Stop the activity indicator and remove it from the view.
                                                      [indicator stopAnimating];
                                                      [indicator removeFromSuperview];
                                                      
                                                      // Transition the change if image.
                                                      [UIView transitionWithView:oneWishView.photo
                                                                        duration:0.5
                                                                         options:UIViewAnimationOptionTransitionCrossDissolve
                                                                      animations:^{
                                                                          oneWishView.photo.image = image;
                                                                      } completion:NULL];
                                                      
                                                  } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                                      NSLog(@"BIG PHOTOS: Failed to download image!"
                                                            @"\nError: %@", error.localizedDescription);
                                                  }];
                

            }
            
        }
    }
    [self.scrollView addSubview:oneWishView];
    
    
}
#pragma mark - Scroll View Delegate Methods
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = self.scrollView.frame.size.width;
    int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    _visiblePage = page;
    [self loadScrollViewWith3Pages:page];
    
}

@end