//
//  PickerCellDelegate.h
//  IDareU
//
//  Created by iApps Dev. Mac mini on 3/7/13.
//  Copyright (c) 2013 Roee Kattaby. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PickerTextField;

@protocol PickerTextFieldDelegate <NSObject>

-(void)didSelectPickerOption:(PickerTextField*)chosenTextField chosenIndex:(NSInteger)index chosenKey:(NSNumber*)key;
-(void)didCancelPicker:(PickerTextField*)chosenTextField;
-(void)didSelectDatePickerOption:(PickerTextField*)chosenTextField chosenDate:(NSDate*)date;

@end
