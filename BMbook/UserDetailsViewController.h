//
//  UserDetailsViewController.h
//  BMbook
//
//  Created by iApps Mac Mini on 9/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserDetailsViewController : GAITrackedViewController <UITextFieldDelegate>

//@property (strong, nonatomic) IBOutlet UITextField *newPassword;
//@property (strong, nonatomic) IBOutlet UITextField *confirmNewpassword;
//
//@property (strong, nonatomic) IBOutlet UITextField *newCode;
@property (strong, nonatomic) IBOutlet UITextField *Password;
@property (strong, nonatomic) IBOutlet UITextField *confirmpassword;
@property (strong, nonatomic) IBOutlet UITextField *Code;

@property (strong, nonatomic) IBOutlet UIButton *updateDetailsButton;

-(IBAction)updateDetails:(id)sender;
- (void)handleTap:(UITapGestureRecognizer *)recognizer;

@end
