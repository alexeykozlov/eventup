//
//  Globals.h
//  BMbook
//
//  Created by Shoshi V. on 5/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//
////
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)


#define kHeightIphone5 568
#define kHeightIphone4 480

#define kFBSessionStateChangedNotification @"IApps.BMbook:FBSessionStateChangedNotification"
#define kTrackingId                       @"UA-45215303-1"

////////////
typedef enum _OptionsInMainMenu
{
    kOptionToGiveBlessing =2,
    kOptionToShowAlbumBlessings =3,
    kOptionToSendGift =4,
    kOptionToShowInvitation =1,
    kOptionToNavigateMe  =5,
    kOptionToSong=6,
    kOptionToGoOtherEvent = 7,
    kOptionToShowInfo  =8
} _OptionsInMainMenu;

typedef enum _OptionsInCreatorMainMenu
{
    kOptionToSettings=1,
    kOptionToEditWishes=2,
    kOptionToStatistics=3,
    kOptionToInvite=4,
    kOptionToGoback=5
}_OptionsInCreatorMainMenu;

//////////////////////
//Segue
//////////////////////
#define kShowBigPhotosSegue @"ShowBigPhotosSegue"
#define kShowOnwWorldLocationSegue @"ShowOnwWorldLocationSegue"
#define kCellarix @"cellarixSegue"

//////////////////////
//identifier of view controller
//////////////////////
#define kIdentifierGiveBlessingVC         @"identifierGiveBlessingVC"
#define kIdentifierShowAlbumBlessingsVC   @"identifierShowAlbumBlessingsVC"
#define kIdentifierJonatanInWorldVC       @"identifierJonatanInWorldVC"
#define kIdentifierShowInvitationVC       @"identifierShowInvitationVC"
#define kIdentifierNavigateMeVC           @"identifierNavigateMeVC"
#define kIdentifierShowInfoVC             @"identifierShowInfoVC"
#define kIdentifierMainMenuVC             @"identifierMainMenuVC"
#define kIdentifierFirstVC                @"identifierFirstVC"
#define kIdentifierCreatorMainVC          @"identifierCreatorMainVC"
#define kIdentifierSettingsVC             @"identifierSettingsVC"
#define kIdentifierEditWishesVC           @"identifierEditWishesVC"
#define kIdentifierStatisticsVC           @"identifierStatisticsVC"
#define kIdentifierInviteVC               @"identifierInviteVC"
#define kIdentifierSendGiftVC             @"identifierSendGiftVC"
#define kIdentifierUserDetailsVC          @"identifierUserDetailsVC"
#define kIdentifierDateAndTimeVC          @"identifierDateAndTimeVC"
#define kIdentifierSongVC                 @"identifierSongVC"
#define KIdentifierCellarixVC             @"identifierCellarixVC"

//////////////////////
//server
//////////////////////
  
#define kAPIBaseURL                  @"http://iappsdev.mobi/App_BarMitzva/"
#define kAPIPathAddWish              @"api/addWish.php"
#define kAPIPathGetAllEvents         @"api/getAllEvents.php"
#define kAPIPathGetWishes            @"api/getWishes.php"
#define kAPIPathGetWorldLocations    @"api/getWorldLocations.php"
#define kAPIPathURLSplash            @"http://iappsdev.mobi/App_BarMitzva/img/splash@2x.png"
#define kAPIPathURLSplashForIphone5  @"http://iappsdev.mobi/App_BarMitzva/img/splash_iphone5.png"

//////////////////////
//Key
//////////////////////
#define kAPIKeyThumb               @"thumb"
#define kAPIKeyEventWorldLocations @"worldLocations"
#define kNotFirstEnter @"notFirstEnter"
#define kNotFirstCreatorEnter @"notFirstCreatorEnter"

//#define kAPIKeyPlaySound       @"playSound"
#define kAPIKeyEvent           @"event"
#define kAPIKeyKeyword         @"keyword"
#define kAPIKeyAuthorName      @"authorName"
#define kAPIKeyText            @"text"
#define kAPIKeyImg             @"img"
#define kAPIKeyEventID         @"eventID"
#define kAPIKeyEventName       @"eventName"
#define kAPIKeyDateEvent       @"dateEvent"
#define kBaseAddress           @"http://www.iappsdev.mobi/App_EventUp/"
#define KSessionId             @"sessionId"
#define kNumberOfEnteries      @"enteries"
#define kUserName              @"username"
#define kAdress                @"adress"
#define kAllowGifts            @"allowGifts"
#define kIsCreator             @"isCreator"
#define kAPIKeySong            @"song"
#define kAPIKeyPhone           @"phone"
#define kDidExit               @"exit"
#define kInvitationUrl         @"invitationUrl"
#define kLogIn                 @"logIn"
//////////////////////
//locations
//////////////////////

#define kTilteEvent1         NSLocalizedString(@"עלייה לתורה של יונתן",nil)
#define kTilteEvent2         NSLocalizedString(@"מסיבת הבר-מצווה של יונתן",nil)
#define kDescEvent1          NSLocalizedString(@"מוזיאון המזגגה,\nבכניסה המערבית לקיבוץ נחשולים - 11:00 בבוקר",nil)
#define kDescEvent2          NSLocalizedString(@"בית כנסת אוהל יעקב\nברחוב הנדיב 19 במושבה זכרון יעקב - 8:30 בבוקר",nil)
#define kLocationEvent1      NSLocalizedString(@"מוזיאון המזגגה, נחשולים",nil)
#define kLocationEvent2      NSLocalizedString(@"הנדיב 19,זכרון יעקב",nil)

/////////////////////
//imports
/////////////////////
#import "CreateEventViewController.h"
#import "LogInViewController.h"
#import "Protocols.h"
#import "CustomCellMain.h"
#import "CustomCellOnlyText.h"
#import "CustomCellWithSymbol.h"
#import "SettingsViewController.h"
#import "CreatorMainViewController.h"
#import "CreatorSlideViewController.h"
#import "EditWishesViewController.h"
#import "StatisticsViewController.h"
#import "InviteViewController.h"
#import "SendGiftVC.h"
#import "SettingsViewController.h"
#import "Generalmanager.h"
#import "statisticsManager.h"
#import "EventManager.h"
#import "MABarButtonItem.h"
#import "EventTableCell.h"
#import "CoreData+MagicalRecord.h"
#import "Invitation.h"
#import "GAI.h"
#import "GAITrackedViewController.h"
#import "SongViewController.h"