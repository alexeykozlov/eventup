//
//  DateAndTimeViewController.h
//  BMbook
//
//  Created by iApps Mac Mini on 9/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DateAndTimeViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *updateButton;

-(IBAction)updateButtonPressed:(id)sender;

@end
