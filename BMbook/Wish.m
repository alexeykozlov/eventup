//
//  Wish.m
//  BMbook
//
//  Created by Shoshi V. on 5/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "Wish.h"

@implementation Wish
- (id)initWithDictioanry:(NSDictionary *)dictionary
{
    self = [super init];
    if (self)
    {
        self.nID = [dictionary[@"id"] intValue];
        self.strAuthorName = dictionary[@"authorName"];
        self.strText = dictionary[@"text"];
        self.urlImage = [NSURL URLWithString:dictionary[@"img"]];
        self.urlSmallImage = [NSURL URLWithString: dictionary[@"thumb"]];
    }
    return self;
}
+ (Wish *)wishWithDictionary:(NSDictionary *)dictionary
{
    // Create a wishe using the initialize with dictionary method.
    Wish *newWish= [[Wish alloc] initWithDictioanry:dictionary];
    
    return newWish;
}
+ (NSArray *)wishesFromArray:(NSArray *)array
{
    // Create the array wishes
    NSMutableArray *arrWishes = [NSMutableArray arrayWithCapacity:array.count];
    
    // For each of the dictionary wishe create an object and add it to the array.
    for (NSDictionary *dicWish in array)
    {
        Wish *event = [Wish wishWithDictionary:dicWish];
        [arrWishes  addObject:event];
    }
    
    return arrWishes;
}

@end
