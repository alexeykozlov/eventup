//
//  PickerManager.m
//  IDareU
//
//  Created by iApps Dev. Mac mini on 3/7/13.
//  Copyright (c) 2013 Roee Kattaby. All rights reserved.
//

#import "PickerManager.h"

@implementation PickerManager

static PickerManager* sharedInstance;

+ (PickerManager*)getInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[PickerManager alloc] init];
    });
    
    return sharedInstance;
}

- (void)createDatePickerWithTimeForTextField:(PickerTextField*)textField
                           forViewController:(id<PickerTextFieldDelegate>)viewController
{
    // Create a date picker
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    [datePicker setDatePickerMode:UIDatePickerModeDateAndTime];
    [textField insertDatePickerIntoTextField:datePicker withActionDelegate:viewController];
}

- (void)createPickerForTextField:(PickerTextField *)textField
                           ofTag:(NSInteger)tag
                  fromDictionary:(NSDictionary*)dictionary
               forViewController:(id<PickerTextFieldDelegate>)viewController
{
    textField.keysDict = dictionary;
    textField.optionsArray = [textField.keysDict allKeys];
    NSMutableArray *mutableOptions = [NSMutableArray arrayWithObject:@"Make up your own..."];
    [mutableOptions addObjectsFromArray:textField.optionsArray];
    textField.optionsArray = mutableOptions;
    
    // Create a picker
    UIPickerView *picker = [self createPickerViewWithTag:tag andDelegate:textField];
    
    // Insert the picker the textfield
    [textField insertPickerIntoTextField:picker withActionDelegate:viewController];
}

- (void)createPickerForTextField:(PickerTextField*)textField
                           ofTag:(NSInteger)tag
                   withPListName:(NSString*)fileName
               forViewController:(id<PickerTextFieldDelegate>)viewController;
{
    // Get our options from our plist array
    textField.optionsArray = [[NSArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:fileName ofType:@"plist"]];
    
    // Create a picker
    UIPickerView *picker = [self createPickerViewWithTag:tag andDelegate:textField];
    
    // Insert the picker the textfield
    [textField insertPickerIntoTextField:picker withActionDelegate:viewController];
}

- (UIPickerView*)createPickerViewWithTag:(NSInteger)tag andDelegate:(id<UIPickerViewDelegate, UIPickerViewDataSource>)pickerDelegate
{
    // Create a picker view
    UIPickerView *picker = [[UIPickerView alloc] init];
    
    // Set selection indicator
    [picker setShowsSelectionIndicator:YES];
    
    // Set it's tag, datasource and delegate
    [picker setTag:tag];
    [picker setDataSource:pickerDelegate];
    [picker setDelegate:pickerDelegate];
    
    // Return the PickerView
    return picker;
}

@end
