//
//  OneWorldLocationVC.m
//  BMbook
//
//  Created by Shoshi V. on 5/20/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "OneWorldLocationVC.h"

@interface OneWorldLocationVC ()
{
   
}
@end

@implementation OneWorldLocationVC

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Add a progress indicator on the image view.
    self.indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.indicator setHidesWhenStopped:YES];
    [self.indicator startAnimating];
    [self.indicator setCenter:self.imageView.center];
    [self.view insertSubview:self.indicator belowSubview:self.imageView];
    
    
    if (self.worldLocation.urlImage)
    {
       // self.view.userInteractionEnabled = FALSE;
        NSURL *imageURL = self.worldLocation.urlImage;
        __weak NSURLRequest *imageRequest = [NSURLRequest requestWithURL:imageURL];
        
        [self.imageView setImageWithURLRequest:imageRequest
                                 placeholderImage:nil
                                          success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                            
                                              // Stop the activity indicator and remove it from the view.
                                              [self.indicator stopAnimating];
                                              [self.indicator removeFromSuperview];
                                               self.imageView.image = image;
                                             // Transition the change if image.
                                              [UIView transitionWithView:self.imageView 
                                                                duration:0.5
                                                                 options:UIViewAnimationOptionTransitionCrossDissolve
                                                              animations:^{
                                                                  if (self.imageView)
                                                                  {
                                                                      self.imageView.image = image;
                                                                  }
                                              
                                                              } completion:^(BOOL finished) {
                                                                  self.view.userInteractionEnabled = TRUE;
                                                              }];
                                              
                                          } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                              NSLog(@"One world location: Failed to download image!"
                                                    @"\nError: %@", error.localizedDescription);
                                               
                                              // Stop the activity indicator and remove it from the view.
                                              [self.indicator stopAnimating];
                                              [self.indicator removeFromSuperview];
                                              
                                              self.view.userInteractionEnabled = TRUE;
                                              //show alert
                                              UIAlertView *alert =[[UIAlertView alloc] initWithTitle:nil message:@"ישנה תקלה בטעינת התמונה,\nאנא נסה מאוחר יותר" delegate:nil cancelButtonTitle:@"סגור" otherButtonTitles: nil];
                                              [alert show];
                                          }];
        
        
    }
    self.title = NSLocalizedString(@"יונתן בעולם", nil);
    self.lblTitle.text = self.worldLocation.strTitle;
    self.textView.text = self.worldLocation.strDesc;
    //set leftBarButtonItem
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    btn.frame = CGRectMake(0, 0, 34, 31);
    [btn addTarget:self action:@selector(popController) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
}
- (void)popController
{
  
}

- (void)viewDidUnload
{
    if (self.indicator)
    {
        [self.indicator stopAnimating];
        [self.indicator removeFromSuperview];
    }
    [self setWorldLocation:nil];
    [self setIndicator:nil];
    [self setImageView:nil];
    [self setTextView:nil];
    [self setLblTitle:nil];
    [super viewDidUnload];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnClosePressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
