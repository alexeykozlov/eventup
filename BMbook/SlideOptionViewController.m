//
//  SlideOptionViewController.m
//  BiBi
//
//  Created by Shoshi V. on 2/21/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "SlideOptionViewController.h"

@interface SlideOptionViewController ()

@end

@implementation SlideOptionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:[UIImage imageNamed:@"nav_bar_sidemenu_btn.png"] forState:UIControlStateNormal];
    btn.frame = CGRectMake(0, 0, 30, 23);
    [btn addTarget:self action:@selector(btnShowMainMenuPressed:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)btnShowMainMenuPressed:(id)sender
{
    [self.delegate showMainMenu:sender];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
- (NSString *)returnStringOfStatusEvent
{
//    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSTimeInterval epoch = [[[NSUserDefaults standardUserDefaults]stringForKey:kAPIKeyDateEvent] doubleValue];
    NSDate *dateEvent =[NSDate dateWithTimeIntervalSince1970:epoch];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit fromDate:[NSDate date] toDate:dateEvent options:0];
    int minutes = (int)components.minute;
    int hours = (int)components.hour;
    int days=(int)components.day;
    NSString * strminutes =NSLocalizedString(@"דקות", nil);
    NSString * strDays =NSLocalizedString(@"ימים", nil);
    NSString * strHours =NSLocalizedString(@"שעות", nil);
    return [NSString stringWithFormat:@"%d %@, %d %@, %d %@",days,strDays,hours,strHours,minutes,strminutes];
}
- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState) state
                      error:(NSError *)error
{
    switch (state) {
        case FBSessionStateOpen:
            if (!error) {
                // Save our session to NSUserdefaults
            }
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            [FBSession.activeSession closeAndClearTokenInformation];
            break;
        default:
            break;
    }
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:kFBSessionStateChangedNotification
     object:session];
    
    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Error"
                                  message:error.localizedDescription
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

/*
 * Opens a Facebook session and optionally shows the login UX.
 */
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI
{
    return [FBSession openActiveSessionWithReadPermissions:nil
                                              allowLoginUI:allowLoginUI
                                         completionHandler:^(FBSession *session,
                                                             FBSessionState state,
                                                             NSError *error) {
                                             [self sessionStateChanged:session
                                                                 state:state
                                                                 error:error];
                                         }];
}


@end
