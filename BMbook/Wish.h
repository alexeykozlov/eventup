//
//  Wish.h
//  BMbook
//
//  Created by Shoshi V. on 5/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Wish : NSObject
@property (nonatomic ,assign) int nID;
@property (nonatomic ,strong) NSURL *urlImage;
@property (nonatomic ,strong) NSString * strAuthorName;
@property (nonatomic ,strong) NSString * strText;
@property (strong ,nonatomic)  NSURL *urlSmallImage;
+ (NSArray *)wishesFromArray:(NSArray *)array;
+ (Wish *)wishWithDictionary:(NSDictionary *)dictionary;
- (id)initWithDictioanry:(NSDictionary *)dictionary;
@end
