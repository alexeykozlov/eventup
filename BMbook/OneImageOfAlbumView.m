//
//  OneImageOfAlbumView.m
//  BMbook
//
//  Created by Shoshi V. on 5/19/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "OneImageOfAlbumView.h"

@implementation OneImageOfAlbumView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.delegate selectOneImageWithIndex:self.tag];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
