//
//  GiveBlessingViewController.m
//  BMbook
//
//  Created by Shoshi V. on 5/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "GiveBlessingViewController.h"
#import "AppDelegate.h"

@interface GiveBlessingViewController ()
{
     NSData *myPhoto;
    UIImagePickerController * _imgPicker;
    CameraView *_cameraView;
    int selectedOption;
}
@end

@implementation GiveBlessingViewController
#define kPlaceHolderTextView    NSLocalizedString(@"כתבו ברכה",nil)



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{

}

- (void)designView
{
    if (IS_IPHONE_5)
    {

        self.imageVPhoto.frame =CGRectMake(0,21, 320,320);
        self.viewMiddle.frame = CGRectMake(0, 341, self.viewMiddle.frame.size.width, self.viewMiddle.frame.size.height);
        self.imageTextView.frame = CGRectMake(6, 387, 306, 111);
        self.textView.frame = CGRectMake(3, 391, 315, 105);
    }
    else
    {
         self.imageVPhoto.frame =CGRectMake(19,21, 280,280);
         self.viewMiddle.frame = CGRectMake(0, 292, self.viewMiddle.frame.size.width, self.viewMiddle.frame.size.height);
         self.imageTextView.frame = CGRectMake(5, 335, 311, 77);
        self.textView.frame = CGRectMake(3, 329, 314, 82);
    }
}
- (void)viewDidUnload
{

}
-(void)createInputAccessoryViewForTextView
{
    UIView * inputAccView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 40.0)];
    [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
    [inputAccView setAlpha: 0.8];
    UIButton *btnCancel  = [UIButton buttonWithType: UIButtonTypeCustom];
    [btnCancel setBackgroundImage:[UIImage imageNamed:@"SEND 149x85"] forState:UIControlStateNormal];
    [btnCancel setFrame: CGRectMake(0.0, 0.0, 80.0, 40.0)];
    [btnCancel setTitleEdgeInsets:UIEdgeInsetsMake(-2, 4, 0, 0)];
    // Title.
    [btnCancel setTitle:NSLocalizedString(@"סיים", nil) forState: UIControlStateNormal];
    [btnCancel addTarget: self action: @selector(removeTextView) forControlEvents: UIControlEventTouchUpInside];
    [inputAccView addSubview:btnCancel];
    [self.textView setInputAccessoryView:inputAccView];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self designView];
    [self.delegate allowInteractiveSlideing:TRUE];
	self.title = NSLocalizedString(@"שלח/י ברכה", nil);
    
    //set inputAccessoryView to text view
    [self createInputAccessoryViewForTextView];
    
    //set contentSize to scroll
    self.scrollView.contentSize = CGSizeMake(320, self.view.frame.size.height +self.textView.frame.size.height +40);

      self.scrollView.scrollEnabled= FALSE;
    
    _imgPicker = [[UIImagePickerController alloc] init];
    _imgPicker.delegate = self;
    _imgPicker.editing = YES;
    _imgPicker.allowsEditing = YES;
    
    //sets the date color
    self.lblDate.textColor=[UIColor colorWithRed:(255/255.0) green:(220/255.0) blue:(90/255.0) alpha:1] ;
    //indicates that the user didnt select a picture
    self.bPicDidChange=FALSE;
    
}

-(void)viewDidAppear:(BOOL)animated
{
    //set date
    self.lblDate.text =[self returnStringOfStatusEvent];
    self.screenName=@"Send blessing screen";
    
    
}


- (void)setPlaceHolderToTextView
{
    self.textView.text = kPlaceHolderTextView;
    [self.textView setTextColor:[UIColor lightGrayColor]];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)removeTextView
{
    [self.textView resignFirstResponder];
}
#pragma mark - textField metods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.scrollView.scrollEnabled= TRUE;
    [self.scrollView setContentOffset:CGPointMake(0,self.viewMiddle.center.y +textField.center.y - 100) animated:YES];
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.scrollView.scrollEnabled= FALSE;
    [self.scrollView setContentOffset:CGPointMake(0,0) animated:YES];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return TRUE;
}
#pragma mark - textView metods
- (void)textViewDidChange:(UITextView *)textView
{
    if (textView.text.length>0)
    {
          textView.textAlignment = NSTextAlignmentRight;
    }
    else
    {
        textView.textAlignment = NSTextAlignmentCenter;
    }
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    textView.textAlignment = NSTextAlignmentCenter;
    self.scrollView.scrollEnabled= TRUE;
    if ([textView.text isEqualToString:kPlaceHolderTextView])
    {
        textView.text =@"";
        textView.textAlignment = NSTextAlignmentCenter; 
    }
    else
    {
        textView.textAlignment = NSTextAlignmentRight;
    }
    [self.textView setTextColor:[UIColor blackColor]];
      [self.scrollView setContentOffset:CGPointMake(0,self.textView.center.y - self.textView.frame.size.height+20) animated:YES];

}
- (void)textViewDidEndEditing:(UITextView *)textView
{
     textView.textAlignment = NSTextAlignmentRight;
    self.scrollView.scrollEnabled= FALSE;
    if ([textView.text isEqualToString:kPlaceHolderTextView]||
        textView.text.length ==0)
    {
        [self setPlaceHolderToTextView];
    }
     [self.scrollView setContentOffset:CGPointMake(0,0) animated:YES];
}

#pragma mark - interface builder
- (void)checkValid
{
    if (self.bPicDidChange==FALSE)
    {
        UIAlertView *alert =[[UIAlertView alloc] initWithTitle:nil message:@"נא לבחור תמונה" delegate:nil cancelButtonTitle:@"סגור" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
    if (!self.textField.text.length)
    {
        UIAlertView *alert =[[UIAlertView alloc] initWithTitle:nil message:@"נא להזין את שם המברך" delegate:nil cancelButtonTitle:@"סגור" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        if (self.textView.text.length >500)
        {
            UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"שגיאה" message:@"הברכה יכולה להכיל רק 500 תוים" delegate:nil cancelButtonTitle:@"סגור" otherButtonTitles: nil];
            [alert show];
        }
        else
        {
            if (!self.textView.text.length||
                [self.textView.text isEqualToString:kPlaceHolderTextView])
                
            {
                UIAlertView *alert =[[UIAlertView alloc] initWithTitle:nil message:@"נא להזין ברכה" delegate:nil cancelButtonTitle:@"סגור" otherButtonTitles: nil];
                [alert show];
            }
            
else
{
            [self addWish];
}
        }
    }
    }
}

-(IBAction)returned:(UIStoryboardSegue *)segue
{
    //fixes the highlited row in the menu
    AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
    SlideViewController* slideController = appDelegate.window.rootViewController;
    slideController.mainVC._selectedRow=1;
    [slideController.mainVC.tableView reloadData];
    [segue.sourceViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)addWish
{
   MBProgressHUD * HUD = [[MBProgressHUD alloc] initWithWindow:[UIApplication sharedApplication].keyWindow];
    HUD.labelText = @"שולח ברכה...";
    [self.view.window addSubview:HUD];
    [HUD show:YES];

    NSData *imageData=UIImageJPEGRepresentation(self.imageVPhoto.image, 0.8);
    [EventManager addWishWithId:[[NSUserDefaults standardUserDefaults] stringForKey:kAPIKeyEventID].intValue andKeyword:[[NSUserDefaults standardUserDefaults] stringForKey:kAPIKeyKeyword].intValue andAuthorName:self.textField.text andText:self.textView.text andImage:imageData
        Success:^{
            [HUD hide:YES afterDelay:2];
            
            self.textView.text= @"";
            [self.textView resignFirstResponder];
            self.textField.text= @"";
            [self.textField resignFirstResponder];
            self.imageVPhoto.image = nil;
            
        }
                        failure:^{[HUD hide:YES];}];
}
- (IBAction)btnSendPressed
{
        [self checkValid];
}

- (IBAction)btnAddPicPressed:(id)sender
{

    NSString *actionSheetTitle = @"הוסף תמונה";
    NSString *camera = @"צלם";
    NSString *photo = @"בחר תמונה קיימת";
    NSString *cancelTitle = @"בטל";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:camera, photo,nil];
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    //Get the name of the current pressed button
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    if ([buttonTitle isEqualToString:@"צלם"]) {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:nil];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"The device doesn't have a camera" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
        }
    }
    if ([buttonTitle isEqualToString:@"בחר תמונה קיימת"]) {
        
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:nil];
    }
    if ([buttonTitle isEqualToString:@"בטל"]) {
        
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *) Picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}



- (void)imagePickerController:(UIImagePickerController *) Picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    self.imageVPhoto.image= [info objectForKey:UIImagePickerControllerOriginalImage];
    self.bPicDidChange=TRUE;
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark UIImagePickerController Delegate Methods

-(UIImage *)cropImage:(UIImage *)sourceImage cropRect:(CGRect)cropRect aspectFitBounds:(CGSize)finalImageSize fillColor:(UIColor *)fillColor {
    
    CGImageRef sourceImageRef = sourceImage.CGImage;
    
    //Since the crop rect is in UIImageOrientationUp we need to transform it to match the source image.
    CGAffineTransform rectTransform = [self transformSize:sourceImage.size orientation:sourceImage.imageOrientation];
    CGRect transformedRect = CGRectApplyAffineTransform(cropRect, rectTransform);
    
    //Now we get just the region of the source image that we are interested in.
    CGImageRef cropRectImage = CGImageCreateWithImageInRect(sourceImageRef, transformedRect);
    
    //Figure out which dimension fits within our final size and calculate the aspect correct rect that will fit in our new bounds
    CGFloat horizontalRatio = finalImageSize.width / CGImageGetWidth(cropRectImage);
    CGFloat verticalRatio = finalImageSize.height / CGImageGetHeight(cropRectImage);
    CGFloat ratio = MIN(horizontalRatio, verticalRatio); //Aspect Fit
    CGSize aspectFitSize = CGSizeMake(CGImageGetWidth(cropRectImage) * ratio, CGImageGetHeight(cropRectImage) * ratio);
    
    
    CGContextRef context = CGBitmapContextCreate(NULL,
                                                 finalImageSize.width,
                                                 finalImageSize.height,
                                                 CGImageGetBitsPerComponent(cropRectImage),
                                                 0,
                                                 CGImageGetColorSpace(cropRectImage),
                                                 CGImageGetBitmapInfo(cropRectImage));
    
    if (context == NULL) {
        NSLog(@"NULL CONTEXT!");
    }
    
    //Fill with our background color
    CGContextSetFillColorWithColor(context, fillColor.CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, finalImageSize.width, finalImageSize.height));
    
    //We need to rotate and transform the context based on the orientation of the source image.
    CGAffineTransform contextTransform = [self transformSize:finalImageSize orientation:sourceImage.imageOrientation];
    CGContextConcatCTM(context, contextTransform);
    
    //Give the context a hint that we want high quality during the scale
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    
    //Draw our image centered vertically and horizontally in our context.
    CGContextDrawImage(context, CGRectMake((finalImageSize.width-aspectFitSize.width)/2, (finalImageSize.height-aspectFitSize.height)/2, aspectFitSize.width, aspectFitSize.height), cropRectImage);
    
    //Start cleaning up..
    CGImageRelease(cropRectImage);
    
    CGImageRef finalImageRef = CGBitmapContextCreateImage(context);
    UIImage *finalImage = [UIImage imageWithCGImage:finalImageRef];
    
    CGContextRelease(context);
    CGImageRelease(finalImageRef);
    return finalImage;
}
//Creates a transform that will correctly rotate and translate for the passed orientation.
//Based on code from niftyBean.com
- (CGAffineTransform) transformSize:(CGSize)imageSize orientation:(UIImageOrientation)orientation {
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    switch (orientation) {
        case UIImageOrientationLeft: { // EXIF #8
            CGAffineTransform txTranslate = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            CGAffineTransform txCompound = CGAffineTransformRotate(txTranslate,M_PI_2);
            transform = txCompound;
            break;
        }
        case UIImageOrientationDown: { // EXIF #3
            CGAffineTransform txTranslate = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            CGAffineTransform txCompound = CGAffineTransformRotate(txTranslate,M_PI);
            transform = txCompound;
            break;
        }
        case UIImageOrientationRight: { // EXIF #6
            CGAffineTransform txTranslate = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            CGAffineTransform txCompound = CGAffineTransformRotate(txTranslate,-M_PI_2);
            transform = txCompound;
            break;
        }
        case UIImageOrientationUp: // EXIF #1 - do nothing
        default: // EXIF 2,4,5,7 - ignore
            break;
    }
    return transform;
    
}
- (void)savePhoto:(UIImage *)image
{
    self.imageVPhoto.image = image;
}
- (void)cancelCameraViewController:(CameraViewController *)viewController
{
    [viewController dismissViewControllerAnimated:YES completion:^{}];
}
@end
