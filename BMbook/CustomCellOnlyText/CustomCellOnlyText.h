//
//  CustomCellOnlyText.h
//  BiBi
//
//  Created by Shoshi V. on 2/26/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCellOnlyText : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *appNameImage;


@end
