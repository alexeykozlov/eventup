//
//  CreatorMainViewController.h
//  BMbook
//
//  Created by iApps Mac Mini on 9/10/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreatorMainViewController : UITableViewController <UIAlertViewDelegate>
{
    NSArray *_arrTitles;
    NSArray *_arrIcons;
    int _selectedRow;
}

@property (nonatomic,assign) id <ProtocolMainViewConroller> delegate;

@end
