//
//  JonatanInWorldVC.m
//  BMbook
//
//  Created by Shoshi V. on 5/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "JonatanInWorldVC.h"

@interface JonatanInWorldVC ()
{
   
}
@end

@implementation JonatanInWorldVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (BOOL)coordinateIsValid:(double)latitude long:(double)longitude
{
    return CLLocationCoordinate2DIsValid(CLLocationCoordinate2DMake(latitude,longitude));
    
}

- (void)viewDidUnload
{
    self.mpDisplayMap.delegate = nil;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.delegate allowInteractiveSlideing:FALSE];
	self.title = NSLocalizedString(@"יונתן בעולם", nil);
    
   //get  WorldLocations from userDefault
    NSArray * arrLocation = [[NSUserDefaults standardUserDefaults]objectForKey:kAPIKeyEventWorldLocations];
    arrLocation  =[WorldLocation worldLocationsFromArray:arrLocation ];
    CustomPlacemark * customPlacemark;
    
    self.arrAnontations =[[NSMutableArray alloc] init];
    for (WorldLocation *aWorldLocation in arrLocation)
    {
       if ([self coordinateIsValid:aWorldLocation.fLat long:aWorldLocation.fLon])
       {
        
            customPlacemark = [[CustomPlacemark alloc]initWithCoordinateLat:aWorldLocation.fLat  Long:aWorldLocation.fLon WordLocation:aWorldLocation];
            [self.mpDisplayMap addAnnotation:customPlacemark];
            
        }
       
    }
//    CustomPlacemark *customPlacemark1 =[[CustomPlacemark alloc]initWithCoordinateLat:34.5958201   Long:-120.13764809999998  WordLocation:nil];
//    [self.mpDisplayMap addAnnotation:customPlacemark1];
    //init mapView
	[self.mpDisplayMap setMapType:MKMapTypeStandard];
	[self.mpDisplayMap setDelegate:self];
    
    // zoom to map

        CLLocationCoordinate2D zoomLocation = CLLocationCoordinate2DMake(37.368247,-122.029266);
        MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation,400000,400000);
        MKCoordinateRegion adjustedRegion = [self.mpDisplayMap regionThatFits:viewRegion];
        [self.mpDisplayMap setRegion:adjustedRegion animated:YES];
        
        
    

}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kShowOnwWorldLocationSegue])
    {
        OneWorldLocationVC *oneWorldLocationVC = segue.destinationViewController;
        oneWorldLocationVC.worldLocation = self.selectedWorldLocation;
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - MKMapView delegate functions
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change  context:(void *)context{
    
    NSString *action = (__bridge NSString*)context;
    NSLog(@"selected");
    if([action isEqualToString:@"MAP_ANNOTATION_SELECTED"]){
        // do something
        
    }
}

//- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views {
//    
//    for (MKAnnotationView *anAnnotationView in views) {
//        
//        [anAnnotationView addObserver:self
//                           forKeyPath:@"selected"
//                              options:NSKeyValueObservingOptionNew
//                              context:@"MAP_ANNOTATION_SELECTED"];
//    }
//    
//}
- (void)showOneWordLocation:(CustomButton *)btn
{
    self.selectedWorldLocation = ((CustomButton *)btn).worldLocation;
    [self performSegueWithIdentifier:kShowOnwWorldLocationSegue sender:nil];
}
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    
    self.selectedWorldLocation = ((CustomButton *)((CustomAnnotationView *)view).btn).worldLocation;
    [self performSegueWithIdentifier:kShowOnwWorldLocationSegue sender:nil];
}

- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>) annotation
{
    CustomAnnotationView *customAnnotationView =
    (CustomAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomAnnotationView"];
    if (customAnnotationView == nil)
    {
        customAnnotationView = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomAnnotationView"];
[self.arrAnontations addObject:customAnnotationView];
    }
//            [customAnnotationView addObserver:self
//                                        forKeyPath:@"selected"
//                                           options:NSKeyValueObservingOptionNew
//                                           context:@"MAP_ANNOTATION_SELECTED"];
    customAnnotationView.multipleTouchEnabled= TRUE;
    [customAnnotationView setBounds:CGRectMake(0, 0, 70, 68)];
    customAnnotationView.annotation = annotation;
    ((CustomButton *)customAnnotationView.btn).worldLocation = ((CustomPlacemark*)annotation).worldLocation;
     [customAnnotationView.btn addTarget:self action:@selector(showOneWordLocation:) forControlEvents:UIControlEventTouchUpInside];
    [customAnnotationView.imgUser.layer setCornerRadius:7];
    [customAnnotationView.imgUser.layer setMasksToBounds:YES];
    [customAnnotationView setRightCalloutAccessoryView:customAnnotationView.imgUser];
    customAnnotationView.enabled = YES;
    customAnnotationView.canShowCallout = NO;
    return customAnnotationView;
    
    
//    // Try to dequeue an existing pin view first.
//    CustomAnnotationView* pinView = (CustomAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"PinAnnotationView"];
//    if (!pinView)
//    {
//        // If an existing pin view was not available, create one.
//        pinView = [[CustomAnnotationView alloc] initWithAnnotation:annotation
//                                                   reuseIdentifier:@"PinAnnotationView"];
//    }
//    pinView.pinColor = MKPinAnnotationColorRed;
//    pinView.animatesDrop = YES;
//    if ([[[InfoManager sharedManager]connectedUserType]isEqualToString:kAPIUserTypeWorker])
//    {
//        pinView.job = [((CustomPlacemark *)annotation) job];
//    }
//    else
//    {
//        pinView.worker = [((CustomPlacemark *)annotation) worker];
//    }
//    
//    return pinView;
//
//    MKAnnotationView *pinView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"PinAnnotationView"];
//    if (pinView)
//    {
//        pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation
//                                               reuseIdentifier:@"PinAnnotationView"];
//    }
//return pinView;
}

@end
