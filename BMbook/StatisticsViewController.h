//
//  StatisticsViewController.h
//  BMbook
//
//  Created by iApps Mac Mini on 9/10/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "SlideOptionViewController.h"

@interface StatisticsViewController : SlideOptionViewController
@property (strong, nonatomic) IBOutlet UILabel *enteriesLabel;
@property (strong, nonatomic) IBOutlet UILabel *wishesLabel;
@property (strong, nonatomic) IBOutlet UILabel *labelClock;

@end
