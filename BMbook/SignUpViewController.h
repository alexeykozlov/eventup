//
//  SignUpViewController.h
//  EventUp
//
//  Created by Alexey on 10/29/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "GAITrackedViewController.h"
#import <MessageUI/MessageUI.h>
#import <StoreKit/StoreKit.h>

@interface SignUpViewController : GAITrackedViewController <UITextFieldDelegate,MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UITextField *textFieldName;
@property (strong, nonatomic) IBOutlet UITextField *textFieldPhone;
@property (strong, nonatomic) IBOutlet UIScrollView *scroll;

@property (strong, nonatomic) IBOutlet UILabel *labelSignedUp;



-(IBAction) SignUp: (id)sender;

@end
