//
//  CustomPlacemark.m
//  BMbook
//
//  Created by Shoshi V. on 5/20/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "CustomPlacemark.h"

@implementation CustomPlacemark
@synthesize coordinate,strSubtitle,strTitle,worldLocation;
-(NSString *)subtitle
{
	return (self.strSubtitle);
}
-(NSString *)title
{
	return (self.strTitle);
}
-(id)initWithCoordinate:(CLLocationCoordinate2D) c{
	self.coordinate=c;
	NSLog(@"%f,%f",c.latitude,c.longitude);
	return self;
}

-(id)initWithCoordinateLat:(double)dLat Long:(double)dLong WordLocation:(WorldLocation *)newWordLocation
{
   	
	coordinate.latitude = dLat;
	coordinate.longitude = dLong;
	worldLocation = newWordLocation;
	return self;
}
-(id)initWithCoordinateLat:(double)dLat Long:(double)dLong Title:(NSString*)title Subtitle:(NSString*)subtitle WordLocation:(WorldLocation *)newWordLocation
{
	
	coordinate.latitude = dLat;
	coordinate.longitude = dLong;
	strTitle = title;
	strSubtitle = subtitle;
	worldLocation = newWordLocation;
	return self;
}

@end
