//
//  MABarButtonItem.h
//  MatchApp
//
//  Created by Itamar Biton on 11/29/12.
//  Copyright (c) 2012 iApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MABarButtonItem : UIBarButtonItem

@property (strong, nonatomic) UIButton *customButton;

- (id)initWithTitle:(NSString *)title andBackgroundImage:(UIImage *)backgroundImage;
- (id)initWithTitle:(NSString *)title andBackgroundImage:(UIImage *)backgroundImage target:(id)target action:(SEL)action;

@end
