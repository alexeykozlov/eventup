//
//  OnePage.m
//  InstructionsFramework
//
//  Created by Shoshi V. on 6/11/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "OnePage.h"

@implementation OnePage

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)init
{
	if (self=[super init])
    {
        //init image view
        self.backgroundColor =[UIColor clearColor];
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(kLocationXImageView,kLocationYImageView,kWidthImageView, kHeightImageView)];
        self.imageView.backgroundColor = [UIColor clearColor];
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;

        
        //init text view
        self.textView = [[UITextView alloc] initWithFrame:CGRectMake(kLocationXTextView, kLocationYTextView,kWidthTextView, kHeightTextView)];
        self.textView.userInteractionEnabled = FALSE;
        self.textView.textAlignment = UITextAlignmentCenter;
        self.textView.font =kFontTextView;
        self.textView.editable = FALSE;
        self.textView.textColor = [UIColor blackColor];
        self.textView.backgroundColor =[UIColor clearColor];
        
        //init lable
        self.lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(kLocationXLable, kLocationYLable,kWidthLable, kHeightLable)];
        self.lblTitle.textAlignment = UITextAlignmentCenter;
        self.lblTitle.font =kFontLable;
        self.lblTitle.font = kFontLable;
        self.lblTitle.textColor = [UIColor blackColor];
        self.lblTitle.backgroundColor =[UIColor clearColor];
        self.lblTitle.numberOfLines = kNumberOfLineInLable;
        
        
        //add imageView and textView to page
        [self addSubview:self.imageView];
        [self addSubview:self.lblTitle];
        [self addSubview:self.textView];
        
	}
    return self;
}
@end
