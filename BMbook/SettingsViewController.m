//
//  SettingsViewController.m
//  BMbook
//
//  Created by iApps Mac Mini on 9/10/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "SettingsViewController.h"
#import "UserDetailsViewController.h"
#import "CellarixVC.h"

static NSString *const cellarixSignUpUrl = @"https://www.cellarix.com/Free/Mobile/Reg.aspx";

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=@"הגדרות";
    self.name.placeholder=@"שם אירוע";
    self.name.text=[[NSUserDefaults standardUserDefaults]stringForKey:kAPIKeyEventName];
    self.name.delegate=self;
    self.name.textAlignment=NSTextAlignmentCenter;
    self.adress.placeholder=@"כתובת אירוע";
    self.adress.text=[[NSUserDefaults standardUserDefaults] stringForKey:kAdress];
    self.adress.delegate=self;
    self.adress.textAlignment=NSTextAlignmentCenter;
    NSTimeInterval epoch = [[[NSUserDefaults standardUserDefaults]stringForKey:kAPIKeyDateEvent] doubleValue];
    self.date=[NSDate dateWithTimeIntervalSince1970:epoch];
    NSDateFormatter* formatterDate = [[NSDateFormatter alloc] init];
    [formatterDate setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    NSString* strDate = [formatterDate stringFromDate:self.date];
    self.dateAndTime.placeholder=@"תאריך ושעה";
    self.dateAndTime.text=strDate;
    self.dateAndTime.delegate=self;
    self.dateAndTime.textAlignment=NSTextAlignmentCenter;
	[[PickerManager getInstance]createDatePickerWithTimeForTextField:self.dateAndTime forViewController:self];
    [self.allowGiftsSwitch setOn:[[NSUserDefaults standardUserDefaults]stringForKey:kAllowGifts].boolValue];
    //sets the tap recognizer
    UITapGestureRecognizer * recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    recognizer.delegate = self;
    [self.view addGestureRecognizer:recognizer];
    
    self.song.placeholder=@"הזן לינק ל- YouTube";
    
    self.song.text=[[NSUserDefaults standardUserDefaults]stringForKey:kAPIKeySong];
    self.song.textAlignment=NSTextAlignmentCenter;
    self.song.delegate=self;
    
    //changes the switch color
    self.allowGiftsSwitch.onTintColor = [UIColor colorWithRed:(255/255.0) green:(220/255.0) blue:(90/255.0) alpha:1];
    self.allowGiftsSwitch.tintColor= [UIColor colorWithRed:(255/255.0) green:(220/255.0) blue:(90/255.0) alpha:1];
    
    UIImage* infoIconImage = [UIImage imageNamed:@"Shape-105-copy.png"];
    CGRect frameimg = CGRectMake(0, 0, infoIconImage.size.width, infoIconImage.size.height);
    UIButton *infoButton = [[UIButton alloc] initWithFrame:frameimg];
    [infoButton setBackgroundImage:infoIconImage forState:UIControlStateNormal];
    [infoButton addTarget:self action:@selector(infoButtonWasPressed
                                                )
         forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *infoBarButton =[[UIBarButtonItem alloc] initWithCustomView:infoButton];
    self.navigationItem.rightBarButtonItem=infoBarButton;
    
    if([[NSUserDefaults standardUserDefaults]boolForKey:kNotFirstCreatorEnter]==NO)
    {
        [self.navigationController.navigationBar setHidden:YES];
        NSArray *arrImages = @[@"HTU_8",@"HTU_9",@"HTU_10",@"HTU_6"];
        NSArray *arrTexts = @[@"הזינו את פרטי האירוע, תוכלו לערוך את הפרטים בכל עת",@"פתחו חשבון בחברת Cellarix ותחסכו זמן יקר בטיפול בצ'קים",@"צרפו את הזמנת החתונה כדי שתוכלו לשתף אותה עם האורחים",@"המסך מציג את הזמן שנותר עד החתונה, מספר הצפיות באירוע ומספר הברכות שהתקבלו"];
        NSArray *arrTitels = @[@"מתחתנים? מזל טוב!",@"המתנות שלכם במקום אחד",@"הזמנה לאירוע",@"סטטיסטיקה"];
        
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:kNotFirstCreatorEnter];
        
        self.instructionsView =[[[NSBundle mainBundle] loadNibNamed:@"InstructionsView" owner:nil options:nil]objectAtIndex:0];
        self.instructionsView.delegate =self;
        [self.instructionsView configureWithFrameDarkOverlayView:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) andNumPages:4 andArrayTitels:arrTitels andArrayImages:arrImages andArrayTexts:arrTexts];
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setBackgroundColor:[UIColor clearColor]];
        btn.titleLabel.font = [UIFont systemFontOfSize:12];
        [btn setTitleColor:[UIColor colorWithRed:(255/255.0) green:(220/255.0) blue:(90/255.0) alpha:1] forState:UIControlStateNormal];
        
        [btn setTitle:@"אישור" forState:UIControlStateNormal];
        btn.frame = CGRectMake(0, 0, 80, 40);
        [btn setBackgroundImage:[UIImage imageNamed:@"button_small"] forState:UIControlStateNormal];
        [btn addTarget:self.instructionsView action:@selector(didClickedInstructionsContinueButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.instructionsView setButton:btn];
        
        //add instructions View
        [self.instructionsView showOnView:self.view];
    }
    
    self.instructionsViewIsOpen = NO;
}

//- (IBAction)clickedInstruction:(id)sender
//{
//    [self.navigationController.navigationBar setHidden:NO];
//    [self.instructionsView didClickedInstructionsContinueButton:nil];
//}

- (void) infoButtonWasPressed
{
    //[self.navigationController.navigationBar setHidden:YES];
    if(self.instructionsViewIsOpen == NO)
    {
    self.instructionsViewIsOpen = YES;
    NSArray *arrImages = @[@"HTU_8",@"HTU_9",@"HTU_10",@"HTU_6"];
    NSArray *arrTexts = @[@"הזינו את פרטי האירוע, תוכלו לערוך את הפרטים בכל עת",@"פתחו חשבון בחברת Cellarix ותחסכו זמן יקר בטיפול בצ'קים",@"צרפו את הזמנת החתונה כדי שתוכלו לשתף אותה עם האורחים",@"המסך מציג את הזמן שנותר עד החתונה, מספר הצפיות באירוע ומספר הברכות שהתקבלו"];
    NSArray *arrTitels = @[@"מתחתנים? מזל טוב!",@"המתנות שלכם במקום אחד",@"הזמנה לאירוע",@"סטטיסטיקה"];
    
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:kNotFirstCreatorEnter];
    
    self.instructionsView =[[[NSBundle mainBundle] loadNibNamed:@"InstructionsView" owner:nil options:nil]objectAtIndex:0];
    self.instructionsView.delegate =self;
    [self.instructionsView configureWithFrameDarkOverlayView:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) andNumPages:4 andArrayTitels:arrTitels andArrayImages:arrImages andArrayTexts:arrTexts];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundColor:[UIColor clearColor]];
    btn.titleLabel.font = [UIFont systemFontOfSize:12];
    [btn setTitleColor:[UIColor colorWithRed:(255/255.0) green:(220/255.0) blue:(90/255.0) alpha:1] forState:UIControlStateNormal];
    
    [btn setTitle:@"אישור" forState:UIControlStateNormal];
    btn.frame = CGRectMake(0, 0, 80, 40);
    [btn setBackgroundImage:[UIImage imageNamed:@"button_small"] forState:UIControlStateNormal];
    [btn addTarget:self.instructionsView action:@selector(didClickedInstructionsContinueButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.instructionsView setButton:btn];
    
    //add instructions View
        [self.instructionsView showOnView:self.view];}
    
}

-(void)viewDidAppear:(BOOL)animated
{
    self.screenName=@"Settings screen";
    [self.navigationController.navigationBar setHidden:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setName:nil];
    [self setAdress:nil];
    [self setUserDetailsButton:nil];
    [self setAllowGiftsSwitch:nil];
    [self setPaymentsButton:nil];
    [self setInvitationButton:nil];
    [self setDateAndTime:nil];
    [self setDateAndTime:nil];
    [super viewDidUnload];
}

#pragma mark - Button actions
-(IBAction) userDetailsButtonPressed: (id)sender
{
   [self performSegueWithIdentifier:@"detailsVCSegue" sender:nil];
}

-(IBAction) paymentsButtonPressed: (id)sender
{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"חשוב מאוד"
                                                      message:@"בעת ההרשמה יש לוודא כי מזינים מספר טלפון זהה למספר הטלפון איתו נוצר האירוע"
                                                     delegate:self
                                            cancelButtonTitle:nil
                                            otherButtonTitles:@"הבנתי", nil];
    [message show];
}

//sets the buttons of the alert
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"הבנתי"])
        [self performSegueWithIdentifier:@"cellarixSignUP" sender:cellarixSignUpUrl];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"cellarixSignUP"])
    {
        CellarixVC* cellarixVC = segue.destinationViewController;
        cellarixVC.url=sender;
    }
}

-(IBAction) updatePressed: (id) sender{
    if(self.name.text.length==0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"שגיאה" message:@"יש להזין שם אירוע" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }else if (self.dateAndTime.text.length==0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"שגיאה" message:@"יש לבחור תאריך" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }else if(self.adress.text.length==0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"שגיאה" message:@"יש להזין כתובת" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    } else
    {
        //displays hud
        MBProgressHUD * HUD = [[MBProgressHUD alloc] initWithWindow:[UIApplication sharedApplication].keyWindow];
        HUD.labelText = @"מעדכן...";
        [self.view.window addSubview:HUD];
        [HUD show:YES];
        [GeneralManager updateEventDetailsWithSessionId:[[NSUserDefaults standardUserDefaults]stringForKey:KSessionId] andName:self.name.text andDate:[self.date timeIntervalSince1970] andAdress:self.adress.text andAllowGifts:self.allowGiftsSwitch.on andEmail:[[NSUserDefaults standardUserDefaults]stringForKey:kUserName] andPassword:@"" andCode:[[NSUserDefaults standardUserDefaults]stringForKey:kAPIKeyKeyword].intValue andSong:self.song.text
                                                Success:^{
                                                    [HUD hide:YES];
                                                    self.name.text=[[NSUserDefaults standardUserDefaults]stringForKey:kAPIKeyEventName];
                                                    self.adress.text=[[NSUserDefaults standardUserDefaults] stringForKey:kAdress];
                                                    
                                                    NSDate *date=self.date;
                                                    NSDateFormatter* formatterDate = [[NSDateFormatter alloc] init];
                                                    [formatterDate setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
                                                    NSString* strDate = [formatterDate stringFromDate:date];
                                                    self.dateAndTime.text=strDate;
                                                    [self.allowGiftsSwitch setOn:[[NSUserDefaults standardUserDefaults]stringForKey:kAllowGifts].boolValue];
                                                    self.song.text=[[NSUserDefaults standardUserDefaults]stringForKey:kAPIKeySong];
                                                    //succes alert
                                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"פרטים עודכנו בהצלחה" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                                                    [alert show];
                                                }
                                                failure:^{
                                                    [HUD hide:YES];
                                                }];
    }
}

#pragma mark - Image picker methods
-(IBAction) invitationButtonPressed: (id) sender{
    NSString *actionSheetTitle = @"העלה הזמנה"; 
    NSString *camera = @"צלם";
    NSString *photo = @"בחר תמונה קיימת";
    NSString *cancelTitle = @"בטל";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:camera, photo,nil];
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    //Get the name of the current pressed button
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    if ([buttonTitle isEqualToString:@"צלם"]) {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:nil];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"The device doesn't have a camera" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
        }
    }
    if ([buttonTitle isEqualToString:@"בחר תמונה קיימת"]) {
        
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:nil];
    }
    if ([buttonTitle isEqualToString:@"בטל"]) {
        
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *) Picker {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}



- (void)imagePickerController:(UIImagePickerController *) Picker

didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    self.selectedImage= [info objectForKey:UIImagePickerControllerOriginalImage];
    if(self.selectedImage!=nil)
    {
        //displays hud
//        MBProgressHUD * HUD = [[MBProgressHUD alloc] initWithView:self.view];
//        HUD.labelText = @"מעדכן...";
//        [self.view.window addSubview:HUD];
//        [HUD show:YES];
        
       [MBProgressHUD showHUDAddedTo:self.view animated:YES];
       
        NSData *imageData=UIImageJPEGRepresentation(self.selectedImage, 0.8);
        [EventManager addInvitationWithId:[[NSUserDefaults standardUserDefaults] stringForKey:kAPIKeyEventID].intValue andKeyword:[[NSUserDefaults standardUserDefaults] stringForKey:kAPIKeyKeyword].intValue andImage:imageData
                                  Success:^(){
                                      [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                      //save the invitation
                                      [EventManager getInvitationWithId:[[NSUserDefaults standardUserDefaults] stringForKey:kAPIKeyEventID].intValue
                                                                Success:^(NSURL* imageUrl)
                                       {
                                           //[HUD hide:YES];
                                           [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                           NSString *urlString = [imageUrl absoluteString];
                                           [[NSUserDefaults standardUserDefaults]setObject:urlString forKey:@"invitationUrl"];
                                       }
                                                                failure:^()
                                       {
                                           [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                            //[HUD hide:YES];
                                       }];
                                  }
                                  failure:^(){}];
    }
    //[[Picker parentViewController] dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}


//- (void)imagePickerControllerDidCancel:(UIImagePickerController *) Picker {
//    
//    [self dismissModalViewControllerAnimated:YES];
//    
//}
//
//- (void)imagePickerController:(UIImagePickerController *) Picker
//
//didFinishPickingMediaWithInfo:(NSDictionary *)info {
//    
//    self.selectedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
//    
//    
//    [self dismissModalViewControllerAnimated:YES];
//}

#pragma mark - Date and time picker delegate
-(void)didSelectDatePickerOption:(PickerTextField *)chosenTextField chosenDate:(NSDate *)date
{
    // Getting the Hour
    NSDateFormatter* formatterDate = [[NSDateFormatter alloc] init];
    //the date format will be hours|minutes
    [formatterDate setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    NSString* strDate = [formatterDate stringFromDate:date];
    //convirting the string to NSDate
    //self.date = [[NSDate alloc] init];
    date = [formatterDate dateFromString:strDate];
    self.date=date;
    // Show in picker
    self.dateAndTime.text =strDate;
    [chosenTextField resignFirstResponder];
}
-(void)didCancelPicker:(PickerTextField*)chosenTextField
{
    //if pressing cancel on time picker cancel all notification
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    //close the picker
    [chosenTextField resignFirstResponder];
    //clear the text in the textField
    [self.dateAndTime setText:@""];
}

#pragma mark - text field methods
-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    [textField resignFirstResponder];
    return NO; // We do not want UITextField to insert line-breaks.
}

#pragma mark - gesture methods
- (void)handleTap:(UITapGestureRecognizer *)recognizer {
    if ([self.name isFirstResponder])
        [self.name resignFirstResponder];
    else if([self.adress isFirstResponder])
        [self.adress resignFirstResponder];
    else if([self.dateAndTime isFirstResponder])
        [self.dateAndTime resignFirstResponder];
    else if([self.song isFirstResponder])
        [self.song resignFirstResponder];
}

#pragma InstuctionsViewDelegate Metods

- (void)dismissInstructionsView
{
    //remove instructions View
    [self.instructionsView hide];
    self.instructionsViewIsOpen = NO;
}

@end
