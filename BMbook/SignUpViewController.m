//
//  SignUpViewController.m
//  EventUp
//
//  Created by Alexey on 10/29/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "SignUpViewController.h"

#define customerEmail "Info.EventUp@gmail.com"

@interface SignUpViewController ()

@end

@implementation SignUpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.title=@"הרשמה";
    //sets the tap recognizer
    UITapGestureRecognizer * recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    recognizer.delegate = self;
    [self.view addGestureRecognizer:recognizer];
    //customizes the back button
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 25.0f, 27.0f)];
    [backButton setImage:[UIImage imageNamed:@"nav_bar_back_btn"]  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(popVC) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    //sets the text fields
    self.textFieldName.textAlignment=NSTextAlignmentRight;
    self.textFieldName.placeholder=@"שם";
    self.textFieldName.delegate=self;
    self.textFieldPhone.textAlignment=NSTextAlignmentRight;
    self.textFieldPhone.placeholder=@"מס' טלפון";
    self.textFieldPhone.delegate=self;
    self.textFieldPhone.keyboardType = UIKeyboardTypeNumberPad;
    
    //sets number keyboar toolbar
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    [numberToolbar setBackgroundImage:[UIImage imageNamed:@"nav_bar"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
    UIBarButtonItem* cancel= [[UIBarButtonItem alloc]initWithTitle:@"בטל" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)];
    [cancel setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor],  UITextAttributeTextColor,nil] forState:UIControlStateNormal];
    cancel.tintColor=[UIColor whiteColor];
    UIBarButtonItem* done= [[UIBarButtonItem alloc]initWithTitle:@"אשר" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)];
    [done setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor],  UITextAttributeTextColor,nil] forState:UIControlStateNormal];
    done.tintColor=[UIColor whiteColor];
    numberToolbar.items = [NSArray arrayWithObjects:
                           cancel,
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           done,
                           nil];
    [numberToolbar sizeToFit];
    self.textFieldPhone.inputAccessoryView = numberToolbar;
}

-(void)viewWillAppear:(BOOL)animated
{
    self.screenName=@"Sign up screen screen";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//dissmisses the vc
- (void) popVC{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - gesture methods
-(void)handleTap:(UITapGestureRecognizer *)recognizer {
    if([self.textFieldName isFirstResponder])
        [self.textFieldName resignFirstResponder];
    else if([self.textFieldPhone isFirstResponder])
        [self.textFieldPhone resignFirstResponder];
}

#pragma mark - text field methods
-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    [textField resignFirstResponder];
    return NO; // We do not want UITextField to insert line-breaks.
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    //sets the scroll view
    [self.scroll setScrollEnabled:YES];
    [self.scroll setContentSize:CGSizeMake(320,720)];
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    if (screenSize.height <= 480.0f)
    {
        [self.scroll setContentOffset:CGPointMake(0,205) animated:YES];
    }
    else
    {
        [self.scroll setContentOffset:CGPointMake(0,100) animated:YES];
        
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.scroll setContentOffset:CGPointMake(0,0) animated:YES];
    [self.scroll setScrollEnabled:NO];
}

#pragma mark - signUp
-(IBAction) SignUp: (id)sender
{
    if(self.textFieldName.text.length==0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"שגיאה" message:@"שדה זה הינו שדה חובה" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else if(self.textFieldPhone.text.length<9)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"שגיאה" message:@"מספר טלפון לא חוקי" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else
    {
        if ([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc]init];
            [mailComposer setSubject:NSLocalizedString(@"EventUp", nil)];
            
            NSArray *toRecipents = [NSArray arrayWithObject:@customerEmail];
            
            // Fill out the email body text
            NSString *text = @"היי, ברצוני להירשם ל-EventUp, אנא צור איתי קשר";
            NSString *name = @"שם: ";
            NSString *number=@"טלפון: ";

            NSString *emailBody = [NSString stringWithFormat:@"%@<br/>%@%@<br/>%@%@<br/>", text,name,self.textFieldName.text,number,self.textFieldPhone.text];
            [mailComposer setMessageBody:emailBody isHTML:YES];
            [mailComposer setMailComposeDelegate:self];
            [mailComposer setToRecipients:toRecipents];
            [self presentViewController:mailComposer animated:YES completion:nil];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"תקלה", nil)  message:NSLocalizedString(@"לא ניתן לשלוח מייל,אנא בדוק אם יש חשבון דואר מוגדר במכשיר.",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"סגור" ,nil)otherButtonTitles:nil];
            [alert show];
        }
    }
}

#pragma mark MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
    if(result== MFMailComposeResultSent)
    {
        [self dismissViewControllerAnimated:YES completion:^{[self.navigationController popViewControllerAnimated:YES];}];
    }
            
           
            
    
    
#if __IPHONE_OS_VERSION_MIN_REQUIRED < 60000
	[self dismissModalViewControllerAnimated:YES];
#else
    [self dismissViewControllerAnimated:YES completion:NULL];
#endif
}

#pragma mark - toolbar
-(void)cancelNumberPad{
    [self.textFieldPhone resignFirstResponder];
    self.textFieldPhone.text =@"";
}

-(void)doneWithNumberPad{
    NSString *numberFromTheKeyboard = self.textFieldPhone.text;
    [self.textFieldPhone resignFirstResponder];
}



@end
