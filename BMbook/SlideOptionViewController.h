//
//  SlideOptionViewController.h
//  BiBi
//
//  Created by Shoshi V. on 2/21/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Protocols.h"
#import <FacebookSDK/FacebookSDK.h>
#import "GAITrackedViewController.h"

@interface SlideOptionViewController : GAITrackedViewController


@property (nonatomic,assign) id <ProtocolSlide> delegate;
- (void)btnShowMainMenuPressed:(id)sender;
- (NSString *)returnStringOfStatusEvent;
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;
@end
