//
//  GiveBlessingViewController.h
//  BMbook
//
//  Created by Shoshi V. on 5/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideOptionViewController.h"
#import "CameraView.h"
#import "CameraViewController.h"
#import "MBProgressHUD.h"
#import "SoundHandler.h"
#import "GAITrackedViewController.h"
@interface GiveBlessingViewController : SlideOptionViewController<UITextViewDelegate ,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CameraViewControllerDelegate,UIActionSheetDelegate>
{
    UIImagePickerController *picker;
}


@property (nonatomic ,weak)IBOutlet UIScrollView *scrollView;
@property (nonatomic ,weak)IBOutlet UITextField *textField;
@property (nonatomic ,weak)IBOutlet UITextView *textView;
@property (nonatomic ,weak)IBOutlet UILabel *lblDate;
@property (nonatomic ,weak)IBOutlet UIButton *btnAddPhoto;
@property (nonatomic ,weak)IBOutlet UIImageView *imageVPhoto;
@property (nonatomic ,weak)IBOutlet UIView *viewMiddle;
@property (nonatomic ,weak)IBOutlet UIImageView *imageTextView;
@property BOOL bPicDidChange;


- (IBAction)btnAddPicPressed:(id)sender;
- (IBAction)btnSendPressed;
-(IBAction)returned:(UIStoryboardSegue *)segue;

@end
