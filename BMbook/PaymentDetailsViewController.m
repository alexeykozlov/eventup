//
//  PaymentDetailsViewController.m
//  BMbook
//
//  Created by iApps Mac Mini on 9/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "PaymentDetailsViewController.h"

@interface PaymentDetailsViewController ()

@end

@implementation PaymentDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=@"הגדרת חשבון";
	// Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    self.screenName=@"Users cellarix account screen";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
