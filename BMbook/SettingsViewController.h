//
//  SettingsViewController.h
//  BMbook
//
//  Created by iApps Mac Mini on 9/10/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "SlideOptionViewController.h"
#import "PickerManager.h"
#import "InstructionsView.h"

@interface SettingsViewController : SlideOptionViewController <UIImagePickerControllerDelegate,PickerTextFieldDelegate,UIActionSheetDelegate,UITextFieldDelegate,InstructionsViewDelegate>
{
    UIImagePickerController *picker;
    
}

@property (nonatomic ,strong) InstructionsView *instructionsView;

@property (strong, nonatomic) IBOutlet UITextField *song;
@property (strong, nonatomic) IBOutlet UITextField *name;
@property (strong, nonatomic) IBOutlet UITextField *adress;
@property (strong, nonatomic) IBOutlet PickerTextField *dateAndTime;
@property (nonatomic, assign) BOOL instructionsViewIsOpen;

@property (strong, nonatomic) IBOutlet UIButton *userDetailsButton;
@property (strong, nonatomic) IBOutlet UISwitch *allowGiftsSwitch;
@property (strong, nonatomic) IBOutlet UIButton *paymentsButton;
@property (strong, nonatomic) IBOutlet UIButton *invitationButton;

@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSString *strTime;

@property (retain, nonatomic) UIImage *selectedImage;

@property int unixTime;


-(IBAction) userDetailsButtonPressed: (id)sender;
-(IBAction) paymentsButtonPressed: (id)sender;
-(IBAction) invitationButtonPressed: (id) sender;
-(IBAction) updatePressed: (id) sender;
- (void) infoButtonWasPressed;

@end
