//
//  CameraViewController.h
//  BMbook
//
//  Created by Shoshi V. on 5/19/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "resizeImage.h"
@class CameraViewController;
@protocol CameraViewControllerDelegate <NSObject>

- (void)savePhoto:(UIImage *)image;
- (void)cancelCameraViewController:(CameraViewController *)viewController;
@end
@interface CameraViewController : UIViewController


@property (weak ,nonatomic) IBOutlet UIView *viewBottom;
@property (strong ,nonatomic)  UIImageView * imageViewPhoto;
@property (strong ,nonatomic)  UIImageView * imageViewBackground;
@property (weak, nonatomic) IBOutlet UIView *cropBounds;
@property (assign,nonatomic) id<CameraViewControllerDelegate> delegate;

//action for pan gesture
- (IBAction)btnCancelPressed;
- (IBAction)handlePan:(UIPanGestureRecognizer *)recognizer;
- (IBAction)cropImage:(id)sender;
@end
