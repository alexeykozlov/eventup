//
//  ViewController.h
//  BMbook
//
//  Created by Shoshi V. on 5/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShakingAlertView.h"
#import "ASIHTTPRequest.h"
#import "GAITrackedViewController.h"
#import "UIImageView+AFNetworking.h"
#import "SlideOptionViewController.h"
#import "Protocols.h"
#import "InstructionsView.h"

@interface FirstViewController : GAITrackedViewController <UITableViewDataSource ,UITableViewDelegate,UISearchBarDelegate, UISearchDisplayDelegate,InstructionsViewDelegate>

@property (nonatomic ,strong) InstructionsView *instructionsView;

@property (nonatomic ,strong) NSArray *arrAllEvents;
@property (nonatomic ,strong) NSMutableArray *arrayNamesOfEvents;
@property (nonatomic ,strong) NSMutableArray *arrayPasswordsOfEvents;
@property (nonatomic ,strong) NSMutableArray *filteredArrayNamesOfEvents;
@property (nonatomic ,strong) NSMutableArray *filteredArrayPasswordsOfEvents;
@property (nonatomic ,strong) NSString *strNameOfselectedEvent;
@property (nonatomic ,strong) NSString *strPasswordOfselectedEvent;
@property (nonatomic ,weak)IBOutlet UIImageView *imageVBackGroung;
@property (strong, nonatomic) IBOutlet UITableView *TableView;
@property (nonatomic ,assign) id <ProtocolSlide> delegate;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong,nonatomic) NSMutableArray *filteredEvents;
@property (strong, nonatomic) IBOutlet UIButton *eventButton;
@property (strong, nonatomic) IBOutlet UILabel *eventLabel;
@property (strong, nonatomic) IBOutlet UILabel *labelChooseEvent;


- (IBAction)newEvent:(id)sender;
@end
