//
//  Event.h
//  BMbook
//
//  Created by Shoshi V. on 5/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Event : NSObject
@property (nonatomic ,strong) NSString *strEmail;
@property (nonatomic ,strong) NSString *strPassword;
@property (nonatomic ,strong) NSString *strAdress;
@property (nonatomic ,assign) BOOL bAllowGifts;




@property (nonatomic ,strong) NSString *strName;
@property (nonatomic ,strong) NSString *strCode;
@property (nonatomic ,strong) NSArray *arrWishes;
@property (nonatomic ,assign) int nID;
@property (nonatomic ,assign) int dateEvent;
@property (nonatomic ,strong) NSString *nPhone;
@property (nonatomic ,strong) NSString *song;

- (id)initWithDictioanry:(NSDictionary *)dictionary;
+ (Event *)eventWithDictionary:(NSDictionary *)dictionary;
+ (NSArray *)eventsFromArray:(NSArray *)array;
@end
