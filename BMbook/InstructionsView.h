//
//  InstuctionsView.h
//  InstructionsFramework
//
//  Created by Shoshi V. on 6/11/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalValuesInstrucions.h"
#import "OnePage.h"
#import "SMPageControl.h"

@protocol InstructionsViewDelegate <NSObject>

- (void)dismissInstructionsView;

@end
@interface InstructionsView : UIView <UIScrollViewDelegate>

////////////////////////////////
//Properties

//IBOutlet
@property (nonatomic ,weak) IBOutlet UIImageView *imageVBackGround;
@property (nonatomic ,weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic ,weak) IBOutlet SMPageControl *pageControlInstructions;
@property (nonatomic ,weak) IBOutlet UIView *darkOverlayView;
@property (nonatomic ,weak) IBOutlet UIView *viewOfScroll;
@property (nonatomic ,weak) IBOutlet UIView *viewButtons;
@property (nonatomic ,weak) IBOutlet UIImageView *imageVBackgroundViewButtons;

@property (nonatomic ,strong) UIButton *btnLetsGo;
@property (nonatomic ,strong) NSArray *arrImages;
@property (nonatomic ,strong) NSArray *arrTitels;
@property (nonatomic ,strong) NSArray *arrTexts;
@property (nonatomic ,strong) NSMutableArray *arrPages;
@property (nonatomic ,assign) id<InstructionsViewDelegate> delegate;

////////////////////////////////
//Metods
- (void)configureWithFrameDarkOverlayView:(CGRect)frame andNumPages:(int)numPages andArrayTitels:(NSArray *)arrTitels andArrayImages:(NSArray *)arrImages andArrayTexts:(NSArray *)arrTexts;
- (void)showOnView:(UIView *)view;
- (void)hide;
- (void)setButtons:(NSArray *)arrButtons;
- (void)setButton:(UIButton *)oneButton;
- (void)setButtonsToFirstLine:(NSArray *)arrButtons1 andButtonsToNextLine:(NSArray *)arrButtons2;
- (IBAction)didClickedInstructionsContinueButton:(id)sender;
@end
