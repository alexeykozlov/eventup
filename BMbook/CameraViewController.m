//
//  CameraViewController.m
//  BMbook
//
//  Created by Shoshi V. on 5/19/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#include <QuartzCore/QuartzCore.h>
#import "CameraViewController.h"

@interface CameraViewController ()
{
    UIButton * _btnCancel;
    UIButton *_btnShare;
    float height;
      UIImage *imageToResize;
}
- (void)btnCancelPressed;
- (void)btnSharePressed;
- (UIImage *)screenShot;
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo;
@end

@implementation CameraViewController
@synthesize  imageViewPhoto;
@synthesize  imageViewBackground;
//@synthesize  imageViewRoof;
//@synthesize  imageViewCell;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self = [super initWithNibName:@"CameraViewController" bundle:[NSBundle mainBundle] ];
        
        if (IS_IPHONE_5)
        {
            height = kHeightIphone5;
        }
        else
        {
             height = kHeightIphone4 ;
        }
        //init imageViewBackground
        self.imageViewBackground = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320,height)];
        [self.imageViewBackground setBackgroundColor:[UIColor clearColor]];
        self.imageViewBackground.contentMode = UIViewContentModeScaleAspectFit;
        
        //init imageViewPhoto
        self.imageViewPhoto = [[UIImageView alloc] init];
        self.imageViewPhoto.contentMode = UIViewContentModeScaleAspectFit;
        [self.imageViewPhoto setBackgroundColor:[UIColor clearColor]];
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.imageViewPhoto.frame = CGRectMake(0,0, 320,height);
    self.cropBounds.frame =CGRectMake(0,40, 320,320);
    self.imageViewBackground.userInteractionEnabled =FALSE;
    [self.view addSubview:self.imageViewPhoto];
    [self.view addSubview:self.imageViewBackground];
    [self.view bringSubviewToFront:self.cropBounds];
    [self.view bringSubviewToFront:self.viewBottom];
    self.viewBottom.frame = CGRectMake(0, height-20 - self.viewBottom.frame.size.height, 320, self.viewBottom.frame.size.height);
   UIImage *imagePhoto= [self screenShot];
    self.imageViewPhoto.image = imagePhoto;
    self.imageViewBackground.hidden = TRUE;
    [self.imageViewBackground setFrame:CGRectMake(self.imageViewBackground.frame.origin.x,
                                                  -54, 320,height)];
    [self.imageViewPhoto setFrame:CGRectMake(self.imageViewPhoto.frame.origin.x,
                                            -54, 320, height)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnCancelPressed
{
//    [self dismissModalViewControllerAnimated:YES];
    [self.delegate cancelCameraViewController:self];
}
- (UIImage *)screenShot
{
    [_btnCancel setHidden:TRUE];
    [_btnShare setHidden:TRUE];
    [self.cropBounds setHidden:TRUE];
    [self.viewBottom setHidden:TRUE];
    UIGraphicsBeginImageContext(self.view.frame.size);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [_btnCancel setHidden:FALSE];
    [_btnShare setHidden:FALSE];
    [self.cropBounds setHidden:FALSE];
    [self.viewBottom setHidden:FALSE];

    
    return viewImage;
    
}
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    UIAlertView *alert;
    
    // Unable to save the image
    if (error)
        alert = [[UIAlertView alloc] initWithTitle:@"שגיאה"
                                           message:@"לא ניתן לשמור את התמונה ספריית התמונות"
                                          delegate:self cancelButtonTitle:@"אישור"
                                 otherButtonTitles:nil];
    else // All is well
        alert = [[UIAlertView alloc] initWithTitle:nil
                                           message:@"התמונה נשמרה בהצלחה"
                                          delegate:self cancelButtonTitle:@"אישור"
                                 otherButtonTitles:nil];
    [alert show];
    
}

//Pan gesture recognizer action
- (IBAction)handlePan:(UIPanGestureRecognizer *)recognizer {
    
    switch (recognizer.state) {
            
        case UIGestureRecognizerStateChanged: {
            
            CGPoint translation = [recognizer translationInView:self.view];
            recognizer.view.center = CGPointMake(recognizer.view.center.x, recognizer.view.center.y + translation.y);
            [recognizer setTranslation:CGPointMake(0, 0) inView:self.view];
            
            float topEdgePosition = CGRectGetMinY(self.cropBounds.frame);
            float bottomEdgePosition = CGRectGetMaxY(self.cropBounds.frame);
            if (topEdgePosition<0)
            {
                self.cropBounds.frame = CGRectMake(self.cropBounds.frame.origin.x,0, self.cropBounds.frame.size.width, self.cropBounds.frame.size.height);
            }
            else if (bottomEdgePosition > self.viewBottom.frame.origin.y)
            {
                self.cropBounds.frame = CGRectMake(self.cropBounds.frame.origin.x,self.viewBottom.frame.origin.y-self.cropBounds.frame.size.height , self.cropBounds.frame.size.width, self.cropBounds.frame.size.height);
            }
          }
            
        default:
            
            break;
            
    }
    
    
}

//crop image button pressed
- (IBAction)cropImage:(id)sender
{
    
    [self cropImageMethod];
    
}

- (void) cropImageMethod
{
    //get top corner coordinate of crop frame
    float topEdgePosition = CGRectGetMinY(self.cropBounds.frame);
    if (!IS_IPHONE_5)
    {
        topEdgePosition +=44;
    }
    
    
    float rightEdgePosition = CGRectGetMinX(self.cropBounds.frame);
    
    //create UIIMage instance to hold cropped result
    UIImage *croppedImage;
    
    //crop image to selected bounds
    CGRect croppedRect;
    croppedRect = CGRectMake(rightEdgePosition, topEdgePosition, 320, 320);
    CGImageRef tmp = CGImageCreateWithImageInRect([self.imageViewPhoto.image CGImage], croppedRect);
    croppedImage = [UIImage imageWithCGImage:tmp];
    CGImageRelease(tmp);
    
    //convert cropped image into NSData object
    NSData *CroppedImageData = UIImageJPEGRepresentation(croppedImage, 0.7);
    NSData *imageData = UIImageJPEGRepresentation(self.imageViewPhoto.image, 0.7);
    
    //get number of bytes in NSData
    NSInteger imageToCropDataSize = imageData.length;
    
    NSLog(@"original size %d Bytes", imageToCropDataSize);
    
    imageToResize = [UIImage imageWithData:CroppedImageData];
    [self resizeImage];
}


//resizeImage for storage
- (void) resizeImage
{
    
    //call resize image class
    resizeImage *imageResize = [[resizeImage alloc]init];
    [imageResize resizeImage:imageToResize width:320 height:320];
    NSData *resizedImageData = [imageResize thumbnailImageData];
    
    NSInteger resizedImageDataSize = resizedImageData.length;
    
    self.imageViewPhoto.image = [UIImage imageWithData:resizedImageData];
    
    [self.delegate savePhoto:[UIImage imageWithData:resizedImageData]];
    NSLog(@"resized size %d Bytes", resizedImageDataSize);
    [self dismissModalViewControllerAnimated:YES];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}



@end
