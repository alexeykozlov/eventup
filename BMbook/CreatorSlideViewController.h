//
//  CreatorSlideViewController.h
//  BMbook
//
//  Created by iApps Mac Mini on 9/10/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "CHSlideController.h"
#import "SettingsViewController.h"
#import "CreatorMainViewController.h"

@interface CreatorSlideViewController : CHSlideController <ProtocolMainViewConroller, ProtocolSlide>

@property (nonatomic,strong) SettingsViewController *settingsVC;
@property (nonatomic ,strong) UINavigationController *nav;
@property (nonatomic, strong) CreatorMainViewController * creatorMainVC;

@end
