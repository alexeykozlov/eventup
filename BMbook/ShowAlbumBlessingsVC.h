//
//  ShowAlbumBlessingsVC.h
//  BMbook
//
//  Created by Shoshi V. on 5/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideOptionViewController.h"
#import "OneImageOfAlbumView.h"
#import "ASIHTTPRequest.h"
#import "ShowBigPhotosViewController.h"
#import "UIScrollView+SVPullToRefresh.h"
#import "ManagerService.h"
@interface ShowAlbumBlessingsVC : SlideOptionViewController <ProtocolOneImageOnAlbum ,UITableViewDataSource,UITableViewDelegate>

@property (nonatomic ,strong) NSArray *arrWishes;
@property (nonatomic ,strong) NSMutableArray *arrPhoto;
@property (nonatomic ,assign) BOOL isPush;
@property (nonatomic ,weak) IBOutlet UITableView *tableViewAlbum;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;


@end
