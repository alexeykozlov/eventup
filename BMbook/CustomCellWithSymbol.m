//
//  CustomCellWithSymbol.m
//  BiBi
//
//  Created by Shoshi V. on 2/27/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "CustomCellWithSymbol.h"

@implementation CustomCellWithSymbol

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
