//
//  ViewController.m
//  BMbook
//
//  Created by Shoshi V. on 5/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "FirstViewController.h"

@interface FirstViewController ()

@end

@implementation FirstViewController

- (void)viewWillDisappear:(BOOL)animated
{
    //show  navigationBar
      self.navigationController.navigationBarHidden = FALSE;
}
- (void)viewWillAppear:(BOOL)animated
{
     NSLog(@"[[[NSUserDefaults standardUserDefaults] objectForKey:kAPIKeyEventID] intValue])%d",[[[NSUserDefaults standardUserDefaults] objectForKey:kAPIKeyEventID] intValue]);
    //hidden  navigationBar
    self.navigationController.navigationBarHidden = TRUE;
    [self getEvent];

}
- (void)viewDidAppear:(BOOL)animated
{
    self.screenName = @"Entery screen";
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    //sets search bar cancel button color
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
        [[UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor grayColor],UITextAttributeTextColor,[NSValue valueWithUIOffset:UIOffsetMake(0, 1)],UITextAttributeTextShadowOffset,nil] forState:UIControlStateNormal];
    }
    
    if ([self.TableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.TableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.searchDisplayController.searchResultsTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.searchDisplayController.searchResultsTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    //sets the user/creator boolean to user
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kIsCreator];
    //tells if menu should be open
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kLogIn];
    // Initialize the filtered eventArray with a capacity equal to the eventsArray's capacity
    self.filteredEvents = [NSMutableArray arrayWithCapacity:[self.arrAllEvents count]];
    self.searchBar.placeholder=@"חפשו שם אירוע";
    
    //sets the table size according to device/ios
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            if (screenSize.height > 480.0f) {
                /*Do iPhone 5 stuff here.*/
                self.TableView.frame = CGRectMake(0,350,320,308);
                self.searchBar.frame= CGRectMake(0,306,320,44);
                self.labelChooseEvent.frame=CGRectMake(103, 275, 120, 30);
            } else {
                /*Do iPhone Classic stuff here.*/
                self.TableView.frame = CGRectMake(0,305,320,176);
                self.searchBar.frame= CGRectMake(0,261,320,44);
                self.labelChooseEvent.frame=CGRectMake(103, 225, 120, 30);
            }
        }
    }
    else{
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            if (screenSize.height > 480.0f) {
                /*Do iPhone 5 stuff here.*/
                self.TableView.frame = CGRectMake(0,350,320,220);
                self.searchBar.frame= CGRectMake(0,306,320,44);
                self.labelChooseEvent.frame=CGRectMake(103, 275, 120, 30);
            } else {
                /*Do iPhone Classic stuff here.*/
                self.TableView.frame = CGRectMake(0,306,320,176);
                self.searchBar.frame= CGRectMake(0,262,320,44);
                self.labelChooseEvent.frame=CGRectMake(103, 225, 120, 30);
            }
        }
    }
    
//    if([[NSUserDefaults standardUserDefaults]boolForKey:kNotFirstEnter]==NO)
//    {
//        NSArray *arrImages = @[@"HTU_1",@"HTU_2",@"HTU_3",@"HTU_4",@"HTU_5",@"HTU_7"];
//        NSArray *arrTexts = @[@"הוזמנת לחתונה? מזל טוב! מהיום כל המידע שתצטרכו נמצא במקום אחד",@"בחרו את האירוע שהוזמנתם אליו מתוך כל בעלי השמחה",@"הוסיפו ברכה ייחודית ותמונה מקורית שבעלי השמחה והאורחים יוכלו להתרגש ממנה",@"צפו בהזמנה לאירוע ונווטו ליעד בעזרת האפליקציה",@"תוכלו לשלוח מתנה לזוג המאושר בקלות ובנוחות",@"אהבתם את שיר החתונה? תוכלו להאזין לו שוב בדרך הביתה"];
//        NSArray *arrTitels = @[@"ברוכים הבאים",@"בחרו אירוע",@"הוסיפו ברכה",@"נווטו לאירוע",@"שלח מתנה",@"שיר החתונה"];
//        
//        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:kNotFirstEnter];
//        
//        self.instructionsView =[[[NSBundle mainBundle] loadNibNamed:@"InstructionsView" owner:nil options:nil]objectAtIndex:0];
//        self.instructionsView.delegate =self;
//        [self.instructionsView configureWithFrameDarkOverlayView:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) andNumPages:kNumPages andArrayTitels:arrTitels andArrayImages:arrImages andArrayTexts:arrTexts];
//        
//        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [btn setBackgroundColor:[UIColor clearColor]];
//        btn.titleLabel.font = [UIFont systemFontOfSize:12];
//        [btn setTitleColor:[UIColor colorWithRed:(255/255.0) green:(220/255.0) blue:(90/255.0) alpha:1] forState:UIControlStateNormal];
//        
//        [btn setTitle:@"סגור" forState:UIControlStateNormal];
//        btn.frame = CGRectMake(0, 0, 80, 40);
//        [btn setBackgroundImage:[UIImage imageNamed:@"button_small"] forState:UIControlStateNormal];
//        [btn addTarget:self.instructionsView action:@selector(didClickedInstructionsContinueButton:) forControlEvents:UIControlEventTouchUpInside];
//        [self.instructionsView setButton:btn];
//        
//        //add instructions View
//        [self.instructionsView showOnView:self.view];
//    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getEvent
{
    //get events from the server
    ManagerService *managerService =[[ManagerService alloc] init];
    [managerService getAllEventsSuccess:^(NSArray *arrEvents)
    {
        self.arrAllEvents = arrEvents;
        //init array of name Events and array of Passwords Events
        self.arrayNamesOfEvents =[[NSMutableArray alloc] init];
        self.arrayPasswordsOfEvents = [[NSMutableArray alloc] init];
        for (Event *aEvent in arrEvents)
        {
            [self.arrayNamesOfEvents addObject:aEvent.strName];
            [self.arrayPasswordsOfEvents addObject:aEvent.strCode];
        }
        [self.TableView reloadData];
        
    } failure:^{
        
    }];
}

#pragma mark - interface builder metods


- (IBAction)newEvent:(id)sender
{
    [self performSegueWithIdentifier:@"newEventSegue" sender:nil];
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [super viewDidUnload];
}

#pragma mark - TableView methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Check to see whether the normal table or search results table is being displayed and return the count from the appropriate array
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [self.filteredEvents count];
    } else {
        return [self.arrAllEvents count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    EventTableCell *cell = [self.TableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //cell.textLabel.text=self.arrayNamesOfEvents[indexPath.row];
    
    
    // Check to see whether the normal table or search results table is being displayed and set the event object from the appropriate array
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        Event *event = self.filteredEvents[indexPath.row];
        cell.labelName.text = event.strName;
        cell.labelName.textAlignment=NSTextAlignmentRight;
        NSTimeInterval epoch =event.dateEvent;
        NSDate *date=[NSDate dateWithTimeIntervalSince1970:epoch];
        NSDateFormatter* formatterDate = [[NSDateFormatter alloc] init];
        [formatterDate setDateFormat:@"dd-MM-yyyy"];
        NSString* strDate = [formatterDate stringFromDate:date];
        cell.labelDate.text=strDate;
        cell.labelDate.textAlignment=NSTextAlignmentLeft;
    } else {
        Event *event = self.arrAllEvents[indexPath.row];
        
        cell.labelName.text = event.strName;
        cell.labelName.textAlignment=NSTextAlignmentRight;
        NSTimeInterval epoch =event.dateEvent;
        NSDate *date=[NSDate dateWithTimeIntervalSince1970:epoch];
        NSDateFormatter* formatterDate = [[NSDateFormatter alloc] init];
        [formatterDate setDateFormat:@"dd-MM-yyyy"];
        NSString* strDate = [formatterDate stringFromDate:date];
        cell.labelDate.text=strDate;
        cell.labelDate.textAlignment=NSTextAlignmentLeft;
    }
    
    cell.textLabel.textAlignment=NSTextAlignmentRight;
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        Event *event = self.filteredEvents[indexPath.row];
        self.strNameOfselectedEvent = event.strName;
        self.strPasswordOfselectedEvent =event.strCode;
    } else {
        
        self.strNameOfselectedEvent = self.arrayNamesOfEvents[indexPath.row];
        self.strPasswordOfselectedEvent = self.arrayPasswordsOfEvents[indexPath.row];
    }
    if (self.strPasswordOfselectedEvent)
    {
        ShakingAlertView *shakingAlert = nil;
        shakingAlert = [[ShakingAlertView alloc] initWithAlertTitle:NSLocalizedString(@"נא הזן/י קוד לאירוע", nil) checkForPassword:self.strPasswordOfselectedEvent onCorrectPassword:^
                        {
                            //get selected event
                            Event *selectedEvent;
                            if (tableView == self.searchDisplayController.searchResultsTableView) {
                                int selectedRow = indexPath.row;
                                selectedEvent =self.filteredEvents[selectedRow];
                            } else {
                                int selectedRow = indexPath.row;
                                selectedEvent =self.arrAllEvents[selectedRow];
                            }
                            
                            
                            
                            //save in user default
                            [[NSUserDefaults standardUserDefaults]setObject:@(selectedEvent.dateEvent).stringValue forKey:kAPIKeyDateEvent];
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:selectedEvent.nID] forKey:kAPIKeyEventID];
                            [[NSUserDefaults standardUserDefaults]setObject:selectedEvent.strCode forKey:kAPIKeyKeyword];
                            [[NSUserDefaults standardUserDefaults]setObject:selectedEvent.strName forKey:kAPIKeyEventName];
                            [[NSUserDefaults standardUserDefaults]setObject:selectedEvent.strEmail forKey:kUserName];
                            [[NSUserDefaults standardUserDefaults]setObject:selectedEvent.strAdress forKey:kAdress];
                            [[NSUserDefaults standardUserDefaults]setObject:@(selectedEvent.bAllowGifts).stringValue forKey:kAllowGifts];
                            [[NSUserDefaults standardUserDefaults]setObject:selectedEvent.nPhone forKey:kAPIKeyPhone];
                            [[NSUserDefaults standardUserDefaults]setObject:selectedEvent.song forKey:kAPIKeySong];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            //update the number of enteries to the event
                            [statisticsManager addEntryToEventWithEventId:[[NSUserDefaults standardUserDefaults] stringForKey:kAPIKeyEventID].intValue andKeyword:[[NSUserDefaults standardUserDefaults] stringForKey:kAPIKeyKeyword].intValue
                                                            Success:^{}
                                                            failure:^{}];
                            
                            //pop first controller
                            [self.delegate allowInteractiveSlideing:TRUE];
                            [self.navigationController popViewControllerAnimated:YES];
                            
                        } onDismissalWithoutPassword:^
                        {
                            
                        }];
        
        //show alert
        [shakingAlert show];
        
    }

}

#pragma mark Content Filtering
-(void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope {
    // Update the filtered array based on the search text and scope.
    // Remove all objects from the filtered search array
    [self.filteredEvents removeAllObjects];
    // Filter the array using NSPredicate
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.strName contains[c] %@",searchText];
    self.filteredEvents = [NSMutableArray arrayWithArray:[self.arrAllEvents filteredArrayUsingPredicate:predicate]];
}

#pragma mark - UISearchDisplayController Delegate Methods
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    // Tells the table data source to reload when text changes
    [self filterContentForSearchText:searchString scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    // Return YES to cause the search result table view to be reloaded.
    
    //sets search table color
    [self.searchDisplayController.searchResultsTableView setBackgroundColor:[UIColor blackColor]];
    
    return YES;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    // Tells the table data source to reload when scope bar selection changes
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (void)searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller
{
    //[self.searchBar setFrame:CGRectMake(0, 0, 320, 44)];
    [self.searchBar setShowsCancelButton:YES animated:NO];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] <7){
        //customizes cancel button in ios6
        for (UIView *subView in self.searchBar.subviews){
            if([subView isKindOfClass:[UIButton class]]){
                [(UIButton*)subView setTitle:@"בטל" forState:UIControlStateNormal];
                
            }
        }
    }
    else
    {
        //customizes cancel button in ios6
        self.searchDisplayController.searchBar.showsCancelButton = YES;
        UIButton *cancelButton;
        UIView *topView = self.searchDisplayController.searchBar.subviews[0];
        for (UIView *subView in topView.subviews) {
            if ([subView isKindOfClass:NSClassFromString(@"UINavigationButton")]) {
                cancelButton = (UIButton*)subView;
            }
        }
        if (cancelButton) {
            //Set the new title of the cancel button
            [cancelButton setTitle:@"בטל" forState:UIControlStateNormal];
            
        }
    }
}

-(void)searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller
{
    //[self.scroll setContentOffset:CGPointZero animated:YES];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            if (screenSize.height > 480.0f) {
                /*Do iPhone 5 stuff here.*/
                self.searchBar.frame= CGRectMake(0,306,320,44);
            } else {
                /*Do iPhone Classic stuff here.*/
                self.searchBar.frame= CGRectMake(0,261,320,44);
            }
        }
    }
    else{
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            if (screenSize.height > 480.0f) {
                /*Do iPhone 5 stuff here.*/
               self.searchBar.frame= CGRectMake(0,306,320,44);
            } else {
                /*Do iPhone Classic stuff here.*/
                self.searchBar.frame= CGRectMake(0,262,320,44);
            }
        }
    }
    
    [self.eventButton setHidden:NO];
    [self.eventLabel setHidden:NO];
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    self.searchBar.frame =CGRectMake(0, 0, 320, 44);
    [self.eventButton setHidden:YES];
    [self.eventLabel setHidden:YES];
    
}

#pragma InstuctionsViewDelegate Metods

- (void)dismissInstructionsView
{
    //remove instructions View
    [self.instructionsView hide];
}



@end
