//
//  OneImageOfAlbumView.h
//  BMbook
//
//  Created by Shoshi V. on 5/19/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Protocols.h"
@interface OneImageOfAlbumView : UIView
@property (nonatomic,weak) IBOutlet UIImageView *imageBackGround;
@property (nonatomic,weak) IBOutlet UIImageView *imageAccesory;
@property (nonatomic,assign) id<ProtocolOneImageOnAlbum> delegate;
@end
