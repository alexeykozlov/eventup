//
//  StatisticsViewController.m
//  BMbook
//
//  Created by iApps Mac Mini on 9/10/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "StatisticsViewController.h"

@interface StatisticsViewController ()

@end

@implementation StatisticsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=@"סטטיסטיקה";
    //gets the number of enteries
    [statisticsManager getStatisticsWithEventId:[[NSUserDefaults standardUserDefaults]stringForKey:kAPIKeyEventID].intValue andKeyword:[[NSUserDefaults standardUserDefaults]stringForKey:kAPIKeyKeyword].intValue
                                        Success:^(int wishes,int enteries)
                                        {
                                            self.enteriesLabel.text=@(enteries).stringValue;
                                            self.wishesLabel.text=@(wishes).stringValue;
                                        }
                                        failure:^{}
     ];
}

-(void)viewDidAppear:(BOOL)animated
{
    self.screenName=@"Statistics screen";
    //sets the date
    self.labelClock.text=[self dateLeft];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)dateLeft
{
    NSTimeInterval epoch = [[[NSUserDefaults standardUserDefaults]stringForKey:kAPIKeyDateEvent] doubleValue];
    NSDate *dateEvent =[NSDate dateWithTimeIntervalSince1970:epoch];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit fromDate:[NSDate date] toDate:dateEvent options:0];
    int minutes = (int)components.minute;
    int hours = (int)components.hour;
    int days=(int)components.day;
    NSString * strminutes =NSLocalizedString(@"m", nil);
    NSString * strDays =NSLocalizedString(@"d", nil);
    NSString * strHours =NSLocalizedString(@"h", nil);
    return [NSString stringWithFormat:@"%d %@  |  %d %@  |  %d %@",days,strDays,hours,strHours,minutes,strminutes];
}

@end
