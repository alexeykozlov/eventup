//
//  InstuctionsView.m
//  InstructionsFramework
//
//  Created by Shoshi V. on 6/11/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "InstructionsView.h"

@implementation InstructionsView
{
    int _visiblePage;
}
- (id)initWithFrame:(CGRect)frame 
{
    self = [super initWithFrame:frame];
    if (self)
    {
        }
    return self;
}
#pragma mark - IB Metods
- (IBAction)didClickedInstructionsContinueButton:(id)sender
{
    // Dismiss & hide the instructions view.
    [self.delegate dismissInstructionsView];
    
}
#pragma mark -Set buttons
- (void)setButtons:(NSArray *)arrButtons
{
    
    //clean from view of butoons last buttons
    [self cleanViewOfButtonsFromLastButtons];
    
    //set  backgroungColor to view button
    self.viewButtons.backgroundColor = kColorBackGroundViewButton;
    
    //sum widh buttons and height of view buttons
    float maxHeight =0.0f;
    float sumWidthOfButtons =0.0f;
    for (UIButton *btn in arrButtons)
    {
        if (btn.frame.size.height >maxHeight)
        {
            maxHeight =btn.frame.size.height;
        }
        sumWidthOfButtons +=btn.frame.size.width;
    }
    //set frame to view buttons
    self.viewButtons.frame = CGRectMake(self.viewOfScroll.frame.origin.x,
                                        self.viewOfScroll.frame.origin.y +self.viewOfScroll.frame.size.height,
                                        self.viewOfScroll.frame.size.width,
                                        kMarginHightViewButton *2 +maxHeight);
    
    //calculater space between the buttons
    float space = (self.viewButtons.frame.size.width - kMarginWidthViewButton *2 - sumWidthOfButtons)/(arrButtons.count -1);
    
    //add buttons
    if (arrButtons.count ==1)
    {
        [self addOneButtonToViewButtons:arrButtons[0] inLocationY:kMarginHightViewButton];
    }
    else
    {
        [self addButtonsToViewButtons:arrButtons inLocationY:kMarginHightViewButton withSpace:space];
    }
    
    //center all the view
    [self changeLoctionOfViewsAfterPutButtons];
}
- (void)setButton:(UIButton *)oneButton
{
    //clean from view of butoons last buttons
    [self cleanViewOfButtonsFromLastButtons];
    
    //set  backgroungColor to view button
    self.viewButtons.backgroundColor = kColorBackGroundViewButton;
    
    //set frame to view buttons
    self.viewButtons.frame = CGRectMake(self.viewOfScroll.frame.origin.x,
                                        self.viewOfScroll.frame.origin.y +self.viewOfScroll.frame.size.height,
                                        self.viewOfScroll.frame.size.width,
                                        kMarginHightViewButton *2 +oneButton.frame.size.height);
    
    [self addOneButtonToViewButtons:oneButton inLocationY:kMarginHightViewButton];
    
    //center all the view
    [self changeLoctionOfViewsAfterPutButtons];
    
}
- (void)setButtonsToFirstLine:(NSArray *)arrButtons1 andButtonsToNextLine:(NSArray *)arrButtons2
{
    //clean from view of butoons last buttons
    [self cleanViewOfButtonsFromLastButtons];
    
    //set  backgroungColor to view button
    self.viewButtons.backgroundColor = kColorBackGroundViewButton;
    
    //sum widh buttons  In First Line and height of view buttons
    float maxHeightOfButtonsInFirstLine =0.0f;
    float sumWidthOfButtonsInFirstLine =0.0f;
    for (UIButton *btn in arrButtons1)
    {
        if (btn.frame.size.height >maxHeightOfButtonsInFirstLine)
        {
            maxHeightOfButtonsInFirstLine =btn.frame.size.height;
        }
        sumWidthOfButtonsInFirstLine +=btn.frame.size.width;
    }
    
    float maxHeightOfButtonsInSecondLine =0.0f;
    float sumWidthOfButtonsInSecondLine =0.0f;
    for (UIButton *btn in arrButtons2)
    {
        if (btn.frame.size.height >maxHeightOfButtonsInSecondLine)
        {
            maxHeightOfButtonsInSecondLine =btn.frame.size.height;
        }
        sumWidthOfButtonsInSecondLine +=btn.frame.size.width;
    }
    
    //set frame to view buttons
    self.viewButtons.frame = CGRectMake(self.viewOfScroll.frame.origin.x,
                                        self.viewOfScroll.frame.origin.y +self.viewOfScroll.frame.size.height,
                                        self.viewOfScroll.frame.size.width,
                                        kMarginHightViewButton *2 +maxHeightOfButtonsInFirstLine +maxHeightOfButtonsInSecondLine +kSpaceBetween2LineButtons);
    
    //calculater space between the buttons in first line
    float spaceBetweenButtonsInFirstLine = (self.viewButtons.frame.size.width - kMarginWidthViewButton *2 - sumWidthOfButtonsInFirstLine)/(arrButtons1.count -1);
    
    //add buttons
    if (arrButtons1.count ==1)
    {
        UIButton *oneButton = arrButtons1[0];
        [self addOneButtonToViewButtons:oneButton inLocationY:kMarginHightViewButton];
    }
    else
    {
        [self addButtonsToViewButtons:arrButtons1 inLocationY:kMarginHightViewButton withSpace:spaceBetweenButtonsInFirstLine];
    }
    
    
    //calculater space between the buttons in first line
    float spaceBetweenButtonsInSecondLine = (self.viewButtons.frame.size.width - kMarginWidthViewButton *2 - sumWidthOfButtonsInSecondLine)/(arrButtons2.count -1);
    
    
    float locationY  =kMarginHightViewButton +maxHeightOfButtonsInFirstLine +kSpaceBetween2LineButtons;

    //add buttons in second line
    if (arrButtons2.count ==1)
    {
        UIButton *btn = arrButtons2[0];
        [self addOneButtonToViewButtons:btn inLocationY:locationY];
    }
    else
    {
        [self addButtonsToViewButtons:arrButtons2 inLocationY:locationY withSpace:spaceBetweenButtonsInSecondLine];
        
    }
    
    //center all the view
    [self changeLoctionOfViewsAfterPutButtons];
}

#pragma mark -Other Metods
- (void)cleanViewOfButtonsFromLastButtons
{
    for (UIView *view in self.viewButtons.subviews)
    {
        if ([view isKindOfClass:[UIButton class]])
        {
            [view removeFromSuperview];
        }
    }
}
- (void)addOneButtonToViewButtons:(UIButton *)oneButton inLocationY:(float)locationY
{
    
    //set frame to one button
    oneButton.frame = CGRectMake((self.viewButtons.frame.size.width -kMarginWidthViewButton *2 -oneButton.frame.size.width)/2 +kMarginWidthViewButton,
                                 locationY,
                                 oneButton.frame.size.width,
                                 oneButton.frame.size.height);
    [self.viewButtons addSubview:oneButton];
    
}
- (void)addButtonsToViewButtons:(NSArray *)arrButtons inLocationY:(float)locationY withSpace:(float)space
{
    float locatioX = kMarginWidthViewButton;
    for (UIButton *btn in arrButtons)
    {
        btn.frame = CGRectMake(locatioX, locationY, btn.frame.size.width, btn.frame.size.height);
        [self.viewButtons addSubview:btn];
        locatioX += (btn.frame.size.width +space);
        
    }
    
}

- (void)changeLoctionOfViewsAfterPutButtons
{
    self.viewOfScroll.frame = CGRectMake(self.viewOfScroll.frame.origin.x,
                                         self.viewOfScroll.frame.origin.y - self.viewButtons.frame.size.height/2,self.viewOfScroll.frame.size.width,
                                         self.viewOfScroll.frame.size.height);
    
    self.pageControlInstructions.frame = CGRectMake(self.pageControlInstructions.frame.origin.x,
                                                    self.pageControlInstructions.frame.origin.y - self.viewButtons.frame.size.height/2,
                                                    self.pageControlInstructions.frame.size.width,
                                                    self.pageControlInstructions.frame.size.height);
    self.viewButtons.frame = CGRectMake(self.viewButtons.frame.origin.x,
                                                    self.viewButtons.frame.origin.y - self.viewButtons.frame.size.height/2,
                                                    self.viewButtons.frame.size.width,
                                                    self.viewButtons.frame.size.height);
    self.imageVBackgroundViewButtons.frame = CGRectMake(0, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height);
    self.imageVBackgroundViewButtons.image = [UIImage imageNamed:kImageBackGroundViewButton];

}
- (void)configureWithFrameDarkOverlayView:(CGRect)frame andNumPages:(int)numPages andArrayTitels:(NSArray *)arrTitels andArrayImages:(NSArray *)arrImages andArrayTexts:(NSArray *)arrTexts
{
    //set frame to view
    self.frame = frame;
    
    //Calculation frame of Instuctions View
    CGRect frameInstuctionsView = CGRectMake((self.frame.size.width - kWidthInstuctionsView)/2,
                                             (self.frame.size.height - kHeightInstuctionsView)/2,
                                             kWidthInstuctionsView,
                                             kHeightInstuctionsView);
    // set frame of Instuctions View
    self.viewOfScroll.frame = frameInstuctionsView;
    
    //set frame to darkOverlayView
    self.darkOverlayView.frame = frame;
    
    // set frame to pageControlInstructions
    self.pageControlInstructions.frame = CGRectMake((self.frame.size.width -self.pageControlInstructions.frame.size.width)/2,
                                                    frameInstuctionsView.origin.y+self.viewOfScroll.frame.size.height-self.pageControlInstructions.frame.size.height,
                                                    self.pageControlInstructions.frame.size.width,
                                                    self.pageControlInstructions.frame.size.height);
    
    self.arrTitels = arrTitels;
     //set array images                                                 
    self.arrImages = arrImages;
    
    //set array texts
    self.arrTexts = arrTexts;
    
    // set image to background
    self.imageVBackGround.image = [UIImage imageNamed:kImageBackGround];
    
    // Configure the page control.
    self.pageControlInstructions.numberOfPages = numPages;
    self.pageControlInstructions.pageIndicatorTintColor = [UIColor grayColor];
    self.pageControlInstructions.currentPageIndicatorTintColor = [UIColor blackColor];

    
    //init scroll view
    [self initViewPagesWithPageVisble:0 andNumPages:numPages];

}
- (void)initViewPagesWithPageVisble:(int)page andNumPages:(int)numPages
{
    self.arrPages = [[NSMutableArray alloc] init];

    for (unsigned i = 0; i < [self.arrImages count]; i++)
    {
		[ self.arrPages  addObject:[NSNull null]];
    }

    //set contant size to scrollView
    self.scrollView.contentSize = CGSizeMake(kWidthInstuctionsView *numPages,kHeightInstuctionsView);
    
    [self loadScrollViewWith3Pages:page];
    _visiblePage = page;
    [self updateScrollView];
    
}
- (void)showOnView:(UIView *)view
{
    // Transition the presentation of the instructions screen.
    [UIView transitionWithView:self
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve| UIViewAnimationOptionAllowAnimatedContent
                    animations:^{
                        [view addSubview:self];
                    } completion:NULL];

}
- (void)hide
{
    // Transition it's removal.
    [UIView transitionWithView:self
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self removeFromSuperview];
                    } completion:NULL];

}
#pragma mark - Scroll View Delegate Methods
- (void)scrollViewDidScroll:(UIScrollView *)sender
{    
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = self.scrollView.frame.size.width;
    int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    _visiblePage = page;
    [self loadScrollViewWith3Pages:page];
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    // Get the decelerated scroll view.
    uint selectedPage = scrollView.contentOffset.x / scrollView.bounds.size.width;
    
    // Set the new page to the page control.
    [self.pageControlInstructions setCurrentPage:selectedPage];
}
#pragma mark - Scroll Pages  Methods
- (void)loadScrollViewWith3Pages:(int)page
{
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    // A possible optimization would be to unload the views+controllers which are no longer visible
}
- (void)updateScrollView
{
    // update the scroll view to the appropriate page
    CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * _visiblePage;
    frame.origin.y = 0;
    [self.scrollView scrollRectToVisible:frame animated:YES];
}
- (void)loadScrollViewWithPage:(int)page
{
    if (page < 0)
        return;
    if (page >= [self.arrImages count])
        return;
    
    OnePage * onePage = [self.arrPages objectAtIndex:page];
    if ((NSNull *)onePage == [NSNull null])
    {
        onePage = [[OnePage alloc] init];
        [self.arrPages replaceObjectAtIndex:page withObject:onePage];
    }
    //set tag to page
        onePage.tag = page + 1;
    
    //set frame to page
        CGRect frame = self.scrollView.frame;
        frame.origin.x = kWidthInstuctionsView * page;
        frame.origin.y = 0;
        onePage.frame = frame;
    
    //set image to image view
        onePage.imageView.image = [UIImage imageNamed:self.arrImages[page]];
    
    //set text to text view
    onePage.textView.text = self.arrTexts [page];
    
    onePage.lblTitle.text = self.arrTitels[page];
    
//    //add button letGo to last page
//    if (onePage.tag == self.arrPages.count)
//    {
//        [onePage addSubview:self.btnLetsGo];
//    }
    [self.scrollView addSubview:onePage];
}

@end
