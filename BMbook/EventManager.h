//
//  EventManager.h
//  BMbook
//
//  Created by iApps Mac Mini on 9/9/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventManager : NSObject

+(void) addInvitationWithId:(int)eventId andKeyword:(int)keyword andImage:(NSData*)image
               Success:(void (^)())successBlock
               failure:(void (^)())failureBlock;

+(void) getInvitationWithId:(int)eventId
               Success:(void (^)(NSURL* imageUrl))successBlock
               failure:(void (^)())failureBlock;

+(void) addWishWithId:(int)eventID andKeyword:(int)keyword andAuthorName:(NSString*)authorName andText:(NSString*)text andImage:(NSData*)image
               Success:(void (^)())successBlock
               failure:(void (^)())failureBlock;

+(void) getWishesWithId:(int)eventId
                Success:(void (^)(NSArray * arrWishes))successBlock
                failure:(void (^)())failureBlock;

+(void) deleteWishWithID:(int)eventId andKeyword:(int)keyword andWishId:(int)wishId
                Success:(void (^)())successBlock
                failure:(void (^)())failureBlock;

@end
