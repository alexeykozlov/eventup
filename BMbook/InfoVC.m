//
//  InfoVC.m
//  BMbook
//
//  Created by Shoshi V. on 5/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "InfoVC.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@interface InfoVC ()
{
    BOOL _isLoginFacebook;
}
@end

@implementation InfoVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Remove ourselves from being observer's to facebook's session changes
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Register to our notification center
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(sessionStateChanged:)
     name:kFBSessionStateChangedNotification
     object:nil];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.delegate allowInteractiveSlideing:TRUE];
	self.title = NSLocalizedString(@"אודות", nil);
    [self.btnMoreApps setTitle:NSLocalizedString(@"אפליקציות נוספות", nil) forState:UIControlStateNormal];
    [self.btnShareInEmail setTitle:NSLocalizedString(@"שיתוף באימייל", nil) forState:UIControlStateNormal];
     [self.btnShareInFacebook setTitle:NSLocalizedString(@"שיתוף בפייסבוק", nil) forState:UIControlStateNormal];
    self.lblVersion.text = NSLocalizedString(@"גרסה 1.0",nil);
    
    //set date
    self.lblDate.text =[self returnStringOfStatusEvent];
    //sets the date color
    self.lblDate.textColor=[UIColor colorWithRed:(255/255.0) green:(220/255.0) blue:(90/255.0) alpha:1] ;
}

-(void)viewDidAppear:(BOOL)animated
{
    self.screenName=@"Info screen";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)moreApps:(id)sender
{
    if (SYSTEM_VERSION_LESS_THAN (@"6.0"))
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/il/artist/umbrella-business-development/id371057132"]];
    }
    else
    {
        NSDictionary *appParameters = [NSDictionary dictionaryWithObject:@"371057132"
                                                                  forKey:SKStoreProductParameterITunesItemIdentifier];
        
        SKStoreProductViewController *productViewController = [[SKStoreProductViewController alloc] init];
        [productViewController setDelegate:self];
        [productViewController loadProductWithParameters:appParameters
                                         completionBlock:^(BOOL result, NSError *error)
         {
             
         }];
        [self presentViewController:productViewController
                           animated:YES
                         completion:^{
                             
                         }];
    }
  
}
- (IBAction)shareInEmail:(id)sender
{
//    if ([MFMailComposeViewController canSendMail])
//    {
//        MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc]init];
//        //[mailComposer setToRecipients:[NSArray arrayWithObject:kEmailOfContactUs]];
//        [mailComposer setSubject:NSLocalizedString(@"EventUp", nil)];
//
//// [mailComposer setMessageBody:@"היי!\nכדאי שתוריד/י את אפליקציית הבר מצווה של יונתן\nhttps://itunes.apple.com/us/app/bar-mitzva-book/id652071738?ls=1&mt=8" isHTML:TRUE];
////        
//        // Fill out the email body text
//        NSString *iTunesLink = @"https://itunes.apple.com/us/app/bar-mitzva-book/id652071738?ls=1&mt=8"; // Link to iTune App link
//        NSString *content = @"היי!\nכדאי שתוריד/י את אפליקציית החתונות EventUp";
//        NSString *emailBody = [NSString stringWithFormat:@"%@<p>\r\n<a href = '%@'>%@</a></p>", content, iTunesLink,iTunesLink];
//        
//        [mailComposer setMessageBody:emailBody isHTML:YES];
//        [mailComposer setMailComposeDelegate:self];
//        [self presentViewController:mailComposer animated:YES completion:nil];
//    }
//    else
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"תקלה", nil)  message:NSLocalizedString(@"לא ניתן לשלוח מייל,אנא בדוק אם יש חשבון דואר מוגדר במכשיר.",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"סגור" ,nil)otherButtonTitles:nil];
//        [alert show];
//    }
    
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil) {
        MFMailComposeViewController * mailController = [[MFMailComposeViewController alloc] init];
        mailController.mailComposeDelegate = self;
        
        // NSString *downLoadNow = @"הורידו עכשיו את האפליקציה וקבלו חוקים מצחיקים מידי יום!";
        // NSString *theFunnySentence =sTheFunnySentence;
        //NSString *downloadAndFunny = [downLoadNow stringByAppendingString:theFunnySentence];
        
        //Set the subject
        [mailController setSubject:@"הודעה מEventUp"];
        
        //Set the mail body
        [mailController setMessageBody:@"הורידו את אפליקציית החתונות EventUp    https://itunes.apple.com/us/app/bar-mitzva-book/id652071738?ls=1&mt=8" isHTML:NO];
        
        
        
        UIImage *myImage = [UIImage imageNamed:@"faceBtn.png"];
        NSData *imageData = UIImagePNGRepresentation(myImage);
        [mailController addAttachmentData:imageData mimeType:@"image/png" fileName:@"bookButtnMainScreen"];
        
        //Display Email Composer
        if([mailClass canSendMail]) {
            [mailController setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
            [self presentViewController:mailController animated:YES completion:nil];
        }
    }
}

- (IBAction)shareInFacebook:(id)sender
{
    //check if Facebook Account is linked
    SLComposeViewController *mySLComposerSheet;
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        //initiate the Social Controller
        mySLComposerSheet = [[SLComposeViewController alloc] init];
        
        //Tell him with what social plattform to use it, e.g. facebook or twitter
        mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [mySLComposerSheet setInitialText:@"הורידו את אפליקציית החתונות EventUp    https://itunes.apple.com/us/app/bar-mitzva-book/id652071738?ls=1&mt=8"];
        //the message you want to post
        [mySLComposerSheet addImage:[UIImage imageNamed:@"faceBtn.png"]]; //an image you could post
        //for more instance methodes, go here:https://developer.apple.com/library/ios/#documentation/NetworkingInternet/Reference/SLComposeViewController_Class/Reference/Reference.html#//apple_ref/doc/uid/TP40012205
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    }
    
    else {
        UIAlertView *dontHaveFacebookAlert = [[UIAlertView alloc] initWithTitle:@"מצטערים" message:@"על מנת לשתף בפייסבוק עליך להתחבר לחשבון הפייסבוק שלך במסך ההגדרות של המכשיר." delegate:nil cancelButtonTitle:@"המשך" otherButtonTitles:nil];
        [dontHaveFacebookAlert show];
    }
    
    [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        NSString *output;
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                output = @"Action Cancelled";
                break;
            case SLComposeViewControllerResultDone:
                output = @"Post Successfull";
                break;
            default:
                break;
        }
        //check if everything worked properly. Give out a message on the state.
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }];
}

- (IBAction)showInstructionsButtonWasPressed:(id)sender
{
    if(self.instructionsView.hidden == NO)
    {
    [self.navigationController.navigationBar setHidden:NO];
    NSArray *arrImages = @[@"HTU_8",@"HTU_9",@"HTU_10",@"HTU_6"];
    NSArray *arrTexts = @[@"הזינו את פרטי האירוע, תוכלו לערוך את הפרטים בכל עת",@"פתחו חשבון בחברת Cellarix ותחסכו זמן יקר בטיפול בצ'קים",@"צרפו את הזמנת החתונה כדי שתוכלו לשתף אותה עם האורחים",@"המסך מציג את הזמן שנותר עד החתונה, מספר הצפיות באירוע ומספר הברכות שהתקבלו"];
    NSArray *arrTitels = @[@"מתחתנים? מזל טוב!",@"המתנות שלכם במקום אחד",@"הזמנה לאירוע",@"סטטיסטיקה"];
    
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:kNotFirstCreatorEnter];
    
    self.instructionsView =[[[NSBundle mainBundle] loadNibNamed:@"InstructionsView" owner:nil options:nil]objectAtIndex:0];
    self.instructionsView.delegate =self;
    [self.instructionsView configureWithFrameDarkOverlayView:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) andNumPages:4 andArrayTitels:arrTitels andArrayImages:arrImages andArrayTexts:arrTexts];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundColor:[UIColor clearColor]];
    btn.titleLabel.font = [UIFont systemFontOfSize:12];
    [btn setTitleColor:[UIColor colorWithRed:(255/255.0) green:(220/255.0) blue:(90/255.0) alpha:1] forState:UIControlStateNormal];
    
    [btn setTitle:@"אישור" forState:UIControlStateNormal];
    btn.frame = CGRectMake(0, 0, 80, 40);
    [btn setBackgroundImage:[UIImage imageNamed:@"button_small"] forState:UIControlStateNormal];
    [btn addTarget:self.instructionsView action:@selector(didClickedInstructionsContinueButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.instructionsView setButton:btn];
    
    //add instructions View
        [self.instructionsView showOnView:self.view];}
}
//- (void)publishStory
//{
//
//    NSString *str = [NSString stringWithFormat:@"כדאי שתוריד/י את אפליקציית החתונות EventUp"];
//    self.postParams =[[NSMutableDictionary alloc] initWithObjects:@[str,@"https://itunes.apple.com/us/app/bar-mitzva-book/id652071738?ls=1&mt=8"] forKeys:@[@"message", @"link"]];
//    [FBRequestConnection
//     startWithGraphPath:@"me/feed"
//     parameters:self.postParams
//     HTTPMethod:@"POST"
//     completionHandler:^(FBRequestConnection *connection,
//                         id result,
//                         NSError *error) {
//         NSString *alertText;
//         if (error)
//         {
//             //             alertText = [NSString stringWithFormat:
//             //                          @"error: domain = %@, code = %@",
//             //                          error.domain, error.localizedDescription];
//             alertText = @"מצטערים,ארעה תקלה בשיתוף בפייסבוק";
//         } else
//         {
//             alertText = @"השיתוף בפייסבוק הצליח!";
//         }
//         // Show the result in an alert
//         [[[UIAlertView alloc] initWithTitle:nil
//                                     message:alertText
//                                    delegate:self
//                           cancelButtonTitle:@"סגור"
//                           otherButtonTitles:nil]
//          show];
//     }];
//}

- (IBAction)didClickedInstructionsContinueButton:(id)sender
{
    // Dismiss & hide the instructions view.
    [self.delegate2 dismissInstructionsView];
}

#pragma InstuctionsViewDelegate Metods

- (void)dismissInstructionsView
{
    //remove instructions View
    [self.instructionsView hide];
}

#pragma mark -
#pragma mark SKStoreProductViewControllerDelegate
- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController
{
    [viewController dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark -
#pragma mark MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
    
#if __IPHONE_OS_VERSION_MIN_REQUIRED < 60000
	[self dismissModalViewControllerAnimated:YES];
#else
    [self dismissViewControllerAnimated:YES completion:NULL];
#endif
}
#pragma mark - Facebook session methods

//- (void)sessionStateChanged:(NSNotification*)notification
//{
//    // Check if a facebook session is open
//    if (FBSession.activeSession.isOpen)
//    {
//        if (!_isLoginFacebook)
//        {
//            _isLoginFacebook = TRUE;
//            if ([FBSession.activeSession.permissions
//                 indexOfObject:@"publish_actions"] == NSNotFound)
//            {
//                // No permissions found in session, ask for it
//                [FBSession.activeSession
//                 requestNewPublishPermissions:
//                 [NSArray arrayWithObject:@"publish_actions"]
//                 defaultAudience:FBSessionDefaultAudienceFriends
//                 completionHandler:^(FBSession *session, NSError *error) {
//                     if (!error)
//                     {
//                         // If permissions granted, publish the story
//                         [self publishStory];
//                     }
//                 }];
//            }
//            else
//            {
//                // If permissions present, publish the story
//                [self publishStory];
//            }
//            
//        }
//        else
//        {
//        }
//        
//        
//    }
//}


@end
