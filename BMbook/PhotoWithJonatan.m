//
//  PhotoWithJonatan.m
//  BMbook
//
//  Created by Shoshi V. on 5/16/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "PhotoWithJonatan.h"

@interface PhotoWithJonatan ()

@end

@implementation PhotoWithJonatan

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
