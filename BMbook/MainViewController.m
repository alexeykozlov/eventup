
//
//  MainViewController.m
//  BiBi
//
//  Created by Shoshi V. on 2/21/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()
{
    NSArray *_arrayTitles;
    NSArray *_arrIcons;
    
}
@end

@implementation MainViewController

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


#define knumberOfRows 10

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //sets background
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"sidemenu_bg.png"]];
    
    //init array with titels
    _arrayTitles = [NSArray arrayWithObjects:NSLocalizedString(@"ההזמנה לאירוע", nil),
                    NSLocalizedString(@"שלח/י ברכה",nil),
                    NSLocalizedString(@"אלבום הברכות", nil),
                    NSLocalizedString(@"שלח/י מתנה", nil),
                    NSLocalizedString(@"קח אותי לאירוע",nil),
                    NSLocalizedString(@"שיר החתונה",nil),
                    NSLocalizedString(@"עבור לאירוע אחר", nil),
                    NSLocalizedString(@"אודות", nil),nil];
    
    _arrIcons = [NSArray arrayWithObjects:@"sidemenu_invite",@"sidemenu_send_braha",@"sidemenu_gallery",@"sidemenu_gift",@"sidemenu_navigate",@"sidemenu_song_icon",@"sidemenu_logout",@"sidemenu_info",nil];
    self._selectedRow = 1;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return knumberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if (indexPath.row >0 && indexPath.row -1 <_arrayTitles.count)
    {
        //create cell
        CustomCellMain *customcell = (CustomCellMain *)[tableView dequeueReusableCellWithIdentifier:@"CustomCellMain"];
        
        //set text to lblTitle
        customcell.lblTitle.text = [_arrayTitles objectAtIndex:indexPath.row -1];
       
        
        // Highlight selected row
        if (self._selectedRow == indexPath.row)
        {
            customcell.lblTitle.textColor =[UIColor colorWithRed:255/255.0 green:220/255.0 blue:90/255.0 alpha:1];
            customcell.imagVIcon.image = [UIImage imageNamed:[[_arrIcons objectAtIndex:indexPath.row -1] stringByAppendingString:@"_highlight"]];
        }
        else
        {
             customcell.lblTitle.textColor =[UIColor darkGrayColor];
             customcell.imagVIcon.image = [UIImage imageNamed:[_arrIcons objectAtIndex:indexPath.row -1]];
            
             customcell.imagVIcon.layer.shadowOpacity = 0.0f;
             customcell.imagVIcon.layer.shadowRadius = 0.0f;
        }
        cell = customcell;
        

    }
    else if (indexPath.row == 0)//title of table
    {
        CustomCellOnlyText * customCellOnlyText =(CustomCellOnlyText *)[tableView dequeueReusableCellWithIdentifier:@"CustomCellOnlyText"];
        
        //set text to lblTitle
        customCellOnlyText.appNameImage.image=[UIImage imageNamed:@"sidemenu_title.png"];
        cell = customCellOnlyText;

    }
    else//symbol
    {
       CustomCellWithSymbol *customCellWithSymbol =(CustomCellWithSymbol *)[tableView dequeueReusableCellWithIdentifier:@"CustomCellWithSymbol"];
        customCellWithSymbol.imageV.image=[UIImage imageNamed:@"sidemenu_decoration.png"];
        cell =customCellWithSymbol;
    }
       return cell;
}

- (float)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}
- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

   if (indexPath.row ==0||indexPath.row -1 <_arrayTitles.count)
   {
        return 44;
   }
    //last row with image
    return self.view.frame.size.height - (44*(knumberOfRows -1));
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle: nil];
    if (indexPath.row >0 &&indexPath.row -1 <_arrayTitles.count)
    {
        if(indexPath.row!=6 || !([[[NSUserDefaults standardUserDefaults]stringForKey:kAPIKeySong]isEqualToString:@""]))
        {
            //draw old selected row
            CustomCellMain *cell = (CustomCellMain *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self._selectedRow inSection:0]];
            cell.lblTitle.textColor =[UIColor darkGrayColor];
            cell.imagVIcon.image = [UIImage imageNamed:[_arrIcons objectAtIndex:self._selectedRow -1]];
        
            //draw new selected row
            self._selectedRow = indexPath.row;
            cell = (CustomCellMain *)[tableView cellForRowAtIndexPath:indexPath];
            cell.lblTitle.textColor =[UIColor colorWithRed:255/255.0 green:220/255.0 blue:90/255.0 alpha:1];
            cell.imagVIcon.image = [UIImage imageNamed:[[_arrIcons objectAtIndex:indexPath.row -1] stringByAppendingString:@"_highlight"]];
        }
        switch (indexPath.row)
        {
            case kOptionToGiveBlessing:
            {
                GiveBlessingViewController *giveBlessingViewController = [mainStoryboard instantiateViewControllerWithIdentifier:kIdentifierGiveBlessingVC];
                 [self.delegate selectOneOption:giveBlessingViewController];
                break;
            }
            case kOptionToShowAlbumBlessings:
            {
                ShowAlbumBlessingsVC *showAlbumBlessingsVC = [mainStoryboard instantiateViewControllerWithIdentifier:kIdentifierShowAlbumBlessingsVC];
                [self.delegate selectOneOption:showAlbumBlessingsVC];
                break;
            }
            case kOptionToSendGift:
            {
                SendGiftViewController *sendGiftVC =[mainStoryboard instantiateViewControllerWithIdentifier:kIdentifierSendGiftVC];
                [self.delegate selectOneOption:sendGiftVC];
                break;
            }
            case kOptionToShowInvitation:
            {
                ShowInvitationVC *showInvitationVC = [mainStoryboard instantiateViewControllerWithIdentifier:kIdentifierShowInvitationVC];
                [self.delegate selectOneOption:showInvitationVC];
                break;
            }
            case kOptionToNavigateMe:
            {
                NavigateMeVC * navigateMeVC =[mainStoryboard instantiateViewControllerWithIdentifier:kIdentifierNavigateMeVC];
                [self.delegate selectOneOption:navigateMeVC];
                break;
            }
            case kOptionToSong:
            {
                if([[[NSUserDefaults standardUserDefaults]stringForKey:kAPIKeySong]isEqualToString:@""])
                {
                    UIAlertView *alert =[[UIAlertView alloc] initWithTitle:nil message:@"לא נבחר שיר חתונה" delegate:self cancelButtonTitle:@"בטל" otherButtonTitles:nil, nil];
                    [alert show];
                }
                else
                {
                    SongViewController *songVC=[mainStoryboard instantiateViewControllerWithIdentifier:kIdentifierSongVC];
                    [self.delegate selectOneOption:songVC];
                }
                break;
            }
            case kOptionToGoOtherEvent:
            {
                UIAlertView *alert =[[UIAlertView alloc] initWithTitle:nil message:@"האם ברצונך להתנתק מהאירוע?" delegate:self cancelButtonTitle:@"בטל" otherButtonTitles:@"כן", nil];
                [alert show];
                break;
            }
            case kOptionToShowInfo:
            {
                InfoVC *infoVc =[mainStoryboard instantiateViewControllerWithIdentifier:kIdentifierShowInfoVC];
                [self.delegate selectOneOption:infoVc];
                break;
            }
            default:
                break;
        }
    }
}


    
#pragma mark - UIAlertview delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex)
    {
        //restar in user default
        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:0] forKey:kAPIKeyEventID];
        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:kAPIKeyEventName];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        self._selectedRow = 1;
        [self.tableView reloadData];
        
        [self.delegate pushFirstViewController];
        
    }

}

@end
