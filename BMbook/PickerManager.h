//
//  PickerManager.h
//  IDareU
//
//  Created by iApps Dev. Mac mini on 3/7/13.
//  Copyright (c) 2013 Roee Kattaby. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PickerTextField.h"

@interface PickerManager : NSObject

+ (PickerManager*)getInstance;

- (void)createPickerForTextField:(PickerTextField*)textField
                           ofTag:(NSInteger)tag
                   withPListName:(NSString*)fileName
               forViewController:(id<PickerTextFieldDelegate>)viewController;

- (void)createDatePickerWithTimeForTextField:(PickerTextField*)textField
                           forViewController:(id<PickerTextFieldDelegate>)viewController;

- (void)createPickerForTextField:(PickerTextField *)textField
                           ofTag:(NSInteger)tag
                  fromDictionary:(NSDictionary*)dictionary
               forViewController:(id<PickerTextFieldDelegate>)viewController;

@end
