//
//  CustomAnnotationView.m
//  BMbook
//
//  Created by Shoshi V. on 5/20/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "CustomAnnotationView.h"

@implementation CustomAnnotationView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"TOUCHED BTNNNN");
}

- (id)initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self != nil)
    {
//        [[NSBundle mainBundle] loadNibNamed:@"CustomAnnotationView" owner:self options:nil];
//        if (self.vCustomAnnotation)
//        {
//            [self.vCustomAnnotation setCenter:CGPointMake(0, 0)];
//            [self addSubview:self.vCustomAnnotation];
//        }
        self.vCustomAnnotation = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 34, 35)];
        [self addSubview:self.vCustomAnnotation];
        self.imgUser = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 34, 35)];
        [self.imgUser setImage:[UIImage imageNamed:@"yonatan_map_icon.png"]];
        self.imgUser.backgroundColor = [UIColor clearColor];
        [self addSubview:self.imgUser];
        self.btn = [CustomButton buttonWithType:UIButtonTypeCustom];
        [self.btn setTitle:@"" forState:UIControlStateNormal];
        self.btn.frame = CGRectMake(0, 0, 34, 35);
         [self addSubview:self.btn];
    }
    return self;
}

//
//// See this for more information: https://github.com/nfarina/calloutview/pull/9
//- (UIView *) hitTest:(CGPoint)point withEvent:(UIEvent *)event
//{
//    UIView *calloutMaybe = [self.calloutView hitTest:[self.calloutView convertPoint:point fromView:self] withEvent:event];
//    
//    return calloutMaybe;
//}

@end
