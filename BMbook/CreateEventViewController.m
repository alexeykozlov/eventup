//
//  CreateEventViewController.m
//  BMbook
//
//  Created by iApps Mac Mini on 9/9/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "CreateEventViewController.h"
#import "CreatorSlideViewController.h"

@interface CreateEventViewController ()

@end

@implementation CreateEventViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=@"צור אירוע";
    
    self.username.placeholder=@"שם משתמש";
    self.username.delegate=self;
    self.username.textAlignment=NSTextAlignmentRight;
    self.password.placeholder=@"סיסמא";
    self.password.secureTextEntry=YES;
    self.password.delegate=self;
    self.password.textAlignment=NSTextAlignmentRight;
    self.confirmPassword.placeholder=@"אמת סיסמא";
    self.confirmPassword.secureTextEntry=YES;
    self.confirmPassword.delegate=self;
    self.confirmPassword.textAlignment=NSTextAlignmentRight;
    self.eventName.placeholder=@"שם האירוע";
    self.eventName.delegate=self;
    self.eventName.textAlignment=NSTextAlignmentRight;
    self.code.placeholder=@"קוד האירוע";
    self.code.delegate=self;
    self.code.keyboardType = UIKeyboardTypeNumberPad;
    self.code.textAlignment=NSTextAlignmentRight;
    self.adress.placeholder=@"כתובת האירוע";
    self.adress.delegate=self;
    self.adress.textAlignment=NSTextAlignmentRight;
    self.dateAndTime.placeholder=@"תאריך ושעה";
    self.dateAndTime.delegate=self;
    self.dateAndTime.textAlignment=NSTextAlignmentRight;
    [[PickerManager getInstance]createDatePickerWithTimeForTextField:self.dateAndTime forViewController:self];
    
    //sets the tap recognizer
    UITapGestureRecognizer * recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    recognizer.delegate = self;
    [self.view addGestureRecognizer:recognizer];
    
    //customizes the back button
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 25.0f, 27.0f)];
    [backButton setImage:[UIImage imageNamed:@"nav_bar_back_btn"]  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(popVC) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
}

//dissmisses the vc
- (void) popVC{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) viewDidAppear:(BOOL)animated
{
    self.screenName=@"Register screen";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setUsername:nil];
    [self setPassword:nil];
    [self setEventName:nil];
    [self setCode:nil];
    [self setAdress:nil];
    [self setAllowGifts:nil];
    [self setConfirmPassword:nil];
    [self setScroll:nil];
    [self setDateAndTime:nil];
    [super viewDidUnload];
}

#pragma mark - text field methods


-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    [textField resignFirstResponder];
    return NO; // We do not want UITextField to insert line-breaks.
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField==self.code){
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 4) ? NO : YES;
    }
    else
        return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    //sets the scroll view
    [self.scroll setScrollEnabled:YES];
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    int tag=textField.tag;
    if (screenSize.height <= 480.0f)
    {
        [self.scroll setContentSize:CGSizeMake(320,720)];
        //zooms to a text field when keyboard is up
        if(tag==1 || tag==2 || tag==3 || tag==4)
            [self.scroll setContentOffset:CGPointMake(0,20) animated:YES];
        else if(tag==5 || tag==6)
            [self.scroll setContentOffset:CGPointMake(0,200) animated:YES];
        else if(tag==7)
        {
            [self.scroll setContentOffset:CGPointMake(0,240) animated:YES];
        }
    }
    else
    {
        [self.scroll setContentSize:CGSizeMake(320,780)];
        //zooms to a text field when keyboard is up
        if(tag==1 || tag==2 || tag==3)
            [self.scroll setContentOffset:CGPointMake(0,-10) animated:YES];
        else if(tag==4 ||tag==5 || tag==6 || tag==7)
            [self.scroll setContentOffset:CGPointMake(0,110) animated:YES];

    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    if (screenSize.height <= 480.0f)
    {
        [self.scroll setContentOffset:CGPointMake(0,0) animated:YES];
        [self.scroll setScrollEnabled:NO];
    }
    else
    {
        [self.scroll setContentOffset:CGPointMake(0,-30) animated:YES];
        [self.scroll setScrollEnabled:NO];
    }
}

#pragma mark - details validation methods
//checks if the username is in an email format
- (BOOL)checkUsername:(NSString*)username
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:username];
}

//checks if the password and the confirm password fields match
-(BOOL) checkPasswordsWithPassword:(NSString*)password andConfirmPassword:(NSString*)confirmPassword
{
    if([password isEqual:confirmPassword])
        return YES;
    else
        return NO;
}

//checks that the code is exactly 4 numbers
-(BOOL) checkCode:(NSString*)code
{
    if(code.length<4)
        return NO;
    else
        return YES;
}

#pragma mark - register method
-(IBAction) registerEvent: (id)sender
{
    if([self checkUsername:self.username.text]==NO)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"שגיאה" message:@"אימייל לא נכון" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else if (self.password.text.length==0){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"שגיאה" message:@"הזן סיסמא" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else if ([self checkPasswordsWithPassword:self.password.text andConfirmPassword:self.confirmPassword.text]==NO )
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"שגיאה" message:@"הססמאות לא תואמות" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else if (self.eventName.text.length==0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"שגיאה" message:@"הזן שם אירוע" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else if([self checkCode:self.code.text]==NO)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"שגיאה" message:@"קוד האירוע צריך להיות בן 4 ספרות" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else if (self.adress.text.length==0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"שגיאה" message:@"הזן כתובת" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else if (self.dateAndTime.text.length==0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"שגיאה" message:@"בחר תאריך" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    } else
    {
        NSLog(@"%@",self.username.text);
        NSLog(@"%@",self.password.text);
        [GeneralManager registerEventWithMail:self.username.text andPassword:self.password.text andAdress:self.adress.text andAllowGifts:self.allowGifts.on andName:self.eventName.text andCode:[self.code.text intValue] andDate:self.unixTime
                               Success:^{
                                   CreatorSlideViewController *slideController ;
                                   slideController = [[CreatorSlideViewController alloc] init];
                                   slideController.slideViewPaddingRight = 60.0;
                                   slideController.slideViewPaddingLeft = 0;
                                   //changes the mode to creator
                                   [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kIsCreator];
                                   //moves to log in view
                                   [self presentViewController:slideController animated:YES completion:^{[self.navigationController popViewControllerAnimated:YES];}];
                                }
                               failure:^{
                               }];
        
    }
}

#pragma mark - Date and time picker delegate
-(void)didSelectDatePickerOption:(PickerTextField *)chosenTextField chosenDate:(NSDate *)date
{
    // Getting the Hour
    NSDateFormatter* formatterDate = [[NSDateFormatter alloc] init];
    //the date format will be hours|minutes
    [formatterDate setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    NSString* strDate = [formatterDate stringFromDate:date];
    //convirting the string to NSDate
    //self.date = [[NSDate alloc] init];
    date = [formatterDate dateFromString:strDate];
    self.unixTime=[date timeIntervalSince1970];
    
  
    // Show in picker
    self.dateAndTime.text =strDate;
    [chosenTextField resignFirstResponder];
}
-(void)didCancelPicker:(PickerTextField*)chosenTextField
{
    //if pressing cancel on time picker cancel all notification
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    //close the picker
    [chosenTextField resignFirstResponder];
    //clear the text in the textField
    [self.dateAndTime setText:@""];
}

#pragma mark - gesture methods
- (void)handleTap:(UITapGestureRecognizer *)recognizer {
    if([self.username isFirstResponder])
        [self.username resignFirstResponder];
    else if([self.password isFirstResponder])
        [self.password resignFirstResponder];
    else if([self.confirmPassword isFirstResponder])
        [self.confirmPassword resignFirstResponder];
    else if([self.eventName isFirstResponder])
        [self.eventName resignFirstResponder];
    else if([self.code isFirstResponder])
        [self.code resignFirstResponder];
    else if([self.adress isFirstResponder])
        [self.adress resignFirstResponder];
    else if([self.dateAndTime isFirstResponder])
        [self.dateAndTime resignFirstResponder];
}


@end
