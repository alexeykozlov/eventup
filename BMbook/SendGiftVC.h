//
//  SendGiftVC.h
//  EventUp
//
//  Created by Alexey on 11/4/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "SlideOptionViewController.h"


@interface SendGiftVC : SlideOptionViewController <NSXMLParserDelegate,UIScrollViewDelegate,UITextFieldDelegate,UITextViewDelegate>

-(IBAction) call: (id)sender;



@property (strong, nonatomic) IBOutlet UIImageView *imgLogo;
@property (strong, nonatomic) IBOutlet UILabel *lblNoGift;
@property (strong, nonatomic) IBOutlet UIButton *buttonSend;
@property (strong, nonatomic) IBOutlet UITextField *textFieldSum;
@property (strong, nonatomic) IBOutlet UITextView *textViewBless;
@property (strong, nonatomic) IBOutlet UILabel *lblAddGift;
@property (strong, nonatomic) IBOutlet UIScrollView *scroll;

@end
