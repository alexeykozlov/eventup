                                                   //
//  ShowAlbumBlessingsVC.m
//  BMbook
//
//  Created by Shoshi V. on 5/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "ShowAlbumBlessingsVC.h"

@interface ShowAlbumBlessingsVC ()
{
    ManagerService *managerService;
     int _indexOfSelectedPhoto;
}
@end

@implementation ShowAlbumBlessingsVC

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:FALSE];
    [self getWishesFromServer];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)getWishesFromServer
{
    //init arr of photo
    self.arrPhoto =[[NSMutableArray alloc] init];

    //displays hud
    MBProgressHUD * HUD = [[MBProgressHUD alloc] initWithWindow:[UIApplication sharedApplication].keyWindow];
    HUD.labelText = @"טוען ברכות...";
    [self.view.window addSubview:HUD];
    [HUD show:YES];
    [EventManager getWishesWithId:[[NSUserDefaults standardUserDefaults] stringForKey:kAPIKeyEventID].intValue
                          Success:^(NSArray *arrWishes){
                              [HUD hide:YES];
                              NSArray *arrTemp = [arrWishes sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                                  NSNumber * num1 = [ NSNumber numberWithInt:((Wish *)obj1).nID];
                                  NSNumber * num2 = [ NSNumber numberWithInt:((Wish *)obj2).nID];
                                  return [num2 compare:num1];
                              }];
                              self.arrWishes = arrTemp;
                              
                              //add photo to arr
                              for (Wish *aWish in arrTemp)
                              {
                                  [self.arrPhoto addObject:aWish.urlSmallImage];
                              }
                              
                              [self.tableViewAlbum reloadData];
                              [self.tableViewAlbum.pullToRefreshView stopAnimating];

                          }
                          failure:^(){
                              [HUD hide:YES];
                          }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.delegate allowInteractiveSlideing:TRUE];
    
    //set date
    self.lblDate.text =[self returnStringOfStatusEvent];
    //sets the date color
    self.lblDate.textColor=[UIColor colorWithRed:(255/255.0) green:(220/255.0) blue:(90/255.0) alpha:1] ;

    //set title
	self.title = NSLocalizedString(@"אלבום הברכות", nil);
    //init arr of photo
    self.arrPhoto =[[NSMutableArray alloc] init];
    
    //add pull to refresh
    [self.tableViewAlbum addPullToRefreshWithActionHandler:^{
        
    //set color to arrow
    self.tableViewAlbum.pullToRefreshView.arrowColor = [UIColor whiteColor];
    [self getWishesFromServer];
    }];
}

-(void) viewDidAppear:(BOOL)animated
{
    self.screenName=@"Blessings album screen";
}

- (void)popController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kShowBigPhotosSegue])
    {
        ShowBigPhotosViewController *showBigPhotosViewController = segue.destinationViewController;
        showBigPhotosViewController.arrPhotos =[NSMutableArray arrayWithArray: self.arrWishes];
        showBigPhotosViewController.indexOfSelectedPhoto = _indexOfSelectedPhoto;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)btnShowMainMenuPressed:(id)sender
{
    [self.delegate showMainMenu:sender];
}

#pragma mark - Table View Data Source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 113.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    float numCell = ((float)[self.arrPhoto count] /3.0f);
    int intpart = (int)numCell;
    float decpart = (float)numCell - (int)intpart;
    if (decpart)
    {
        intpart++;
    }
	return intpart;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    int index = indexPath.row * 3 ;
    float width =100.0f;
    float space = 5.0f;
    float height = 100.0f;
    float y =6;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    int i = 0;
    for (UIView *view in cell.contentView.subviews)
    {
        if ([view class] == [OneImageOfAlbumView class])
        {
            [view removeFromSuperview];
        }
    }
    
    for (; i < 3 && index < self.arrPhoto.count; i++ ,index ++)
    {
        
        NSURL *urlImage = [self.arrPhoto objectAtIndex:index];
        
        //create oneImageOfAlbumView
        OneImageOfAlbumView *oneImageOfAlbumView =[[[NSBundle mainBundle] loadNibNamed:@"OneImageOfAlbumView"
                                                                                 owner:nil
                                                                               options:nil] objectAtIndex:0];
        oneImageOfAlbumView.frame = CGRectMake((width +space) * i + space,
                                               y,
                                               width,
                                               height);
        
        
        oneImageOfAlbumView.tag = index;
        oneImageOfAlbumView.delegate =self;
        if (oneImageOfAlbumView.imageAccesory.image == nil)
        {
            __weak ASIHTTPRequest* httpReq = [ASIHTTPRequest requestWithURL:urlImage];
            [httpReq setCompletionBlock:^{
                [oneImageOfAlbumView.imageAccesory setAlpha:0.0f];
                [oneImageOfAlbumView.imageAccesory setImage:[UIImage imageWithData:[httpReq responseData]]];
                
                CGContextRef context = UIGraphicsGetCurrentContext();
                [UIView beginAnimations:nil context:context];
                [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
                [UIView setAnimationDuration: 1.0];
                [UIView setAnimationDelegate: self];
                oneImageOfAlbumView.imageAccesory.alpha = 1.0;
                [UIView commitAnimations];
                
            }];
            [httpReq setFailedBlock:^{
                NSError *error = [httpReq error];
                NSLog(@"%@",error.localizedDescription);
            }];
            
            [httpReq startAsynchronous];

            

        }
        else
        {
           
            [oneImageOfAlbumView.imageAccesory setAlpha:0.0f];
            [oneImageOfAlbumView.imageAccesory setImage:[UIImage imageWithData: [NSData dataWithContentsOfURL:urlImage]]];
            
            CGContextRef context = UIGraphicsGetCurrentContext();
            [UIView beginAnimations:nil context:context];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
            [UIView setAnimationDuration: 0.5];
            [UIView setAnimationDelegate: self];
            oneImageOfAlbumView.imageAccesory.alpha = 1.0;
            [UIView commitAnimations];
            
        }
        
        [cell.contentView addSubview:oneImageOfAlbumView];

    }
    
    
    return cell;
    
}

#pragma ProtocolOneImageOnAlbum

-(void)selectOneImageWithIndex:(int)index
{
    _indexOfSelectedPhoto = index;
    [self performSegueWithIdentifier:kShowBigPhotosSegue sender:nil];
}

@end