//
//  DateAndTimeViewController.m
//  BMbook
//
//  Created by iApps Mac Mini on 9/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "DateAndTimeViewController.h"

@interface DateAndTimeViewController ()

@end

@implementation DateAndTimeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=@"תאריך ושעה";
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setUpdateButton:nil];
    [super viewDidUnload];
}

#pragma mark - button methods
-(IBAction)updateButtonPressed:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
