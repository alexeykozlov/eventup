//
//  InviteViewController.h
//  BMbook
//
//  Created by iApps Mac Mini on 9/10/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "SlideOptionViewController.h"
#import <MessageUI/MessageUI.h>
#import <StoreKit/StoreKit.h>
#import <FacebookSDK/FacebookSDK.h>
//#import "Facebook.h"

@interface InviteViewController : SlideOptionViewController <MFMailComposeViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UIButton *emailButton;
@property (strong, nonatomic) IBOutlet UIButton *facebookButton;

@property(strong,nonatomic) NSString* name;
@property(strong,nonatomic) NSString* adress;
@property (strong,nonatomic) NSDate *date;
@property int code;
@property (strong, nonatomic) IBOutlet UIImageView *imgInvitation;
@property (strong, nonatomic) IBOutlet UILabel *lblNoInvitation;

@property (strong, nonatomic) NSMutableDictionary *postParams;

- (IBAction)inviteInEmail:(id)sender;
- (IBAction)inviteInWhatsApp:(id)sender;

@end
