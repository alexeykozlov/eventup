//
//  CellarixVC.m
//  EventUp
//
//  Created by Alexey on 11/5/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "CellarixVC.h"

#define finalUrl @"http://wallettest.cellarix.com/Free/Mobile/RefNumber.aspx"

@interface CellarixVC ()

@end

@implementation CellarixVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //hides the return button
    [self.buttonReturn setHidden:YES];
    self.title=@"Cellarix";
    [self.webView setDelegate:self];
    //customizes back button
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 25.0f, 27.0f)];
    [backButton setImage:[UIImage imageNamed:@"nav_bar_back_btn"]  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(popVC) forControlEvents:UIControlEventTouchUpInside];
    //[self.buttonReturn addTarget:self action:@selector(popVC) forControlEvents:UIControlEventTouchUpInside];
    
    //disables horizontal scroll view
    [[self.webView scrollView] setBounces:NO];
    [self.webView.scrollView setShowsHorizontalScrollIndicator:NO];
    self.webView.scrollView.delegate=self;
    
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    NSURL *url = [NSURL URLWithString:self.url];
    NSLog(@"%@",self.url);
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    [self.webView loadRequest:requestObj];

}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.HUD hide:YES];
    if([webView.request.URL.absoluteString isEqual:finalUrl])
        [self.buttonReturn setHidden:NO];
    //[webView.scrollView setContentSize: CGSizeMake(webView.frame.size.width, webView.scrollView.contentSize.height)];
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    //displays hud
    self.HUD = [[MBProgressHUD alloc] initWithWindow:[UIApplication sharedApplication].keyWindow];
    self.HUD.labelText = @"אנא המתן";
    [self.view.window addSubview:self.HUD];
    [self.HUD show:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
    self.screenName=@"Cellarix screen";
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//dissmisses the vc
- (void) popVC{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - webview scroll methods
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return nil;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.x > 0  ||  scrollView.contentOffset.x < 0 )
        scrollView.contentOffset = CGPointMake(0,scrollView.contentOffset.y);
}

@end
