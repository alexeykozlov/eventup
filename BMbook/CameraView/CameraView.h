//
//  CameraView.h
//  BMbook
//
//  Created by Shoshi V. on 5/16/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CameraView : UIView <UIGestureRecognizerDelegate>
@property (strong ,nonatomic) IBOutlet UIImageView *imageView;
@property (strong ,nonatomic) IBOutlet UIButton *btnBack;


- (IBAction)pinchDetected:(UIPinchGestureRecognizer *)pinchRecognizer;
- (IBAction)rotationDetected:(UIRotationGestureRecognizer *)rotationRecognizer;
- (IBAction)handlePan:(UIPanGestureRecognizer *)recognizer;

@end
