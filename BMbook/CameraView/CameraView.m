//
//  CameraView.m
//  BMbook
//
//  Created by Shoshi V. on 5/16/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "CameraView.h"

@implementation CameraView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
#pragma mark UIGestureRecognizer Delegate Methods

- (void)pinchDetected:(UIPinchGestureRecognizer *)pinchRecognizer
{
    CGFloat scale = pinchRecognizer.scale;
    pinchRecognizer.view.transform = CGAffineTransformScale(pinchRecognizer.view.transform, scale, scale);
    pinchRecognizer.scale = 1.0;
    
}

- (IBAction)rotationDetected:(UIRotationGestureRecognizer *)rotationRecognizer
{
    CGFloat angle = rotationRecognizer.rotation;
    rotationRecognizer.view.transform = CGAffineTransformRotate(rotationRecognizer.view.transform, angle);
    rotationRecognizer.rotation = 0.0;
    
}
- (IBAction)handlePan:(UIPanGestureRecognizer *)recognizer
{
    CGPoint translation = [recognizer translationInView:recognizer.view.superview];
    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,
                                         recognizer.view.center.y + translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:recognizer.view.superview];
    
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
