//
//  UserDetailsViewController.m
//  BMbook
//
//  Created by iApps Mac Mini on 9/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "UserDetailsViewController.h"

@interface UserDetailsViewController ()

@end

@implementation UserDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=@"פרטי התחברות";
    self.Password.placeholder=@"שנה סיסמא";
    self.Password.secureTextEntry=YES;
    self.Password.delegate=self;
    self.Password.textAlignment=NSTextAlignmentCenter;
    self.confirmpassword.placeholder=@"אמת סיסמא";
    self.confirmpassword.secureTextEntry=YES;
    self.confirmpassword.delegate=self;
    self.confirmpassword.textAlignment=NSTextAlignmentCenter;
    self.Code.placeholder=@"קוד האירוע";
    self.Code.text=[[NSUserDefaults standardUserDefaults]stringForKey:kAPIKeyKeyword];
    self.Code.delegate=self;
    self.Code.textAlignment=NSTextAlignmentCenter;
    self.Code.keyboardType = UIKeyboardTypeNumberPad;
    
    //sets the tap recognizer
    UITapGestureRecognizer * recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    recognizer.delegate = self;
    [self.view addGestureRecognizer:recognizer];
    
    //customizes the back button
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 25.0f, 27.0f)];
    [backButton setImage:[UIImage imageNamed:@"nav_bar_back_btn"]  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(popVC) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    //sets number keyboar toolbar
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    [numberToolbar setBackgroundImage:[UIImage imageNamed:@"nav_bar"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
    UIBarButtonItem* cancel= [[UIBarButtonItem alloc]initWithTitle:@"בטל" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)];
    [cancel setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor],  UITextAttributeTextColor,nil] forState:UIControlStateNormal];
    cancel.tintColor=[UIColor whiteColor];
    UIBarButtonItem* done= [[UIBarButtonItem alloc]initWithTitle:@"אשר" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)];
    [done setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor],  UITextAttributeTextColor,nil] forState:UIControlStateNormal];
    done.tintColor=[UIColor whiteColor];
    numberToolbar.items = [NSArray arrayWithObjects:
                           cancel,
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           done,
                           nil];
    [numberToolbar sizeToFit];
    self.Code.inputAccessoryView = numberToolbar;
}

//dissmisses the vc
- (void) popVC{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    self.screenName=@"User details screen";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setPassword:nil];
    [self setConfirmpassword:nil];
    [self setCode:nil];
    [self setUpdateDetailsButton:nil];
    [super viewDidUnload];
}

#pragma mark - text field methods
-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    [textField resignFirstResponder];
    return NO; // We do not want UITextField to insert line-breaks.
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField==self.Code){
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 4) ? NO : YES;
    }
    else
        return YES;
}

#pragma mark - gesture methods
- (void)handleTap:(UITapGestureRecognizer *)recognizer {
    if([self.Code isFirstResponder])
        [self.Code resignFirstResponder];
    else if([self.Password isFirstResponder])
        [self.Password resignFirstResponder];
    else if([self.confirmpassword isFirstResponder])
        [self.confirmpassword resignFirstResponder];
}

#pragma mark - button methods
-(IBAction)updateDetails:(id)sender
{
    if(self.Password.text.length>0 && self.Password.text.length<8)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"שגיאה" message:@"הסיסמא צריכה להכיל לפחות 8 תווים" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else if (![self checkPasswordsWithPassword:self.Password.text andConfirmPassword:self.confirmpassword.text])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"שגיאה" message:@"הססמאות לא תואמות" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    } else if([self checkCode:self.Code.text]==NO)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"שגיאה" message:@"קוד האירוע צריך להיות בן 4 ספרות" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    } else
    {
        //displays hud
        MBProgressHUD * HUD = [[MBProgressHUD alloc] initWithWindow:[UIApplication sharedApplication].keyWindow];
        HUD.labelText = @"מעדכן...";
        [self.view.window addSubview:HUD];
        [HUD show:YES];
        [GeneralManager updateEventDetailsWithSessionId:[[NSUserDefaults standardUserDefaults]stringForKey:KSessionId] andName:[[NSUserDefaults standardUserDefaults]stringForKey:kAPIKeyEventName] andDate:[[NSUserDefaults standardUserDefaults]stringForKey:kAPIKeyDateEvent].intValue andAdress:[[NSUserDefaults standardUserDefaults]stringForKey:kAdress] andAllowGifts:[[NSUserDefaults standardUserDefaults]stringForKey:kAllowGifts].boolValue andEmail:[[NSUserDefaults standardUserDefaults]stringForKey:kUserName] andPassword:self.Password.text andCode:self.Code.text.intValue andSong:[[NSUserDefaults standardUserDefaults]stringForKey:kAPIKeySong]
                                                Success:^{
                                                    [HUD hide:YES];
                                                    [self.navigationController popViewControllerAnimated:YES];
                                                }
                                                failure:^{
                                                    [HUD hide:YES];
                                                }];
    }
    
}

#pragma mark - details validation methods



//checks if the password and the confirm password fields match
-(BOOL) checkPasswordsWithPassword:(NSString*)password andConfirmPassword:(NSString*)confirmPassword
{
    if(password.length==0 && confirmPassword.length==0)
        return YES;
    else if([password isEqual:confirmPassword])
        return YES;
    else
        return NO;
}

//checks that the code is exactly 4 numbers
-(BOOL) checkCode:(NSString*)code
{
    if(code.length<4)
        return NO;
    else
        return YES;
}

#pragma mark - toolbar
-(void)cancelNumberPad{
    [self.Code resignFirstResponder];
    self.Code.text = [[NSUserDefaults standardUserDefaults]stringForKey:kAPIKeyKeyword];
}

-(void)doneWithNumberPad{
    NSString *numberFromTheKeyboard = self.Code.text;
    [self.Code resignFirstResponder];
}

@end
