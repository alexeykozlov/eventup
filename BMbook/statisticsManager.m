//
//  statisticsManager.m
//  BMbook
//
//  Created by iApps Mac Mini on 9/9/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "statisticsManager.h"

@implementation statisticsManager

//increases the number of enteries to an event by one after each entery
+(void) addEntryToEventWithEventId:(int) eventId andKeyword:(int)keyword
                           Success:(void (^)())successBlock
                           failure:(void (^)())failureBlock;
{
    // Create our parameters dictionary
    NSDictionary *params = @{@"event":@(eventId).stringValue,
                             @"keyword":@(keyword).stringValue};
    
    NSString *path = [NSString stringWithFormat:@"%@%@", kBaseAddress, @"api/addEntryToEvent.php"];
    
    // Perform our request
    [[ServiceManager getInstance] handleRequestwithMethod:@"POST" andPath:path parameters:params successBlock:^(NSDictionary *responseDict) {
        
        // --------TODO operations HERE -------- //
        
    } failureBlock:^(AppError *error) {
        
        // Display the right error message to the user
        [error displayErrorMessage];
    }];
}


+(void) getCountOfEntriesToEventWithEventId:(int) eventId andKeyword:(int)keyword
                                    Success:(void (^)(int count))successBlock
                                    failure:(void (^)())failureBlock{
    // Create our parameters dictionary
    NSDictionary *params = @{@"event":@(eventId).stringValue,
                             @"keyword":@(keyword).stringValue};
    
    NSString *path = [NSString stringWithFormat:@"%@%@", kBaseAddress, @"api/getCountOfEntriesToEvent.php"];
    
    // Perform our request
    [[ServiceManager getInstance] handleRequestwithMethod:@"POST" andPath:path parameters:params successBlock:^(NSDictionary *responseDict) {
        
        NSDictionary* dataDict = [responseDict objectForKey:@"data"];
        NSString *count = [dataDict objectForKey:@"entriesCount"];
        successBlock(count.intValue);
        [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"entriesCount"] forKey:kNumberOfEnteries];
    } failureBlock:^(AppError *error) {
        
        // Display the right error message to the user
        [error displayErrorMessage];
        failureBlock();
    }];
}

//returns the number of wishes in the event
+(void) getCountOfWishesToEventWithId:(int)eventId andKeyword:(int)keyword
                              Success:(void (^)(int count))successBlock
                              failure:(void (^)())failureBlock
{
    // Create our parameters dictionary
    NSDictionary *params = @{@"event":@(eventId).stringValue,
                             @"keyword":@(keyword).stringValue};
    
    NSString *path = [NSString stringWithFormat:@"%@%@", kBaseAddress, @"api/getCountOfWishesToEvent.php"];
    
    // Perform our request
    [[ServiceManager getInstance] handleRequestwithMethod:@"POST" andPath:path parameters:params successBlock:^(NSDictionary *responseDict) {
        
        NSDictionary* dataDict = [responseDict objectForKey:@"data"];
        NSString *count = [dataDict objectForKey:@"wishesCount"];
        successBlock(count.intValue);
        
    } failureBlock:^(AppError *error) {
        
        // Display the right error message to the user
        [error displayErrorMessage];
        failureBlock();
    }];
}

+(void) getStatisticsWithEventId:(int) eventId andKeyword:(int)keyword
                         Success:(void (^)(int wishes,int enteries))successBlock
                         failure:(void (^)())failureBlock
{
    // Create our parameters dictionary
    NSDictionary *params = @{@"event":@(eventId).stringValue,
                             @"keyword":@(keyword).stringValue};
    
    NSString *path = [NSString stringWithFormat:@"%@%@", kBaseAddress, @"api/getStatistic.php"];
    
    // Perform our request
    [[ServiceManager getInstance] handleRequestwithMethod:@"POST" andPath:path parameters:params successBlock:^(NSDictionary *responseDict) {
        
        NSDictionary* dataDict = [responseDict objectForKey:@"data"];
        NSString *wishes = [dataDict objectForKey:@"wishesCount"];
        NSString *enteries = [dataDict objectForKey:@"entriesCount"];
        successBlock(wishes.intValue,enteries.intValue);
        
    } failureBlock:^(AppError *error) {
        
        // Display the right error message to the user
        [error displayErrorMessage];
        failureBlock();
    }];
}

@end
