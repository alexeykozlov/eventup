//
//  LogInViewController.m
//  BMbook
//
//  Created by iApps Mac Mini on 9/9/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "LogInViewController.h"

@interface LogInViewController ()

@end

@implementation LogInViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=@"כניסה לאירוע";
    self.scroll.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"BG.png"]];
    //sets the textfields first text
    self.usernameTextfield.placeholder=@"שם משתמש";
    self.usernameTextfield.textAlignment = NSTextAlignmentRight;
    self.usernameTextfield.delegate=self;
    self.passwordTextfield.placeholder=@"סיסמא";
    self.passwordTextfield.textAlignment = NSTextAlignmentRight;
    self.passwordTextfield.secureTextEntry=YES;
    self.passwordTextfield.delegate=self;
    
    //sets the tap recognizer
    UITapGestureRecognizer * recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    recognizer.delegate = self;
    [self.view addGestureRecognizer:recognizer];
    //customizes the back button
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 25.0f, 27.0f)];
    [backButton setImage:[UIImage imageNamed:@"nav_bar_back_btn"]  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(popVC) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
}
//dissmisses the vc
- (void) popVC{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    self.screenName=@"logIn screen";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setRegisterButton:nil];
    [self setLogInButton:nil];
    [self setUsernameTextfield:nil];
    [self setPasswordTextfield:nil];
    [super viewDidUnload];
}

#pragma mark - navigation methods

- (IBAction) createEvent: (id) sender
{
    [self performSegueWithIdentifier:@"signUpSegue" sender:nil];
}

-(IBAction) logInEvent: (id)sender
{
    if(self.passwordTextfield.text.length==0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"שגיאה" message:@"הזן סיסמא" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else
    {
        
        //displays hud
        MBProgressHUD * HUD = [[MBProgressHUD alloc] initWithWindow:[UIApplication sharedApplication].keyWindow];
        HUD.labelText = @"טוען...";
        [self.view.window addSubview:HUD];
        [HUD show:YES];
        [GeneralManager logInWithEmail:self.usernameTextfield.text andPassword:self.passwordTextfield.text Success:^
         {
        
             //save the invitation
             [EventManager getInvitationWithId:[[NSUserDefaults standardUserDefaults] stringForKey:kAPIKeyEventID].intValue
                                  Success:^(NSURL* imageUrl)
              {
                  [HUD hide:YES];
                  NSString *urlString = [imageUrl absoluteString];
                  [[NSUserDefaults standardUserDefaults]setObject:urlString forKey:@"invitationUrl"];
              }
                                       failure:^()
                                    {
                                      [ HUD hide:YES];
                                    }];
             CreatorSlideViewController *slideController;
             slideController = [[CreatorSlideViewController alloc] init];
             slideController.slideViewPaddingRight = 60.0;
             slideController.slideViewPaddingLeft = 0;
             //changes the mode to creator
             [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kIsCreator];
             [self presentViewController:slideController animated:YES completion:nil];
         } failure:^{
             [HUD hide:YES];
             NSLog(@"Fail");
         }];
        //dismisses the keyboard
        if([self.usernameTextfield isFirstResponder])
            [self.usernameTextfield resignFirstResponder];
        else if([self.passwordTextfield isFirstResponder])
            [self.passwordTextfield resignFirstResponder];
        [HUD hide:YES];

    }
}

#pragma mark - text field methods
-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    [textField resignFirstResponder];
    return NO; // We do not want UITextField to insert line-breaks.
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    //sets the scroll view
    [self.scroll setScrollEnabled:YES];
    [self.scroll setContentSize:CGSizeMake(320,720)];
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    if (screenSize.height <= 480.0f)
    {
        [self.scroll setContentOffset:CGPointMake(0,150) animated:YES];
    }
    else
    {
        [self.scroll setContentOffset:CGPointMake(0,50) animated:YES];
        
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
        [self.scroll setContentOffset:CGPointMake(0,0) animated:YES];
        [self.scroll setScrollEnabled:NO];
}

#pragma mark - gesture methods
- (void)handleTap:(UITapGestureRecognizer *)recognizer {
    if([self.usernameTextfield isFirstResponder])
        [self.usernameTextfield resignFirstResponder];
    else if([self.passwordTextfield isFirstResponder])
        [self.passwordTextfield resignFirstResponder];
}

@end
