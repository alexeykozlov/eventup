//
//  statisticsManager.h
//  BMbook
//
//  Created by iApps Mac Mini on 9/9/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface statisticsManager : NSObject
+(void) addEntryToEventWithEventId:(int) eventId andKeyword:(int)keyword
                Success:(void (^)())successBlock
                failure:(void (^)())failureBlock;

+(void) getStatisticsWithEventId:(int) eventId andKeyword:(int)keyword
                Success:(void (^)(int wishes,int enteries))successBlock
                failure:(void (^)())failureBlock;
@end
