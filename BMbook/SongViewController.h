//
//  SongViewController.h
//  EventUp
//
//  Created by Alexey on 10/28/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "SlideOptionViewController.h"
#import "MBProgressHUD.h"

@interface SongViewController : SlideOptionViewController <UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong,nonatomic) MBProgressHUD * HUD;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@end
