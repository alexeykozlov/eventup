//
//  OneWorldLocationVC.h
//  BMbook
//
//  Created by Shoshi V. on 5/20/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WorldLocation.h"
@interface OneWorldLocationVC : UIViewController
@property (nonatomic,strong) WorldLocation *worldLocation;
@property (nonatomic,strong)  UIActivityIndicatorView *indicator;
@property (nonatomic ,weak)IBOutlet UIImageView *imageView;
@property (nonatomic ,weak)IBOutlet UITextView *textView;
@property (nonatomic ,weak)IBOutlet UILabel *lblTitle;

- (IBAction)btnClosePressed:(id)sender;
@end
