//
//  PickerTextField.m
//  IDareU
//
//  Created by iApps Dev. Mac mini on 3/13/13.
//  Copyright (c) 2013 Roee Kattaby. All rights reserved.
//

#import "PickerTextField.h"

@implementation PickerTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)insertDatePickerIntoTextField:(UIDatePicker*)datePicker withActionDelegate:(id<PickerTextFieldDelegate>)controller
{
    // Set the cells delegate
    self.pickerDelegate = controller;
    
    // Create a toolbar for the datepicker
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [toolBar setTintColor:[UIColor blackColor]];
    
    // Create a done UIBarButton
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:kLabelDone style:UIBarButtonItemStyleDone target:self action:@selector(didChoosePickerOption)];
    
    // Create a cancel UIBarButton
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:kLabelCancel style:UIBarButtonItemStyleBordered target:self action:@selector(cancelPicker)];
    
    UIBarButtonItem *fixedspaceBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    // Set fixed space bar button item width
    [fixedspaceBarButton setWidth:180];
    
    // Set the toolbar's items
    [toolBar setItems:@[cancelButton, fixedspaceBarButton, doneButton]];
    
    // Set the datepicker as the cell's input view
    [self setInputAccessoryView:toolBar];
    [self setInputView:datePicker];
}

-(void)insertPickerIntoTextField:(UIPickerView *)pickerView withActionDelegate:(id<PickerTextFieldDelegate>)controller
{
    // Set the cells delegate
    self.pickerDelegate = controller;
    
    // Create a toolbar for the datepicker
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [toolBar setTintColor:[UIColor blackColor]];
    
    // Create a done UIBarButton
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:kLabelDone style:UIBarButtonItemStyleDone target:self action:@selector(didChoosePickerOption)];
    
    // Create a cancel UIBarButton
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:kLabelCancel style:UIBarButtonItemStyleBordered target:self action:@selector(cancelPicker)];
    
    UIBarButtonItem *fixedspaceBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    // Set fixed space bar button item width
    [fixedspaceBarButton setWidth:186];
    
    // Set the toolbar's items
    [toolBar setItems:@[cancelButton, fixedspaceBarButton, doneButton]];
    
    // Set the datepicker as the cell's input view
    [self setInputAccessoryView:toolBar];
    [self setInputView:pickerView];
}

-(void)cancelPicker
{
    // Call our delegate
    [self.pickerDelegate didCancelPicker:self];
}

-(void)didChoosePickerOption
{
    // Check if our picker is a date picker or a pickerview
    if ([self.inputView isKindOfClass:[UIDatePicker class]])
    {
        // Get our date picker
        UIDatePicker *inputPicker = (UIDatePicker*)self.inputView;
        
        // Get our chosen date
        NSDate *chosenDate = inputPicker.date;
        
        // Invoke our delegate
        [self.pickerDelegate didSelectDatePickerOption:self chosenDate:chosenDate];
    }
    else if ([self.inputView isKindOfClass:[UIPickerView class]])
    {
        // Get our pickerview
        UIPickerView *pickerView = (UIPickerView*)self.inputView;
        
        // Get our chosen index
        NSInteger chosenIndex = [pickerView selectedRowInComponent:0];
                
        // Get our value for chosen key in the dictionary
        NSNumber *value = self.keysDict[self.optionsArray[chosenIndex]];
        
        // Return our value from the dictionary
        [self.pickerDelegate didSelectPickerOption:self chosenIndex:chosenIndex chosenKey:value];
    }
}

#pragma mark - UIPickerViewDataSource methods

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    // Return the number of options
    return self.optionsArray.count;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

#pragma mark - UIPickerView Delegate methods

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    // Get a reuseable view
    UILabel *tView = (UILabel*)view;
    
    if (!tView)
    {
        // Allocate a new label
        tView = [[UILabel alloc] init];
        
        // Set line break
        [tView setNumberOfLines:0];
        
        [tView setFont:[UIFont fontWithName:@"Arial-BoldMT" size:17]];
        
        // Align our text to the center
        [tView setTextAlignment:NSTextAlignmentCenter];
        
        // Clear the labels background color
        [tView setBackgroundColor:[UIColor clearColor]];
    }
    
    // Set the labels text
    [tView setText:self.optionsArray[row]];
    
    // Return the label
    return tView;
}

//-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
//{
//    // Return our option in our array
//    return self.optionsArray[row];
//}

@end
