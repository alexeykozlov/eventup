//
//  CustomAnnotationView.h
//  BMbook
//
//  Created by Shoshi V. on 5/20/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "SMCalloutView.h"
#import <QuartzCore/QuartzCore.h>
#import "WorldLocation.h"
#import "CustomButton.h"
@interface CustomAnnotationView : MKAnnotationView
@property (nonatomic, strong)  UIView* vCustomAnnotation;
@property (nonatomic, strong)  UIImageView* imgUser;
//@property (nonatomic, strong)  WorldLocation *worldLocation;
@property (nonatomic, strong)  UIButton *btn;
//@property (nonatomic, strong) IBOutlet UIButton* btn;
//@property (strong, nonatomic) SMCalloutView *calloutView;
@end
