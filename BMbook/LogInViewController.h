//
//  LogInViewController.h
//  BMbook
//
//  Created by iApps Mac Mini on 9/9/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface LogInViewController : GAITrackedViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *usernameTextfield;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextfield;


@property (strong, nonatomic) IBOutlet UIButton *logInButton;
@property (strong, nonatomic) IBOutlet UIButton *registerButton;
@property (strong, nonatomic) IBOutlet UIScrollView *scroll;

- (IBAction) createEvent: (id) sender;
-(IBAction) logInEvent: (id)sender;


@end
