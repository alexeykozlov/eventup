//
//  EventManager.m
//  BMbook
//
//  Created by iApps Mac Mini on 9/9/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "EventManager.h"

@implementation EventManager
//uploads an invitation image
+(void) addInvitationWithId:(int)eventId andKeyword:(int)keyword andImage:(NSData*)image
                  Success:(void (^)())successBlock
                  failure:(void (^)())failureBlock
{
    NSString *path = [NSString stringWithFormat:@"%@%@", kBaseAddress, @"api/addInvitation.php"];
 
    // Create our parameters dictionary
    NSDictionary *params = @{@"event":@(eventId).stringValue,
                             @"keyword":@(keyword).stringValue};
    
    MultiPartFile* imageFile = [[MultiPartFile alloc] initWithServerKey:@"img" andData:image andFileName:@"picture" andMimeType:MimeTypeImagePNG];
    
    [[ServiceManager getInstance] handleMultiPartRequestWithMethod:@"POST" andPath:path parameters:params andMultiPartFiles:@[imageFile] successBlock:^(NSDictionary *responseDict) {
        
        successBlock();
        
    } failureBlock:^(AppError *error) {
        [error displayErrorMessage];
    }];
}

//gets the invitation image from the server
+(void) getInvitationWithId:(int)eventId
                    Success:(void (^)(NSURL* imageUrl))successBlock
                    failure:(void (^)())failureBlock
{
    // Create our parameters dictionary
    NSDictionary *params = @{@"event":@(eventId).stringValue};
    
    NSString *path = [NSString stringWithFormat:@"%@%@", kBaseAddress, @"api/getInvitation.php"];
    
    // Perform our request
    [[ServiceManager getInstance] handleRequestwithMethod:@"POST" andPath:path parameters:params successBlock:^(NSDictionary *responseDict) {
        NSDictionary* dataDict = [responseDict objectForKey:@"data"];
        @try {
            NSURL *imageUrl = [NSURL URLWithString:[dataDict objectForKey:@"img"]];
            successBlock(imageUrl);
        }
        @catch (NSException *exception) {
            successBlock(NULL);
        }
        @finally {
            
        }
        
        
        
    } failureBlock:^(AppError *error) {
        
        // Display the right error message to the user
        [error displayErrorMessage];
    }];
}

//adds a wish to the event
+(void) addWishWithId:(int)eventID andKeyword:(int)keyword andAuthorName:(NSString*)authorName andText:(NSString*)text andImage:(NSData*)image
              Success:(void (^)())successBlock
              failure:(void (^)())failureBlock
{
    // Create our parameters dictionary
    NSDictionary *params = @{@"event":@(eventID).stringValue,
                             @"keyword":@(keyword).stringValue,
                             @"authorName":authorName,
                             @"text":text};
    
    NSString *path = [NSString stringWithFormat:@"%@%@", kBaseAddress, @"api/addWish.php"];
    
    MultiPartFile* imageFile = [[MultiPartFile alloc] initWithServerKey:@"img" andData:image andFileName:@"picture" andMimeType:MimeTypeImagePNG];
    
    // Perform our request
    [[ServiceManager getInstance] handleMultiPartRequestWithMethod:@"POST" andPath:path parameters:params andMultiPartFiles:@[imageFile]
    successBlock:^(NSDictionary *responseDict)  {
        
        successBlock();
        
    } failureBlock:^(AppError *error) {
        
        // Display the right error message to the user
        [error displayErrorMessage];
        
        failureBlock();
    }];
}

//gets the wishes from the server
+(void) getWishesWithId:(int)eventId
                Success:(void (^)(NSArray * arrWishes))successBlock
                failure:(void (^)())failureBlock
{
    // Create our parameters dictionary
    NSDictionary *params = @{@"event":@(eventId).stringValue};
    
    NSString *path = [NSString stringWithFormat:@"%@%@", kBaseAddress, @"api/getWishes.php"];
    
    // Perform our request
    [[ServiceManager getInstance] handleRequestwithMethod:@"POST" andPath:path parameters:params successBlock:^(NSDictionary *responseDict) {
        
    NSArray * arrWishes =[Wish wishesFromArray:[responseDict objectForKey:@"data"]];
    successBlock(arrWishes);
    } failureBlock:^(AppError *error) {
        
        // Display the right error message to the user
        [error displayErrorMessage];
    }];
}

//deletes a wish
+(void) deleteWishWithID:(int)eventId andKeyword:(int)keyword andWishId:(int)wishId
                Success:(void (^)())successBlock
                failure:(void (^)())failureBlock
{
    // Create our parameters dictionary
    NSDictionary *params = @{@"event":@(eventId).stringValue,
                             @"keyword":@(keyword).stringValue,
                             @"wishID":@(wishId).stringValue};
    
    NSString *path = [NSString stringWithFormat:@"%@%@", kBaseAddress, @"api/deleteWish.php"];
    
    // Perform our request
    [[ServiceManager getInstance] handleRequestwithMethod:@"POST" andPath:path parameters:params successBlock:^(NSDictionary *responseDict) {
        
        successBlock();
        
    } failureBlock:^(AppError *error) {
        
        // Display the right error message to the user
        [error displayErrorMessage];
        
        failureBlock();
    }];
}





@end
