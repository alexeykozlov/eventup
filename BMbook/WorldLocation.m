//
//  WorldLocation.m
//  BMbook
//
//  Created by Shoshi V. on 5/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "WorldLocation.h"

@implementation WorldLocation

- (id)initWithDictioanry:(NSDictionary *)dictionary
{
    self = [super init];
    if (self)
    {
        self.nID = [dictionary[@"id"] intValue];
        self.fLat = [dictionary[@"lat"] floatValue];
        self.fLon = [dictionary[@"lng"] floatValue];
        self.strTitle = dictionary[@"title"];
        self.strDesc = dictionary[@"desc"];
        self.urlImage = [NSURL URLWithString: dictionary[@"pic"]];
        self.urlSmallImage = [NSURL URLWithString: dictionary[@"thumb"]];
    }
    return self;
}

+ (WorldLocation *)worldLocationWithDictionary:(NSDictionary *)dictionary
{
    // Create a WorldLocation using the initialize with dictionary method.
    WorldLocation *newWorldLocation= [[WorldLocation alloc] initWithDictioanry:dictionary];
    
    return newWorldLocation;
}
+ (NSArray *)worldLocationsFromArray:(NSArray *)array
{
    // Create the array WorldLocations
    NSMutableArray *arrWorldLocations = [NSMutableArray arrayWithCapacity:array.count];
    
    // For each of the dictionary wishe create an object and add it to the array.
    for (NSDictionary *dicWorldLocation in array)
    {
        WorldLocation *event = [WorldLocation worldLocationWithDictionary:dicWorldLocation];
        [arrWorldLocations  addObject:event];
    }
    
    return arrWorldLocations;

}
@end
