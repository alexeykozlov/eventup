//
//  SendGiftVC.m
//  EventUp
//
//  Created by Alexey on 11/4/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "SendGiftViewController.h"
#import "AFHTTPClient.h"

#define textViewLength 90

static NSString *const BaseURLString = @"http://wallettest.cellarix.com/Interfaces/MobilePaymentsInterface.asmx/";
static NSString *const tokenRequestUrl = @"http://wallettest.cellarix.com/Free/Mobile/order.aspx?OrderToken=";

@interface SendGiftVC ()

@end

@implementation SendGiftViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.title=@"שלח מתנה";
    self.scroll.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"BG.png"]];
    //user wants to recieve gifts
	if([[NSUserDefaults standardUserDefaults]stringForKey:kAllowGifts].boolValue)
    {
        [self.lblNoGift setHidden:YES];
        self.textFieldSum.placeholder=@"סכום";
        self.textFieldSum.textAlignment=NSTextAlignmentCenter;
        self.textFieldSum.delegate=self;
        self.textFieldSum.keyboardType = UIKeyboardTypeNumberPad;
        
        self.textViewBless.textAlignment=NSTextAlignmentRight;
        self.textViewBless.delegate=self;
        
        //sets number keyboar toolbar
        UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
        numberToolbar.barStyle = UIBarStyleBlackTranslucent;
        [numberToolbar setBackgroundImage:[UIImage imageNamed:@"nav_bar"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
        UIBarButtonItem* cancel= [[UIBarButtonItem alloc]initWithTitle:@"בטל" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)];
        [cancel setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor],  UITextAttributeTextColor,nil] forState:UIControlStateNormal];
        cancel.tintColor=[UIColor whiteColor];
        UIBarButtonItem* done= [[UIBarButtonItem alloc]initWithTitle:@"אשר" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)];
        [done setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor],  UITextAttributeTextColor,nil] forState:UIControlStateNormal];
        done.tintColor=[UIColor whiteColor];
        numberToolbar.items = [NSArray arrayWithObjects:
                               cancel,
                               [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                               done,
                               nil];
        [numberToolbar sizeToFit];
        self.textFieldSum.inputAccessoryView = numberToolbar;
        
        //sets the tap recognizer
        UITapGestureRecognizer * recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        recognizer.delegate = self;
        [self.view addGestureRecognizer:recognizer];
    }
    //no gifts
    else
    {
        [self.textFieldSum setHidden:YES];
        [self.textViewBless setHidden:YES];
        [self.lblAddGift setHidden:YES];
        [self.buttonSend setHidden:YES];
    }
    
    //set date
    self.lblDate.text =[self returnStringOfStatusEvent];
    //sets the date color
    self.lblDate.textColor=[UIColor colorWithRed:(255/255.0) green:(220/255.0) blue:(90/255.0) alpha:1];
}

-(void) viewDidAppear:(BOOL)animated
{
    self.screenName=@"Send gift screen";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//brings up alert when send is pressed
-(IBAction)sendPressed:(id)sender
{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"אישור"
                                                      message:@"לידיעתך: שירות שליחת המתנה כרוך בעמלה של 1 שח.\n הערה:\n בכל בעיה בהעברת הכספים יש לפנות ל-Cellarix \n www.cellarix.com"
                                                     delegate:self
                                            cancelButtonTitle:nil
                                            otherButtonTitles:@"תודה", nil];
    [message show];
}

//sets the buttons of the alert
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"לא"])
    {
        
    }
    else if([title isEqualToString:@"כן"])
    {
        [self sendAction];
    }
    
}

-(void) sendAction
{
    //checks if a sum was entered
    if(self.textFieldSum.text.length==0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"שגיאה" message:@"הזן סכום לשליחה" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else
    {
        //hides the keyboard if its open
        if([self.textFieldSum isFirstResponder])
            [self.textFieldSum resignFirstResponder];
        else if([self.textViewBless isFirstResponder])
            [self.textViewBless resignFirstResponder];
        
        AFHTTPClient* httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:BaseURLString]];
        
        NSString* keyString = @"<Key><KeyObj><PhoneNumber>test</PhoneNumber><SecureKey>Jr4YMnoDOxQ-QAHsT8v8VZW1rr3l-bGM</SecureKey></KeyObj></Key>";
        
        NSString *phoneNumber=[[NSUserDefaults standardUserDefaults]stringForKey:kAPIKeyPhone];
        
        NSString* orderXML = [NSString stringWithFormat:@"<MoneyTransfer><Amount>%d</Amount><PhoneNumber>%@</PhoneNumber><Message>%@</Message><ServiceId>10</ServiceId><sDealId>0</sDealId><SupplierURL>http://www.supplierSite.com</SupplierURL></MoneyTransfer>",self.textFieldSum.text.intValue,phoneNumber,self.textViewBless.text];
        
        //displays hud
        MBProgressHUD * HUD = [[MBProgressHUD alloc] initWithWindow:[UIApplication sharedApplication].keyWindow];
        HUD.labelText = @"אנא המתן";
        [self.view.window addSubview:HUD];
        [HUD show:YES];
        
        
         [httpClient postPath:@"OrderParams" parameters:@{@"sKey": keyString,
         @"orderXML": orderXML} success:^(AFHTTPRequestOperation *operation, id responseObject)
        {
             [HUD hide:YES];
         
             NSError* error;
             //fixes the returned xml format
             NSString *strXML= [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             strXML = [strXML stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
             strXML = [strXML stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
             NSData *dataXML = [strXML dataUsingEncoding:NSUTF8StringEncoding ];
             GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:dataXML options:0 error:&error];
         
            if (error)
            {
                [HUD hide:YES];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"שגיאה" message:@"בעיה בביצוע הפעולה, נסו שנית מאוחר יותר" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alert show];
            }
            else
            {
             
                //gets the data from the xml
                NSArray* resultArray = [doc.rootElement elementsForName:@"Result"];
                for (GDataXMLElement *oneResult in resultArray)
                {
                    GDataXMLElement *status = [oneResult elementsForName:@"Status"][0];
                    GDataXMLElement *number = [status elementsForName:@"Number"][0];
                    GDataXMLElement *message = [status elementsForName:@"Message"][0];
                    GDataXMLElement *data = [oneResult elementsForName:@"Data"][0];
         
                    if(number.stringValue.intValue<0){
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"שגיאה" message:@"בעיה בביצוע הפעולה, נסו שנית מאוחר יותר" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                        [alert show];
                    }
                    else
                    {
                        NSString *fullURL = [NSString stringWithFormat:@"%@%@",tokenRequestUrl,message.stringValue];
                        [self performSegueWithIdentifier:kCellarix sender:fullURL];
                    }
                }
        }
         
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
        }];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kCellarix])
    {
        CellarixVC* cellarixVC = segue.destinationViewController;
        //[cellarixVC setUrl:sender];
        cellarixVC.url=sender;
    }
}


#pragma mark - text field methods


-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    [textField resignFirstResponder];
    return NO; // We do not want UITextField to insert line-breaks.
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    if (screenSize.height <= 480.0f)
    {
        [self.scroll setContentOffset:CGPointMake(0,231) animated:YES];
        [self.scroll setScrollEnabled:NO];
    }
    else
    {
        [self.scroll setContentOffset:CGPointMake(0,250) animated:YES];
        [self.scroll setScrollEnabled:NO];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.scroll setContentOffset:CGPointMake(0,0) animated:YES];
    [self.scroll setScrollEnabled:NO];
    if(![self.textFieldSum.text isEqual:@""])
        self.textFieldSum.text=[NSString stringWithFormat:@"%d %@",([self.textFieldSum text].intValue),@"ש\"ח"];
}


#pragma mark - textView
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    if (screenSize.height <= 480.0f)
    {
        [self.scroll setContentOffset:CGPointMake(0,220) animated:YES];
        [self.scroll setScrollEnabled:NO];
    }
    else
    {
        [self.scroll setContentOffset:CGPointMake(0,250) animated:YES];
        [self.scroll setScrollEnabled:NO];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    if(textView.text.length>textViewLength && range.length==0)
        return NO;
    
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    [self.scroll setContentOffset:CGPointMake(0,0) animated:YES];
    [self.scroll setScrollEnabled:NO];
}


#pragma mark - gesture methods
- (void)handleTap:(UITapGestureRecognizer *)recognizer {
    if([self.textFieldSum isFirstResponder])
        [self.textFieldSum resignFirstResponder];
    else if([self.textViewBless isFirstResponder])
        [self.textViewBless resignFirstResponder];
    
}

#pragma mark - toolbar 
-(void)cancelNumberPad{
    [self.textFieldSum resignFirstResponder];
    self.textFieldSum.text = @"";
}

-(void)doneWithNumberPad{
    NSString *numberFromTheKeyboard = self.textFieldSum.text;
    [self.textFieldSum resignFirstResponder];
}


@end
