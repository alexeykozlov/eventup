//
//  MenagerService.h
//  BMbook
//
//  Created by Shoshi V. on 5/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServiceManager.h"
#import "Event.h"
#import "Wish.h"
#import "Globals.h"
#import "WorldLocation.h"
#import "ServiceManager.h"
#import "AppError.h"
@interface ManagerService : NSObject

- (void)getAllEventsSuccess:(void (^)(NSArray * arrEvents))successBlock
failure:(void (^)())failureBlock;

- (void)getWishesForEvent:(int )event
                    Success:(void (^)(NSArray * arrWishes))successBlock
                    failure:(void (^)())failureBlock;

- (void)getWorldLocationsForEvent:(int )event
                  Success:(void (^)(NSArray * arrWorldLocations))successBlock
                  failure:(void (^)())failureBlock;

- (void)addWishWithInfo:(NSDictionary *)dicInfo andFile:(NSData*)file
                Success:(void (^)())successBlock
                failure:(void (^)())failureBlock;
@end
