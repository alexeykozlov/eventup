//
//  NavigateMeVC.h
//  BMbook
//
//  Created by Shoshi V. on 5/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideOptionViewController.h"

@interface NavigateMeVC : SlideOptionViewController

@property (nonatomic ,weak) IBOutlet UIImageView *imageVTypeMapUp;
@property (nonatomic ,weak) IBOutlet UIView *viewUp;
@property (nonatomic ,weak) IBOutlet UILabel *lblTakeMeUp;
@property (nonatomic ,weak) IBOutlet UILabel *lblLocationUp;
@property (nonatomic ,weak)IBOutlet UILabel *lblDate;
@property (nonatomic ,strong)NSArray *arrLocation;

- (IBAction)takeMe:(id)sender;
@end
