//
//  MainViewController.h
//  BiBi
//
//  Created by Shoshi V. on 2/21/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomCellMain.h"
#import "Protocols.h"
#import "GiveBlessingViewController.h"
#import "ShowAlbumBlessingsVC.h"
#import "JonatanInWorldVC.h"
#import "NavigateMeVC.h"
#import "ShowInvitationVC.h"
#import "SlideOptionViewController.h"
#import "CustomCellOnlyText.h"
#import "CustomCellWithSymbol.h"
#import "InfoVC.h"
#import "FirstViewController.h"
#import "SendGiftViewController.h"

@interface MainViewController : UITableViewController <UIAlertViewDelegate>

@property (nonatomic,assign) id <ProtocolMainViewConroller> delegate;
@property int _selectedRow;

@end
