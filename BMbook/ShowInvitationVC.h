//
//  ShowInvitationVC.h
//  BMbook
//
//  Created by Shoshi V. on 5/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideOptionViewController.h"
#import "InstructionsView.h"

@interface ShowInvitationVC : SlideOptionViewController <MBProgressHUDDelegate ,UIWebViewDelegate,UIScrollViewDelegate,InstructionsViewDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *imgInvitation;
@property (strong, nonatomic) IBOutlet UILabel *labelNoInvitation;
@property (nonatomic ,strong) InstructionsView *instructionsView;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@end
