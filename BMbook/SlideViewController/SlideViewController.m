//
//  SlideViewController.m
//  BiBi
//
//  Created by Shoshi V. on 2/21/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "SlideViewController.h"

@interface SlideViewController ()

{
    
}
@end

@implementation SlideViewController

-(id)init
{
    if([super init])
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle: nil];
        
        self.mainVC = [mainStoryboard instantiateViewControllerWithIdentifier:kIdentifierMainMenuVC];
        self.mainVC.delegate = self;
        
        
        self.showInvitationVC = [mainStoryboard instantiateViewControllerWithIdentifier:kIdentifierShowInvitationVC];
        self.showInvitationVC.delegate = self;
        
        
        self.nav = [[UINavigationController alloc]  initWithNavigationBarClass:[UINavigationBar class] toolbarClass:[UIToolbar class]];
        [self.nav pushViewController:self.showInvitationVC animated:NO];
        
//        // Load the initial view controller.
//        InitialViewController *initialController = [mainStoryboard instantiateViewControllerWithIdentifier:@"InitialViewControllerID"];
//        [self.nav pushViewController:initialController animated:NO];
        
        //sets image background
        [self.nav.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bar"] forBarMetrics:UIBarMetricsDefault];
        self.staticViewController = self.mainVC;
        self.slidingViewController = self.nav;
        [self btnSlidepressed:nil];
        
        //sets navigation bar color
        self.nav.navigationBar.titleTextAttributes = @{UITextAttributeTextColor : [UIColor blackColor], UITextAttributeTextShadowColor: [UIColor blackColor]};
    }
    return self;
}
- (void)playButtonsOfSlidingView:(BOOL)play
{
    if ([self.nav.visibleViewController isKindOfClass:[JonatanInWorldVC class]])
    {
        [self allowInteractiveSlideing:!play];
    }
}

-(void)btnSlidepressed:(id)sender
{
    if (isStaticViewVisible)
    {
        if (sender == nil)
        {
            [self showSlidingViewAnimated:NO];
        }
        else
        {
            [self showSlidingViewAnimated:YES];
        }
    }
    else
    {
        if (sender == nil)
        {
            [self hideSlidingViewAnimated:NO];
        }
        else
        {
            [self hideSlidingViewAnimated:YES];
        }

    }
}
#pragma mark - ProtocolMainViewConroller
-(void)selectOneOption:(UIViewController *)viewController
{
    ((SlideOptionViewController *)viewController).delegate = self;
    [self.nav popToRootViewControllerAnimated:NO];
    [self.nav pushViewController:viewController animated:NO];
    if (isStaticViewVisible)
    {
        [self showSlidingViewAnimated:NO];
    }
    else
    {
        [self hideSlidingViewAnimated:NO];
    }
    
}
-(void)pushFirstViewController
{
     [self btnSlidepressed:nil];
    [self.nav popToRootViewControllerAnimated:NO];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle: nil];
    FirstViewController *firstVC = [mainStoryboard instantiateViewControllerWithIdentifier:kIdentifierFirstVC];
   
    self.allowInteractiveSlideing = FALSE;
    firstVC.delegate = self;
    [self.nav pushViewController:firstVC animated:NO];
}
#pragma mark - ProtocolSlide
- (void)allowInteractiveSlideing:(BOOL)allow
{
    self.allowInteractiveSlideing = allow;

}
-(void)showMainMenu:(id)sender
{
    [self allowInteractiveSlideing:YES];
    [self btnSlidepressed:sender];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
//    if ([((UINavigationController *)self.slidingViewController).visibleViewController isKindOfClass:[OpenURLViewController class]])
//    {
//        return YES;
//    }
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
//    if ([((UINavigationController *)self.slidingViewController).visibleViewController isKindOfClass:[OpenURLViewController class]])
//    {
//        return YES;
//    }
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
//    if ([((UINavigationController *)self.slidingViewController).visibleViewController isKindOfClass:[OpenURLViewController class]])
//    {
//        return UIInterfaceOrientationMaskAllButUpsideDown;
//    }
    return UIInterfaceOrientationMaskPortrait;
}

@end
