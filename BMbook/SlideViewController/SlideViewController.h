//
//  SlideViewController.h
//  BiBi
//
//  Created by Shoshi V. on 2/21/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CHSlideController.h"
#import "Protocols.h"
#import "GiveBlessingViewController.h"
#import "MainViewController.h"
#import "CustomNavigationBar.h"
#import "FirstViewController.h"
#import "JonatanInWorldVC.h"

@interface SlideViewController : CHSlideController <ProtocolMainViewConroller, ProtocolSlide>

@property (nonatomic ,strong) ShowInvitationVC *showInvitationVC;
@property (nonatomic ,strong) MainViewController *mainVC;
@property (nonatomic ,strong) UINavigationController *nav;
@end
