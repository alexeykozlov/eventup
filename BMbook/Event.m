//
//  Event.m
//  BMbook
//
//  Created by Shoshi V. on 5/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "Event.h"

@implementation Event
- (id)initWithDictioanry:(NSDictionary *)dictionary
{
    self = [super init];
    if (self)
    {
        self.nID = [dictionary[@"id"]intValue];
        self.strName = dictionary[@"name"];
        self.dateEvent = [dictionary[@"date"]intValue];
        self.strAdress = dictionary[@"address"];
        self.bAllowGifts = [dictionary[@"allowGifts"]intValue];
        self.strEmail = dictionary[@"email"];
        self.strCode = dictionary[@"keyword"];
        self.nPhone = dictionary[@"phone"];
        self.song = dictionary[@"song"];
    }
    return self;
}
+ (Event *)eventWithDictionary:(NSDictionary *)dictionary
{
    // Create a Event using the initialize with dictionary method.
    Event *newEvent = [[Event alloc] initWithDictioanry:dictionary];
    
    return newEvent;
}
+ (NSArray *)eventsFromArray:(NSArray *)array;
{
    // Create the array that will hold our Histories.
    NSMutableArray *arrEvents = [NSMutableArray arrayWithCapacity:array.count];
    
    // For each of the dictionary History create an object and add it to the array.
    for (NSDictionary *dicEvent in array)
    {
        Event *event = [Event eventWithDictionary:dicEvent];
        [arrEvents addObject:event];
    }
    
    return arrEvents;
}
@end
