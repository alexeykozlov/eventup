//
//  SendGiftVC.m
//  EventUp
//
//  Created by Alexey on 11/4/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "SendGiftVC.h"
#import "AFHTTPClient.h"
static NSString *const BaseURLString = @"http://wallettest.cellarix.com/Interfaces/MobilePaymentsInterface.asmx/";
static NSString *const tokenRequestUrl = @"http://wallettest.cellarix.com/Free/Mobile/order.aspx?OrderToken=";

@interface SendGiftVC ()

@end

@implementation SendGiftVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.title=@"שלח מתנה";
    
    //user wants to recieve gifts
	if([[NSUserDefaults standardUserDefaults]stringForKey:kAllowGifts].boolValue)
    {
        [self.lblNoGift setHidden:YES];
        self.textFieldSum.placeholder=@"סכום";
        self.textFieldSum.textAlignment=NSTextAlignmentRight;
        self.textFieldSum.delegate=self;
        self.textFieldSum.keyboardType = UIKeyboardTypeNumberPad;
        self.textViewBless.textAlignment=NSTextAlignmentRight;
        self.textViewBless.delegate=self;
        //sets the tap recognizer
        UITapGestureRecognizer * recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        recognizer.delegate = self;
        [self.view addGestureRecognizer:recognizer];
    }
    //no gifts
    else
    {
        [self.textFieldSum setHidden:YES];
        [self.textViewBless setHidden:YES];
        [self.lblAddGift setHidden:YES];
        [self.buttonSend setHidden:YES];
    }
}

-(void) viewDidAppear:(BOOL)animated
{
    self.screenName=@"Send gift screen";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction) call: (id)sender
{
    //checks if a sum was entered
    if(self.textFieldSum.text.length==0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"שגיאה" message:@"הזן סכום לשליחה" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else
    {
        AFHTTPClient* httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:BaseURLString]];
    
        NSString* keyString = @"<Key><KeyObj><PhoneNumber>test</PhoneNumber><SecureKey>Jr4YMnoDOxQ-QAHsT8v8VZW1rr3l-bGM</SecureKey></KeyObj></Key>";
    
        NSString* orderXML = [NSString stringWithFormat:@"<MoneyTransfer><Amount>%f</Amount><PhoneNumber>%@</PhoneNumber><Message>%@</Message><ServiceId>10</ServiceId><sDealId>0</sDealId><SupplierURL>http://www.supplierSite.com</SupplierURL></MoneyTransfer>",self.textFieldSum.text.doubleValue,self.textViewBless.text,@"0544444444"];
/*
        [httpClient postPath:@"OrderParams" parameters:@{@"sKey": keyString,
                                                         @"orderXML": orderXML} success:^(AFHTTPRequestOperation *operation, id responseObject) {
    
            
            NSError* error;
            //fixes the returned xml format
            NSString *strXML= [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            strXML = [strXML stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
            strXML = [strXML stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
            
            NSData *dataXML = [strXML dataUsingEncoding:NSUTF8StringEncoding ];
            GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:dataXML options:0 error:&error];
            
            //gets the data from the xml
            NSArray* resultArray = [doc.rootElement elementsForName:@"Result"];
            for (GDataXMLElement *oneResult in resultArray)
            {
                GDataXMLElement *status = [oneResult elementsForName:@"Status"][0];
                GDataXMLElement *number = [status elementsForName:@"Number"][0];
                GDataXMLElement *message = [status elementsForName:@"Message"][0];
                GDataXMLElement *data = [oneResult elementsForName:@"Data"][0];
                GDataXMLElement *token = [data elementsForName:@"Token"][0];
                if(number.stringValue.intValue<0){
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"שגיאה" message:@"בעיה בביצוע הפעולה, נסו שנית מאוחר יותר" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alert show];
                }
                else
                {
//                    NSString *fullURL = [NSString stringWithFormat:tokenRequestUrl,token.stringValue];
//                    NSURL *url = [NSURL URLWithString:fullURL];
//                    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
//                    [self.webView loadRequest:requestObj];
                }
            }
            
            
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
            }];*/
    }
}

#pragma mark - text field methods


-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    [textField resignFirstResponder];
    return NO; // We do not want UITextField to insert line-breaks.
}

#pragma mark - textView
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    if (screenSize.height <= 480.0f)
    {
        [self.scroll setContentOffset:CGPointMake(0,220) animated:YES];
        [self.scroll setScrollEnabled:NO];
    }
    else
    {
        [self.scroll setContentOffset:CGPointMake(0,225) animated:YES];
        [self.scroll setScrollEnabled:NO];
    }
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    [self.scroll setContentOffset:CGPointMake(0,0) animated:YES];
    [self.scroll setScrollEnabled:NO];
}

#pragma mark - gesture methods
- (void)handleTap:(UITapGestureRecognizer *)recognizer {
    if([self.textFieldSum isFirstResponder])
        [self.textFieldSum resignFirstResponder];
    else if([self.textViewBless isFirstResponder])
        [self.textViewBless resignFirstResponder];
   
}


@end
