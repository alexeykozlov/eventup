//
//  GlobalValuesInstrucions.h
//  InstructionsFramework
//
//  Created by Shoshi V. on 6/11/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#define kNumPages                   6
#define kImageBackGround            @"HTU_Background"
#define kWidthInstuctionsView       260.0f
#define kHeightInstuctionsView      280.0f


//Values of Image View
#define kWidthImageView              130.0f 
#define kHeightImageView             130.0f
#define kLocationXImageView          65.0f
#define kLocationYImageView          30.0f

//Values of Text View
#define kWidthTextView               200.0f
#define kHeightTextView              90.0f
#define kLocationXTextView           30.0f
#define kLocationYTextView           160.0f
#define kFontTextView                [UIFont boldSystemFontOfSize:15]

//Values of Title
#define kWidthLable                  200.0f
#define kHeightLable                 30.0f
#define kLocationXLable              30.0f
#define kLocationYLable              0.0f
#define kFontLable                  [UIFont boldSystemFontOfSize:17]
#define kNumberOfLineInLable         1


//Values of Button
#define kImageBackGroundViewButton     @"main_bg"
#define kColorBackGroundViewButton     [UIColor clearColor]
#define kMarginWidthViewButton          10.0f
#define kMarginHightViewButton          5.0f
#define kSpaceBetween2LineButtons       10.0f