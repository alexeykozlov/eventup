//
//  ExceptionHandler.h
//  Assembles
//
//  Created by Roee Kattaby on 5/22/13.
//  Copyright (c) 2013 iapps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define kURLWebServiceLogClientException @"http://iappsdev.mobi/App_Log/api/logClientException.php"

@interface ExceptionHandler : NSObject

+ (void)setupExceptionHandling;

@end
