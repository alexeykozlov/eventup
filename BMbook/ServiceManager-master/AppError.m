//
//  AppError.m
//  iSpotARC
//
//  Created by Roee Kattaby on 4/18/13.
//  Copyright (c) 2013 iApps. All rights reserved.
//

#import "AppError.h"

@implementation AppError

static NSDictionary* messagesDict;

-(id)initWithErrorCode:(int)errorCode
{
    self = [super init];
    
    if (self)
    {
        // Set the error's properties
        [self setErrorCode:errorCode];
        
        // Set error description's according to error type
        NSNumber* errorNum = @(errorCode);
        [self setErrorDescription:messagesDict[[errorNum stringValue]]];
    }
    
    return self;
}

+ (void)loadErrorsFromPList:(NSString*)fileName
{   
    // Load our plist file
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:@"plist"];
    messagesDict = [[NSDictionary alloc]initWithContentsOfFile:path];
}

- (void)displayErrorMessage
{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:kErrorTitle message:self.errorDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    [alert show];
}

@end
