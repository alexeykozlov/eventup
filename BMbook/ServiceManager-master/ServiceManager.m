//
//  ServiceManager.m
//  IDareU
//
//  Created by Roee Kattaby on 2/26/13.
//  Copyright (c) 2013 Roee Kattaby. All rights reserved.
//

#import "ServiceManager.h"

@implementation ServiceManager

static ServiceManager* sharedInstance;

+ (ServiceManager*)getInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ServiceManager alloc] init];
    });
    
    return sharedInstance;
}

- (NSDictionary*)validateResponse:(NSData*)responseData appError:(AppError**)error
{
    NSError *errorParse;
    
    // Parse our response
    NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&errorParse];
    
    // Check if an error occured while serializing or server returned false
    if (![responseDict[@"status"] boolValue])
    {
        // Create an error object
        *error = [[AppError alloc] initWithErrorCode:[responseDict[@"error_code"] integerValue]];
        responseDict = nil;
    }
    else if (errorParse)
    {
        // Create an error object
        *error = [[AppError alloc] initWithErrorCode:kErrorCodeServerException];
        responseDict = nil;
    }
    
    return responseDict;
}

- (void)handleRequestwithMethod:(NSString*)methodName
                        andPath:(NSString*)path
                     parameters:(NSDictionary*)parametersDict
                   successBlock:(void (^)(NSDictionary* responseDict))successBlock
                   failureBlock:(void (^)(AppError* error))failureBlock
{
    // Perform our request
    [self performCallWithMethod:methodName andPath:path parameters:parametersDict successBlock:^(AFHTTPRequestOperation* requestOperation) {
        
        // Print our response
        NSLog(@"%@", requestOperation.responseString);
        
        AppError* responseError;
        
        // Get a response error
        NSDictionary *responseDict = [self validateResponse:requestOperation.responseData appError:&responseError];
        
        // Check if there is no error
        if (responseError == nil)
        {
            // There is no error
            
            // Make sure we have a success block
            if (successBlock != NULL)
            {
                // Invoke our success block
                successBlock(responseDict);
            }
        }
        else
        {
            // Log the exception to our server
            [self logServerExceptionAtPath:path withRequestParameters:parametersDict andResponse:requestOperation.responseString];
            
            // Check if we have a failure block
            if (failureBlock != NULL)
            {
                // Invoke our failure block
                failureBlock(responseError);
            }
        }
        
    } failureBlock:^(AppError *error) {
        
        if (failureBlock != NULL)
        {
            // Call our failure block
            failureBlock(error);
        }
    }];
}

- (void)handleMultiPartRequestWithMethod:(NSString*)method
                                 andPath:(NSString*)path
                              parameters:(NSDictionary*)parameters
                       andMultiPartFiles:(NSArray*)filesArray
                            successBlock:(void (^)(NSDictionary* responseDict))successBlock
                            failureBlock:(void (^)(AppError* error))failureBlock
{
    // Perform the multi part request
    [self performMultiPartFormRequestWithMethod:method andPath:path parameters:parameters andMultiPartFiles:filesArray successBlock:^(AFHTTPRequestOperation *requestOperation){
        
        // Print our response
        NSLog(@"%@", requestOperation.responseString);
        
        AppError* responseError;
        
        // Get a response error
        NSDictionary *responseDict = [self validateResponse:requestOperation.responseData appError:&responseError];
        
        // Check if there is no error
        if (responseError == nil)
        {
            // There is no error
            
            // Make sure we have a success block
            if (successBlock != NULL)
            {
                // Invoke our success block
                successBlock(responseDict);
            }
        }
        else
        {
            // Log the exception to our server
            [self logServerExceptionAtPath:path withRequestParameters:parameters andResponse:requestOperation.responseString];
            
            
            if (failureBlock != NULL)
            {
                // Invoke our failure block
                failureBlock(responseError);
            }
        }
        
    } failureBlock:^(AppError *error) {
        
         if (failureBlock != NULL)
        {
            // Invoke our failure block
            failureBlock(error);
        }
    }];
}

- (void)performCallWithMethod:(NSString*)methodName
                      andPath:(NSString*)path
                   parameters:(NSDictionary*)parametersDict
                 successBlock:(void (^)(AFHTTPRequestOperation* requestOperation))successBlock
                 failureBlock:(void (^)(AppError* error))failureBlock
{
    // Create a http client to do the calling
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] init];
    
    // Set the client's encoding
    [httpClient setStringEncoding:NSUTF8StringEncoding];
    
    // Create a request operation
    AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:[httpClient requestWithMethod:methodName path:path parameters:parametersDict]];
    
    // Set a completion block
    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // Check if we have a success block
        if (successBlock != NULL)
        {
            // Call our success block
            successBlock(operation);
        }
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
            NSLog(@"%@", operation.responseString);
            
            // Make sure that we recieved a failure block
            if (failureBlock != NULL)
            {
                // Create a connection error
                AppError *error = [[AppError alloc] initWithErrorCode:kErrorCodeNoConnection];
                
                // Invoke our failure block
                failureBlock(error);
            }
    }];
    
    // Start the request operation
    [requestOperation start];
}

- (void)performMultiPartFormRequestWithMethod:(NSString*)method
                                      andPath:(NSString*)path
                                   parameters:(NSDictionary*)parameters
                            andMultiPartFiles:(NSArray*)filesArray
                                 successBlock:(void (^)(AFHTTPRequestOperation* requestOperation))successBlock
                                 failureBlock:(void (^)(AppError* error))failureBlock
{
    // Create a http client
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] init];
    
    // Set the client's encoding
    [httpClient setStringEncoding:NSUTF8StringEncoding];
    
    // Create a request
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:method
                                                                         path:path parameters:parameters
                                                    constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                                        
                                                        // Iterate over our files to send
                                                        for (MultiPartFile* multiPartFile in filesArray)
                                                        {
                                                            // Append our data to our form
                                                            [multiPartFile appendToMultiPartForm:formData];
                                                        }
    }];
    
    // Create a request operation
    AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    // Set our completion block
    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // Check if we have a success block
        if (successBlock != NULL)
        {
            // Call our success block
            successBlock(operation);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if (failureBlock != NULL)
        {
            AppError* connectionError = [[AppError alloc] initWithErrorCode:kErrorCodeNoConnection];
            
            // Call our failure block
            failureBlock(connectionError);
        }
        
    }];
    
    // Start our operation
    [requestOperation start];
}

- (void)logServerExceptionAtPath:(NSString*)url withRequestParameters:(NSDictionary*)requestParameters andResponse:(NSString*)response
{
    // Serialize our response to a JSON
    NSError* error;
    NSData *parametersJSONData = [NSJSONSerialization dataWithJSONObject:requestParameters options:0 error:&error];
    
    // Check if there was no error serializing
    if (!error)
    {
        // Create a string from our data
        NSString *strParametersJSON = [[NSString alloc] initWithData:parametersJSONData encoding:NSUTF8StringEncoding];
        
        // Get our app name
        NSBundle *bundle = [NSBundle mainBundle];
        NSDictionary *info = [bundle infoDictionary];
        NSString *appName = [info objectForKey:@"CFBundleDisplayName"];
        
        // Create our parameters dictionary
        NSDictionary* parameters = @{@"appName" : appName,
                                     @"url" : url,
                                     @"os" : [NSString stringWithFormat:@"%@ - iOS %@", [UIDevice currentDevice].model, [UIDevice currentDevice].systemVersion],
                                     @"request" : strParametersJSON,
                                     @"response" : response};
        
        // Send our log to the server
        [self performCallWithMethod:kMethodPost
                            andPath:kURLWebServiceLogServerException
                         parameters:parameters
                       successBlock:NULL
                       failureBlock:NULL];
    }
}

@end
