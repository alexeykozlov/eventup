//
//  ServiceManager.h
//  IDareU
//
//  Created by Roee Kattaby on 2/26/13.
//  Copyright (c) 2013 Roee Kattaby. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "AppError.h"
#import "MultiPartFile.h"

#define kURLWebServiceLogServerException @"http://iappsdev.mobi/App_Log/api/logServerException.php"
#define kMethodPost @"POST"
#define kMethodGet @"GET"

@interface ServiceManager : NSObject

+ (ServiceManager*)getInstance;

- (void)handleRequestwithMethod:(NSString*)methodName
                        andPath:(NSString*)path
                     parameters:(NSDictionary*)parametersDict
                   successBlock:(void (^)(NSDictionary* responseDict))successBlock
                   failureBlock:(void (^)(AppError* error))failureBlock;

- (void)handleMultiPartRequestWithMethod:(NSString*)method
                                 andPath:(NSString*)path
                              parameters:(NSDictionary*)parameters
                       andMultiPartFiles:(NSArray*)filesArray
                            successBlock:(void (^)(NSDictionary* responseDict))successBlock
                            failureBlock:(void (^)(AppError* error))failureBlock;

@end