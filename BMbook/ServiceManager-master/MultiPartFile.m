//
//  MultiPartFile.m
//  iSpotARC
//
//  Created by Roee Kattaby on 4/18/13.
//  Copyright (c) 2013 iApps. All rights reserved.
//

#import "MultiPartFile.h"

@implementation MultiPartFile

-(id)initWithServerKey:(NSString *)serverKey
                 andData:(NSData *)data
             andFileName:(NSString *)fileName
             andMimeType:(MimeType)mimeType
{
    if (self = [super init])
    {
        // Set our properties
        [self setServerKey:serverKey];
        [self setData:data];
        [self setFileName:fileName];
        [self setMimeType:mimeType];
    }
    
    return self;
}

-(void)appendToMultiPartForm:(id<AFMultipartFormData>)formData
{
    // Append the file data to our form
    [formData appendPartWithFileData:self.data name:self.serverKey fileName:self.fileName mimeType:[self getMimeTypeString]];
}

- (NSString*)getMimeTypeString
{
    NSString *mimeType = @"";
    
    // Check our mime type
    switch (self.mimeType)
    {
        case MimeTypeImageJPG:
            mimeType = @"image/jpeg";
            
            break;
        case MimeTypeImageGIF:
            mimeType = @"image/gif";
            
            break;
        case MimeTypeImagePNG:
            mimeType = @"image/png";
            
            break;
        default:
            break;
    }
    
    return mimeType;
}

@end
