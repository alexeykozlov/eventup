//
//  ExceptionHandler.m
//  Assembles
//
//  Created by Roee Kattaby on 5/22/13.
//  Copyright (c) 2013 iapps. All rights reserved.
//

#import "ExceptionHandler.h"

@implementation ExceptionHandler

+ (void)setupExceptionHandling
{
    // Override point for customization after application launch.
    NSSetUncaughtExceptionHandler(&HandleException);
}

void HandleException(NSException * exception)
{
    // Create an nsurl request to send the exception
    NSMutableURLRequest* urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:kURLWebServiceLogClientException]];
    [urlRequest setHTTPMethod:@"POST"];

    // Get our appname
    NSBundle *bundle = [NSBundle mainBundle];
    NSDictionary *infoDict = [bundle infoDictionary];
    NSString *appName = [infoDict objectForKey:@"CFBundleDisplayName"];
    
    // USER: ADD YOUR OWN INFO HERE
    NSString* infoString = @"";
    
    // Get our device info
    NSString* deviceInfo = [NSString stringWithFormat:@"%@ - iOS %@", [UIDevice currentDevice].model, [UIDevice currentDevice].systemVersion];
    
    // Parse our stacktrace to json
    NSData *stackTraceJSONData = [NSJSONSerialization dataWithJSONObject:exception.callStackSymbols options:0 error:nil];
    NSString* stackTraceJSON = [[NSString alloc] initWithData:stackTraceJSONData encoding:NSUTF8StringEncoding];
    
    // Set our post string
    NSString* postString = [NSString stringWithFormat:@"appName=%@&exceptionName=%@&os=%@&stackTrace=%@&info=%@&reason=%@", appName, exception.name, deviceInfo, stackTraceJSON, infoString, exception.reason];

    // Set our http body
    [urlRequest setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];

    // Perform the request synchronously before the application crashes
    [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:nil error:nil];
}

@end