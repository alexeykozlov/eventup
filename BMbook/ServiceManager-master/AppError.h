//
//  AppError.h
//  iSpotARC
//
//  Created by Roee Kattaby on 4/18/13.
//  Copyright (c) 2013 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define kErrorTitle NSLocalizedString(@"Oops!", @"ErrorTitle")

#define kErrorCodeNoConnection -1
#define kErrorCodeServerException 0

@interface AppError : NSObject

@property (nonatomic, strong) NSString* errorDescription;
@property int errorCode;

+ (void)loadErrorsFromPList:(NSString*)fileName;
-(id)initWithErrorCode:(int)errorCode;
- (void)displayErrorMessage;

@end