//
//  MultiPartFile.h
//  iSpotARC
//
//  Created by Roee Kattaby on 4/18/13.
//  Copyright (c) 2013 iApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

typedef enum
{
    MimeTypeImageJPG,
    MimeTypeImagePNG,
    MimeTypeImageGIF
} MimeType;

@interface MultiPartFile : NSObject

@property MimeType mimeType;
@property (strong, nonatomic) NSString* serverKey;
@property (strong, nonatomic) NSData* data;
@property (strong, nonatomic) NSString* fileName;

- (id)initWithServerKey:(NSString*)serverKey andData:(NSData*)data andFileName:(NSString*)fileName andMimeType:(MimeType)mimeType;
- (void)appendToMultiPartForm:(id<AFMultipartFormData>)formData;

@end
