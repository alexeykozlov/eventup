//
//  AppDelegate.m
//  BMbook
//
//  Created by Shoshi V. on 5/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "AppDelegate.h"

NSString *const FBSessionStateChangedNotification =
@"com.umbrella.EventUp:FBSessionStateChangedNotification";

@implementation AppDelegate
{
    SlideViewController *slideController ;
}
//NSString *const FBSessionStateChangedNotification =
//@"com.iapps.GetJob:FBSessionStateChangedNotification";
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    sleep(2);
    [AppError loadErrorsFromPList:@"errors"];
    
      [self initUserDefault];
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;
    
    // Optional: set Logger to VERBOSE for debug information.
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    
    // Initialize tracker.
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:kTrackingId];

    [ExceptionHandler setupExceptionHandling];
    
    //init slide controller
    slideController = [[SlideViewController alloc] init];
    slideController.slideViewPaddingRight = 60.0;
    slideController.slideViewPaddingLeft = 0;
    self.window.rootViewController = slideController;
    
    [self.window makeKeyAndVisible];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle: nil];
    FirstViewController *firstVC = [mainStoryboard instantiateViewControllerWithIdentifier:kIdentifierFirstVC];
    slideController.allowInteractiveSlideing = FALSE;
    firstVC.delegate = self;
    [slideController.nav pushViewController:firstVC animated:NO];
    

    return YES;
}
				
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
     [FBSession.activeSession handleDidBecomeActive];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kAPIKeyPlaySound] boolValue] ==TRUE)
//    {
//        [SoundHandler playSoundWithSoundName:@"BMbook_sound" withFormat:@"wav" shouldPlayIfIpodPlaying:YES withNumberOfLoop:-1];
//    }
 
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [SoundHandler stopAllMusic];
        [FBSession.activeSession close];
}
- (void)initUserDefault
{
    NSString *strCheakFirst = (NSString *)[[NSUserDefaults standardUserDefaults] objectForKey:@"KfirstSettings"];
	if (strCheakFirst  == nil)     // App first run: set up user defaults.
	{
//        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithBool:TRUE] forKey:kAPIKeyPlaySound];
//        [[NSUserDefaults standardUserDefaults] setObject:@"firstapp" forKey:@"KfirstSettings"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

#pragma mark - ProtocolSlide
- (void)allowInteractiveSlideing:(BOOL)allow
{
    slideController.allowInteractiveSlideing = allow;

}

//alows youtube to play in landscape
- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    
    id presentedViewController = [window.rootViewController presentedViewController];
    NSString *className = presentedViewController ? NSStringFromClass([presentedViewController class]) : nil;
    
    if (window && [className isEqualToString:@"MPInlineVideoFullscreenViewController"]) {
        return UIInterfaceOrientationMaskAll;
    } else {
        return UIInterfaceOrientationMaskPortrait;
    }
}

@end
