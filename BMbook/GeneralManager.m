//
//  GeneralManager.m
//  BMbook
//
//  Created by iApps Mac Mini on 9/9/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "GeneralManager.h"

@implementation GeneralManager

//send data to server and get session id from server
+(void)registerEventWithMail:(NSString*)strEmail andPassword:(NSString*)strPassword andAdress:(NSString*)strAdress andAllowGifts:(BOOL)bAllowGifts andName:(NSString*)strName andCode:(int)nCode andDate: (int)dateEvent
                      Success:(void (^)())successBlock
                      failure:(void (^)())failureBlock
{
    // Create our parameters dictionary
    NSDictionary *params = @{@"name":strName,
                             @"date":@(dateEvent).stringValue,
                             @"address":strAdress,
                             @"allowGifts": bAllowGifts ? @(1) : @(0),
                             @"email":strEmail,
                             @"password":strPassword,
                             @"keyword":@(nCode).stringValue};
    
    NSString *path = [NSString stringWithFormat:@"%@%@", kBaseAddress, @"api/addEvent.php"];
    
    // Perform our request
    [[ServiceManager getInstance] handleRequestwithMethod:@"POST" andPath:path parameters:params successBlock:^(NSDictionary *responseDict) {
        
        NSDictionary* dataDict = [responseDict objectForKey:@"data"];
        NSNumber* nId =[dataDict objectForKey:@"id"];
        NSString *name=[dataDict objectForKey:@"name"];
        NSString *date=[dataDict objectForKey:@"date"];
        NSString *adress=[dataDict objectForKey:@"address"];
        NSNumber* boolNum = [dataDict objectForKey:@"allowGifts"];
        NSString *email=[dataDict objectForKey:@"email"];
        NSNumber *keyword = [dataDict objectForKey:@"keyword"];
        NSString *sessionId=[dataDict objectForKey:@"sid"];
        [[NSUserDefaults standardUserDefaults]setObject:nId forKey:kAPIKeyEventID];
        [[NSUserDefaults standardUserDefaults]setObject:name forKey:kAPIKeyEventName];
        [[NSUserDefaults standardUserDefaults]setObject:date forKey:kAPIKeyDateEvent];
        [[NSUserDefaults standardUserDefaults]setObject:adress forKey:kAdress];
        [[NSUserDefaults standardUserDefaults]setObject:boolNum forKey:kAllowGifts];
        [[NSUserDefaults standardUserDefaults]setObject:email forKey:kUserName];
        [[NSUserDefaults standardUserDefaults]setObject:keyword forKey:kAPIKeyKeyword];
        [[NSUserDefaults standardUserDefaults]setObject:sessionId forKey:KSessionId];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        // Get the response status.
        BOOL status = [[responseDict objectForKey:@"status"] boolValue];
        if (status)
        {
            [GeneralManager setSessionId:[responseDict objectForKey:@"data"]];
            successBlock();
        }
        
    } failureBlock:^(AppError *error) {
        
        // Display the right error message to the user
        [error displayErrorMessage];
    }];
}

//sets the id
+ (void) setSessionId:(NSString*)sessionId
{
    
    [[NSUserDefaults standardUserDefaults]setObject:sessionId forKey:KSessionId];
}

//logs in to an event
+(void) logInWithEmail:(NSString*)strEmail andPassword:(NSString*)strPassword
                Success:(void (^)())successBlock
                failure:(void (^)())failureBlock
{
    // Create our parameters dictionary
    NSDictionary *params = @{@"email":strEmail,
                             @"password":strPassword};
    
    NSString *path = [NSString stringWithFormat:@"%@%@", kBaseAddress, @"api/login.php"];
    
    // Perform our request
    [[ServiceManager getInstance] handleRequestwithMethod:@"POST" andPath:path parameters:params successBlock:^(NSDictionary *responseDict) {
        NSDictionary* dataDict = [responseDict objectForKey:@"data"];
        NSNumber* nId =[dataDict objectForKey:@"id"];
        NSString *name=[dataDict objectForKey:@"name"];
        NSString *date=[dataDict objectForKey:@"date"];
        NSString *adress=[dataDict objectForKey:@"address"];
        NSNumber* boolNum = [dataDict objectForKey:@"allowGifts"];
        NSString *email=[dataDict objectForKey:@"email"];
        NSNumber *keyword = [dataDict objectForKey:@"keyword"];
        NSString *sessionId=[dataDict objectForKey:@"sid"];
        NSString *song=[dataDict objectForKey:@"song"];
        [[NSUserDefaults standardUserDefaults]setObject:nId forKey:kAPIKeyEventID];
        [[NSUserDefaults standardUserDefaults]setObject:name forKey:kAPIKeyEventName];
        [[NSUserDefaults standardUserDefaults]setObject:date forKey:kAPIKeyDateEvent];
        [[NSUserDefaults standardUserDefaults]setObject:adress forKey:kAdress];
        [[NSUserDefaults standardUserDefaults]setObject:boolNum forKey:kAllowGifts];
        [[NSUserDefaults standardUserDefaults]setObject:email forKey:kUserName];
        [[NSUserDefaults standardUserDefaults]setObject:keyword forKey:kAPIKeyKeyword];
        [[NSUserDefaults standardUserDefaults]setObject:sessionId forKey:KSessionId];
        [[NSUserDefaults standardUserDefaults]setObject:song forKey:kAPIKeySong];
        [[NSUserDefaults standardUserDefaults] synchronize];
        successBlock();
        
    } failureBlock:^(AppError *error) {
        

        // Display the right error message to the user
        [error displayErrorMessage];
    }];
}

//logs out from creator mode
+(void) logOutWithSessionId:(NSString*)sessionId
                    Success:(void (^)())successBlock
                    failure:(void (^)())failureBlock
{
    // Create our parameters dictionary
    NSDictionary *params = @{@"sid":sessionId};
    
    NSString *path = [NSString stringWithFormat:@"%@%@", kBaseAddress, @"api/logout.php"];
    
    // Perform our request
    [[ServiceManager getInstance] handleRequestwithMethod:@"POST" andPath:path parameters:params successBlock:^(NSDictionary *responseDict) {
        
        successBlock();
        
    } failureBlock:^(AppError *error) {
        
        // Display the right error message to the user
        [error displayErrorMessage];
    }];
}

//updates the event details
+(void) updateEventDetailsWithSessionId:(NSString*)sessionId andName:(NSString*)name andDate:(int)date andAdress:(NSString*)adress andAllowGifts:(BOOL)allowGifts andEmail:(NSString*)email andPassword:(NSString*)password andCode:(int)code andSong:(NSString*)song
                                Success:(void (^)())successBlock
                                failure:(void (^)())failureBlock
{
    // Create our parameters dictionary
    NSDictionary *params = @{@"sid":sessionId,
                             @"name":name,
                             @"date":@(date).stringValue,
                             @"address":adress,
                             @"allowGifts":allowGifts ? @(1) : @(0),
                             @"email":email,
                             @"password":password == nil ? @"" : password,
                             @"keyword":@(code).stringValue,
                             @"song":song};
    
    NSString *path = [NSString stringWithFormat:@"%@%@", kBaseAddress, @"api/updateEventDetails.php"];
    
    // Perform our request
    [[ServiceManager getInstance] handleRequestwithMethod:@"POST" andPath:path parameters:params successBlock:^(NSDictionary *responseDict) {
        
        [[NSUserDefaults standardUserDefaults]setObject:sessionId forKey:KSessionId];
        [[NSUserDefaults standardUserDefaults]setObject:name forKey:kAPIKeyEventName];
        [[NSUserDefaults standardUserDefaults]setObject:@(date).stringValue forKey:kAPIKeyDateEvent];
        [[NSUserDefaults standardUserDefaults]setObject:adress forKey:kAdress];
        [[NSUserDefaults standardUserDefaults]setObject:@(allowGifts).stringValue forKey:kAllowGifts];
        [[NSUserDefaults standardUserDefaults]setObject:email forKey:kUserName];
        [[NSUserDefaults standardUserDefaults]setObject:@(code).stringValue forKey:kAPIKeyKeyword];
        [[NSUserDefaults standardUserDefaults]setObject:song forKey:kAPIKeySong];
        successBlock();
        
    } failureBlock:^(AppError *error) {
        failureBlock();
        // Display the right error message to the user
        [error displayErrorMessage];
    }];
}

@end
