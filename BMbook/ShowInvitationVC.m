//
//  ShowInvitationVC.m
//  BMbook
//
//  Created by Shoshi V. on 5/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "ShowInvitationVC.h"

@interface ShowInvitationVC ()
{
  }
@end

@implementation ShowInvitationVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.delegate allowInteractiveSlideing:TRUE];
    self.title = NSLocalizedString(@"ההזמנה לאירוע",nil);
    
    //set date
    self.lblDate.text =[self returnStringOfStatusEvent];
    //sets the date color
    self.lblDate.textColor=[UIColor colorWithRed:(255/255.0) green:(220/255.0) blue:(90/255.0) alpha:1] ;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.labelNoInvitation setHidden:YES];
    [self.imgInvitation setHidden:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    self.screenName=@"Invitation display screen";
    
    
    //show activity
    MBProgressHUD *HUD = [[MBProgressHUD alloc]  initWithView:self.view];
	[self.view addSubview:HUD];
    [HUD show:YES];
    
    //gets the image from the server and displays it
    [EventManager getInvitationWithId:[[NSUserDefaults standardUserDefaults] stringForKey:kAPIKeyEventID].intValue
                              Success:^(NSURL* imageUrl)
     {
         if(imageUrl!=NULL)
         {
             [self.imgInvitation setImageWithURL:imageUrl];
             [self.imgInvitation setHidden:NO];
             
         }
         else
         {
             [self.labelNoInvitation setHidden:NO];
         }
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
     }
                              failure:^(){
                                  [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                              }];
    
    //show hot to use
    if([[NSUserDefaults standardUserDefaults]boolForKey:kNotFirstEnter]==NO)
    {
        NSArray *arrImages = @[@"HTU_1",@"HTU_2",@"HTU_3",@"HTU_4",@"HTU_5",@"HTU_7"];
        NSArray *arrTexts = @[@"הוזמנת לחתונה? מזל טוב! מהיום כל המידע שתצטרכו נמצא במקום אחד",@"בחרו את האירוע שהוזמנתם אליו מתוך כל בעלי השמחה",@"הוסיפו ברכה ייחודית ותמונה מקורית שבעלי השמחה והאורחים יוכלו להתרגש ממנה",@"צפו בהזמנה לאירוע ונווטו ליעד בעזרת האפליקציה",@"תוכלו לשלוח מתנה לזוג המאושר בקלות ובנוחות",@"אהבתם את שיר החתונה? תוכלו להאזין לו שוב בדרך הביתה"];
        NSArray *arrTitels = @[@"ברוכים הבאים",@"בחרו אירוע",@"הוסיפו ברכה",@"נווטו לאירוע",@"שלח מתנה",@"שיר החתונה"];
        
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:kNotFirstEnter];
        
        self.instructionsView =[[[NSBundle mainBundle] loadNibNamed:@"InstructionsView" owner:nil options:nil]objectAtIndex:0];
        self.instructionsView.delegate =self;
        [self.instructionsView configureWithFrameDarkOverlayView:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) andNumPages:kNumPages andArrayTitels:arrTitels andArrayImages:arrImages andArrayTexts:arrTexts];
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setBackgroundColor:[UIColor clearColor]];
        btn.titleLabel.font = [UIFont systemFontOfSize:12];
        [btn setTitleColor:[UIColor colorWithRed:(255/255.0) green:(220/255.0) blue:(90/255.0) alpha:1] forState:UIControlStateNormal];
        
        [btn setTitle:@"אישור" forState:UIControlStateNormal];
        btn.frame = CGRectMake(0, 0, 80, 40);
        [btn setBackgroundImage:[UIImage imageNamed:@"button_small"] forState:UIControlStateNormal];
        [btn addTarget:self.instructionsView action:@selector(didClickedInstructionsContinueButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.instructionsView setButton:btn];
        
        //add instructions View
        [self.instructionsView showOnView:self.view];
    }
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:kLogIn]==YES)
    {
        //opens the menu
        [self.delegate showMainMenu:nil];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kLogIn];
        
        
        //show welcome alert
        NSString *messageText=@"ברוכים הבאים לאירוע של: ";
        NSString *messageName=[[NSUserDefaults standardUserDefaults]stringForKey:kAPIKeyEventName];
        NSString *message=[NSString stringWithFormat:@"%@%@",messageText,messageName];
        UIAlertView *alert =[[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:@"המשך לאירוע" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)popController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
}

- (BOOL)shouldAutorotate
{
    return YES;
}
- (NSInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

#pragma  UIWebView delegate
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
{
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [self performSelector:@selector(scale) withObject:nil afterDelay:0.2];


}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

}

#pragma InstuctionsViewDelegate Metods

- (void)dismissInstructionsView
{
    //remove instructions View
    [self.instructionsView hide];
}


@end
