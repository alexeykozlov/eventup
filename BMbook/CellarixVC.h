//
//  CellarixVC.h
//  EventUp
//
//  Created by Alexey on 11/5/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "SlideOptionViewController.h"

@interface CellarixVC : SlideOptionViewController <UIWebViewDelegate>

@property (strong, nonatomic) NSString* url;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong,nonatomic) MBProgressHUD * HUD;

@property (strong, nonatomic) IBOutlet UIButton *buttonReturn;

@end
