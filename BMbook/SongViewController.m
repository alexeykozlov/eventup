//
//  SongViewController.m
//  EventUp
//
//  Created by Alexey on 10/28/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "SongViewController.h"

@interface SongViewController ()

@end

@implementation SongViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=@"שיר החתונה";
    self.view.backgroundColor=[UIColor blackColor];
    self.webView.opaque=NO;
    [self.webView setBackgroundColor:[UIColor clearColor]];
    [self.webView setDelegate:self];
    NSString *prefix=@"http://";
	NSString *fullURL = [[NSUserDefaults standardUserDefaults] stringForKey:kAPIKeySong];
    //checks if the url entered starts with the prefix and adds it if it doesnt
    if([fullURL rangeOfString:prefix].location == NSNotFound)
    {
        fullURL = [NSString stringWithFormat:@"%@%@",prefix,fullURL];
    }
    
    NSURL *url = [NSURL URLWithString:fullURL];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:requestObj];
    
    //set date
    self.lblDate.text =[self returnStringOfStatusEvent];
    //sets the date color
    self.lblDate.textColor=[UIColor colorWithRed:(255/255.0) green:(220/255.0) blue:(90/255.0) alpha:1] ;
    
//    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(didRotate:)
//                                                 name:UIDeviceOrientationDidChangeNotification
//                                               object:nil];
    
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationPortrait | UIInterfaceOrientationLandscapeLeft | UIInterfaceOrientationLandscapeRight | UIInterfaceOrientationPortraitUpsideDown;
}

-(BOOL)shouldAutorotate
{
    return YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    self.screenName=@"Wedding song screen";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    //[self.HUD hide:YES];
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    //displays hud
//    self.HUD = [[MBProgressHUD alloc] initWithWindow:[UIApplication sharedApplication].keyWindow];
//    self.HUD.labelText = @"טוען דף...";
//    [self.view.window addSubview:self.HUD];
//    [self.HUD show:YES];
}

//- (void)didRotate:(NSNotification *)notification {
//    UIDeviceOrientation orientation = [[notification object] orientation];
//    
//    if (orientation == UIDeviceOrientationLandscapeLeft) {
//        [self.view setTransform:CGAffineTransformMakeRotation(M_PI / 2.0)];
//    } else if (orientation == UIDeviceOrientationLandscapeRight) {
//        [self.view setTransform:CGAffineTransformMakeRotation(M_PI / -2.0)];
//    } else if (orientation == UIDeviceOrientationPortraitUpsideDown) {
//        [self.view setTransform:CGAffineTransformMakeRotation(M_PI)];
//    } else if (orientation == UIDeviceOrientationPortrait) {
//        [self.view setTransform:CGAffineTransformMakeRotation(0.0)];
//    }
//}



@end
