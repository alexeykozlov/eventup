//
//  CreateEventViewController.h
//  BMbook
//
//  Created by iApps Mac Mini on 9/9/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PickerManager.h"
#import "GAITrackedViewController.h"


@interface CreateEventViewController : GAITrackedViewController <UIScrollViewDelegate,UITextFieldDelegate, PickerTextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *username;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UITextField *confirmPassword;
@property (strong, nonatomic) IBOutlet UITextField *eventName;
@property (strong, nonatomic) IBOutlet UITextField *code;
@property (strong, nonatomic) IBOutlet UITextField *adress;
@property (strong, nonatomic) IBOutlet PickerTextField *dateAndTime;

@property int unixTime;
//@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSString *strTime;

@property (strong, nonatomic) IBOutlet UISwitch *allowGifts;



@property (strong, nonatomic) IBOutlet UIScrollView *scroll;

-(BOOL) checkUsername:(NSString*)username;
-(BOOL) checkPasswordsWithPassword:(NSString*)password andConfirmPassword:(NSString*)confirmPassword;
-(BOOL) checkCode:(NSString*)code;

-(IBAction) registerEvent: (id)sender;

@end
