//
//  OneWishView.m
//  BMbook
//
//  Created by Shoshi V. on 5/19/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "OneWishView.h"

@implementation OneWishView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)init
{
	if (self=[super init])
    {
        self.backgroundColor =[UIColor clearColor];
        //        UIImageView * imgBackGroundPhoto = [[UIImageView alloc]  initWithFrame:CGRectMake(0, 0, 320, 177)];
        //       imgBackGroundPhoto.backgroundColor = [UIColor whiteColor];
        //
        
        self.photo = [[UIImageView alloc] initWithFrame:CGRectMake(0, 44, 320, 320)];
        self.photo.backgroundColor = [UIColor clearColor];
        self.photo.contentMode = UIViewContentModeScaleAspectFit;

        self.textView = [[UITextView alloc] initWithFrame:CGRectMake(7, 369, 306, 128)];
        self.textView.textAlignment = NSTextAlignmentRight;
        self.textView.font =[UIFont boldSystemFontOfSize:14];
        
        self.textView.editable = FALSE;
        self.textView.textColor = [UIColor blackColor];
        self.textView.backgroundColor =[UIColor whiteColor];
        
        self.lblTitle =[[UILabel alloc] init];
        self.lblTitle.frame = CGRectMake(10, 2, 300, 36);
        self.lblTitle.textAlignment = NSTextAlignmentCenter;
        self.lblTitle.font =[UIFont boldSystemFontOfSize:24];
        self.lblTitle.textColor = [UIColor whiteColor];
         self.lblTitle.backgroundColor =[UIColor clearColor];
        
        if(!IS_IPHONE_5 )
        {
            self.photo.frame =CGRectMake(20, 44, 280, 280);
            self.textView.frame = CGRectMake(7, 330, 306, 78);
        }
        // [self addSubview:imgBackGroundPhoto];
        [self addSubview:self.photo];
        [self addSubview:self.textView];
        [self addSubview:self.lblTitle];
        
	}
    return self;
}
@end
