//
//  OnePage.h
//  InstructionsFramework
//
//  Created by Shoshi V. on 6/11/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalValuesInstrucions.h"
@interface OnePage : UIView
@property (nonatomic ,strong) UIImageView *imageView;
@property (nonatomic ,strong) UITextView *textView;
@property (nonatomic ,strong) UILabel *lblTitle;
@end
