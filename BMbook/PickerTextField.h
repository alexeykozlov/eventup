//
//  PickerTextField.h
//  IDareU
//
//  Created by iApps Dev. Mac mini on 3/13/13.
//  Copyright (c) 2013 Roee Kattaby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PickerTextFieldDelegate.h"

#define kLabelDone NSLocalizedString(@"Done", @"")
#define kLabelCancel NSLocalizedString(@"Remove", @"")

@interface PickerTextField : UITextField <UIPickerViewDataSource,
                                          UIPickerViewDelegate>

- (void)insertPickerIntoTextField:(UIPickerView*)pickerView withActionDelegate:(id<PickerTextFieldDelegate>)controller;
-(void)insertDatePickerIntoTextField:(UIDatePicker*)datePicker withActionDelegate:(id<PickerTextFieldDelegate>)controller;

@property (nonatomic, strong) id<PickerTextFieldDelegate> pickerDelegate;
@property (nonatomic, strong) NSArray* optionsArray;
@property (nonatomic, strong) NSDictionary* keysDict;

@end
