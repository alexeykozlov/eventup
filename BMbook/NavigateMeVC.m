//
//  NavigateMeVC.m
//  BMbook
//
//  Created by Shoshi V. on 5/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import "NavigateMeVC.h"

@interface NavigateMeVC ()

@end


@implementation NavigateMeVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.delegate allowInteractiveSlideing:TRUE];
	self.title = NSLocalizedString(@"נווט לאירוע", nil);
    self.lblTakeMeUp.text = [[NSUserDefaults standardUserDefaults] stringForKey:kAPIKeyEventName];
    
    //set desc of locations
    self.lblLocationUp.text =[[NSUserDefaults standardUserDefaults] stringForKey:kAdress];
    
    //set date
    self.lblDate.text =[self returnStringOfStatusEvent];
    
    //sets the date color
    self.lblDate.textColor=[UIColor colorWithRed:(255/255.0) green:(220/255.0) blue:(90/255.0) alpha:1] ;
}

-(void)viewDidAppear:(BOOL)animated
{
    self.screenName=@"Navigate to event screen";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)takeMe:(id)sender
{
    NSString *strLocation= [[NSUserDefaults standardUserDefaults] stringForKey:kAdress];

    NSString *stringURL;
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"waze://"]])
    {
         stringURL = [[NSString stringWithFormat:@"waze://?hl=iw&ie=UTF8&q=%@&navigate=yes",strLocation]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
    else
    {
        stringURL = [[NSString stringWithFormat:@"maps://?hl=iw&ie=UTF8&q=%@", strLocation] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
        NSURL *url = [NSURL URLWithString:stringURL];
        if ([[UIApplication sharedApplication] canOpenURL:url])
        {
            [[UIApplication sharedApplication] openURL:url];
        }

}
@end
