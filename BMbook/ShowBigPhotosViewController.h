//
//  ShowBigPhotosViewController.h
//  BMbook
//
//  Created by Shoshi V. on 5/19/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OneWishView.h"
#import "GAITrackedViewController.h"

@interface ShowBigPhotosViewController : GAITrackedViewController
@property (nonatomic ,strong) NSMutableArray  *arrPhotos;
@property (nonatomic ,strong) NSMutableArray  *arrViewsOfPhotos;
@property (nonatomic ,assign) int indexOfSelectedPhoto;
@property (nonatomic ,weak) IBOutlet UIScrollView * scrollView;
@property (nonatomic ,weak) IBOutlet UIButton * rightButton;
@property (nonatomic ,weak) IBOutlet UIButton * leftButton;
@property (nonatomic ,weak) IBOutlet UIImageView * rightImage;
@property (nonatomic ,weak) IBOutlet UIImageView * leftImage;
@property (strong, nonatomic) IBOutlet UIImageView *imgTextBackground;

- (IBAction)btnArrowLeftPressed;
- (IBAction)btnArrowRightPressed;

@end
