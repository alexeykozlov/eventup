//
//  InfoVC.h
//  BMbook
//
//  Created by Shoshi V. on 5/12/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideOptionViewController.h"
#import <MessageUI/MessageUI.h>
#import <StoreKit/StoreKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "GlobalValuesInstrucions.h"
#import "OnePage.h"
#import "SMPageControl.h"

@interface InfoVC : SlideOptionViewController <MFMailComposeViewControllerDelegate,UINavigationControllerDelegate,SKStoreProductViewControllerDelegate,InstructionsViewDelegate>

@property (nonatomic,weak) IBOutlet UIButton *btnMoreApps;
@property (nonatomic,weak) IBOutlet UIButton *btnShareInEmail;
@property (nonatomic,weak) IBOutlet UIButton *btnShareInFacebook;
@property (nonatomic,weak) IBOutlet UILabel *lblVersion;
@property (nonatomic ,weak)IBOutlet UILabel *lblDate;
@property (strong, nonatomic) NSMutableDictionary *postParams;
@property (nonatomic ,strong) InstructionsView *instructionsView;
@property (nonatomic ,assign) id<InstructionsViewDelegate> delegate2;

- (IBAction)moreApps:(id)sender;
- (IBAction)shareInEmail:(id)sender;
- (IBAction)shareInFacebook:(id)sender;
- (IBAction)showInstructionsButtonWasPressed:(id)sender;

@end
