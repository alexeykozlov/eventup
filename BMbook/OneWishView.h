//
//  OneWishView.h
//  BMbook
//
//  Created by Shoshi V. on 5/19/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OneWishView : UIView
@property (nonatomic ,strong) UIImageView *photo;
@property (nonatomic ,strong) UITextView *textView;
@property (nonatomic ,strong) UILabel *lblTitle;
@end
