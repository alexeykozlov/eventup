//
//  Invitation.h
//  BMbook
//
//  Created by iApps Mac Mini on 10/1/13.
//  Copyright (c) 2013 Shoshi V. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Invitation : NSManagedObject

@property (nonatomic, retain) NSData * image;

@end
